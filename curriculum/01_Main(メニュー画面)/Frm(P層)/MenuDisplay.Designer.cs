﻿namespace curriculum1
{
    partial class MenuDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBase = new System.Windows.Forms.Button();
            this.btnStructured = new System.Windows.Forms.Button();
            this.btnObject = new System.Windows.Forms.Button();
            this.btnFunction = new System.Windows.Forms.Button();
            this.btnMemory_Resource = new System.Windows.Forms.Button();
            this.btnAsynchronous = new System.Windows.Forms.Button();
            this.btnData = new System.Windows.Forms.Button();
            this.btnDynamic = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(528, 66);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(147, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(214, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "メニュー画面";
            // 
            // btnBase
            // 
            this.btnBase.Location = new System.Drawing.Point(24, 92);
            this.btnBase.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnBase.Name = "btnBase";
            this.btnBase.Size = new System.Drawing.Size(131, 68);
            this.btnBase.TabIndex = 1;
            this.btnBase.Text = "基礎";
            this.btnBase.UseVisualStyleBackColor = true;
            // 
            // btnStructured
            // 
            this.btnStructured.Location = new System.Drawing.Point(197, 92);
            this.btnStructured.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnStructured.Name = "btnStructured";
            this.btnStructured.Size = new System.Drawing.Size(131, 68);
            this.btnStructured.TabIndex = 1;
            this.btnStructured.Text = "構造化(13-1_27-2)";
            this.btnStructured.UseVisualStyleBackColor = true;
            // 
            // btnObject
            // 
            this.btnObject.Location = new System.Drawing.Point(372, 92);
            this.btnObject.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnObject.Name = "btnObject";
            this.btnObject.Size = new System.Drawing.Size(131, 68);
            this.btnObject.TabIndex = 2;
            this.btnObject.Text = "オブジェクト指向(28-1_40-2)";
            this.btnObject.UseVisualStyleBackColor = true;
            // 
            // btnFunction
            // 
            this.btnFunction.Location = new System.Drawing.Point(24, 183);
            this.btnFunction.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnFunction.Name = "btnFunction";
            this.btnFunction.Size = new System.Drawing.Size(131, 68);
            this.btnFunction.TabIndex = 2;
            this.btnFunction.Text = "関数指向(41-1_44-7)";
            this.btnFunction.UseVisualStyleBackColor = true;
            // 
            // btnMemory_Resource
            // 
            this.btnMemory_Resource.Location = new System.Drawing.Point(197, 183);
            this.btnMemory_Resource.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnMemory_Resource.Name = "btnMemory_Resource";
            this.btnMemory_Resource.Size = new System.Drawing.Size(131, 68);
            this.btnMemory_Resource.TabIndex = 3;
            this.btnMemory_Resource.Text = "メモリとリソース管理(45-1_51-3)";
            this.btnMemory_Resource.UseVisualStyleBackColor = true;
            // 
            // btnAsynchronous
            // 
            this.btnAsynchronous.Location = new System.Drawing.Point(24, 274);
            this.btnAsynchronous.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAsynchronous.Name = "btnAsynchronous";
            this.btnAsynchronous.Size = new System.Drawing.Size(131, 68);
            this.btnAsynchronous.TabIndex = 4;
            this.btnAsynchronous.Text = "非同期処理(56-1_58-3)";
            this.btnAsynchronous.UseVisualStyleBackColor = true;
            // 
            // btnData
            // 
            this.btnData.Location = new System.Drawing.Point(372, 183);
            this.btnData.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnData.Name = "btnData";
            this.btnData.Size = new System.Drawing.Size(131, 68);
            this.btnData.TabIndex = 5;
            this.btnData.Text = "データ処理(52-1_55-4)";
            this.btnData.UseVisualStyleBackColor = true;
            // 
            // btnDynamic
            // 
            this.btnDynamic.Location = new System.Drawing.Point(197, 274);
            this.btnDynamic.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnDynamic.Name = "btnDynamic";
            this.btnDynamic.Size = new System.Drawing.Size(131, 68);
            this.btnDynamic.TabIndex = 4;
            this.btnDynamic.Text = "動的な処理(59-1_62-11)";
            this.btnDynamic.UseVisualStyleBackColor = true;
            // 
            // MenuDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 372);
            this.Controls.Add(this.btnData);
            this.Controls.Add(this.btnDynamic);
            this.Controls.Add(this.btnAsynchronous);
            this.Controls.Add(this.btnMemory_Resource);
            this.Controls.Add(this.btnFunction);
            this.Controls.Add(this.btnObject);
            this.Controls.Add(this.btnStructured);
            this.Controls.Add(this.btnBase);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "MenuDisplay";
            this.Text = "d";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBase;
        private System.Windows.Forms.Button btnStructured;
        private System.Windows.Forms.Button btnObject;
        private System.Windows.Forms.Button btnFunction;
        private System.Windows.Forms.Button btnMemory_Resource;
        private System.Windows.Forms.Button btnAsynchronous;
        private System.Windows.Forms.Button btnData;
        private System.Windows.Forms.Button btnDynamic;
    }
}