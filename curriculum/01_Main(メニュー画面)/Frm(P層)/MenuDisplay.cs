﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace curriculum1
{
    /// ********************************************************************************
    /// <summary>
    /// メインフォーム
    /// </summary>
    /// <remarks>none</remarks>
    /// 
    /// <history>
    /// --------------------------------------------------------------------------------
    /// No     Date         User         Remorks
    /// --------------------------------------------------------------------------------
    /// 0001   2015.12.05   宮崎祥彰   *new*
    /// 
    /// </history>
    /// ********************************************************************************
    public partial class MenuDisplay : Base_Form
    {

        #region ++++++++++ member variable ++++++++++
        #endregion

        #region++++++++++ constructor/destructor ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰     *new*
        /// 
        /// </history>
        /// ********************************************************************************
        public MenuDisplay()
        {
            // -------------------------------------------------------
            // 初期化処理
            InitializeComponent();
            this.EntryEvents();
        }

        #endregion

        #region ++++++++++ event ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// イベント Form　Load時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void FormMain_Load(object sender, EventArgs e) {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = !this.MaximizeBox;
            
        }

        /// ********************************************************************************
        /// <summary>
        /// イベントハンドラ登録
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void EntryEvents()
        {
            this.Load += new EventHandler(this.FormMain_Load);
            this.btnBase.Click += new EventHandler(this.btnBase_Click);
            this.btnStructured.Click += new EventHandler(this.btnStructured_Click);
            this.btnObject.Click += new EventHandler(this.btnObject_Click);
            this.btnFunction.Click += new EventHandler(this.btnFunction_Click);
            this.btnMemory_Resource.Click += new EventHandler(this.Memory_Resource_Click);
            this.btnAsynchronous.Click += new EventHandler(this.Asynchronous_Click);
            this.btnData.Click += new EventHandler(this.Data_Click);
            this.btnDynamic.Click += new EventHandler(this.Dynamic_Click);
        }

        #endregion

        #region++++++++++ 同期処理 ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 基礎ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btnBase_Click(object sender, EventArgs e)
        {


        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 構造化ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btnStructured_Click(object sender, EventArgs e)
        {
            FrmStructure StructureDisplay = new FrmStructure();

            StructureDisplay.ShowDialog();

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// オブジェクト指向ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btnObject_Click(object sender, EventArgs e)
        {
            FrmObject ObjectDisplay = new FrmObject();

            ObjectDisplay.ShowDialog();

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 関数指向ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btnFunction_Click(object sender, EventArgs e)
        {
            FrmFunction ObjectDisplay = new FrmFunction();

            ObjectDisplay.ShowDialog();

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// メモリとリソース管理ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void Memory_Resource_Click(object sender, EventArgs e)
        {
            FrmMemory_Resource ObjectDisplay = new FrmMemory_Resource();

            ObjectDisplay.ShowDialog();

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 非同期処理ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void Asynchronous_Click(object sender, EventArgs e)
        {
            FrmAsynchronous ObjectDisplay = new FrmAsynchronous();

            ObjectDisplay.ShowDialog();

        }
        
        ///// ********************************************************************************
        ///// <summary>
        ///// データ処理ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void Data_Click(object sender, EventArgs e)
        {
            FrmData Data = new FrmData();

            Data.ShowDialog();

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// データ処理ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void Dynamic_Click(object sender, EventArgs e)
        {
            FrmDynamic btnDynamic = new FrmDynamic();

            btnDynamic.ShowDialog();

        }
        #endregion
    }
}
