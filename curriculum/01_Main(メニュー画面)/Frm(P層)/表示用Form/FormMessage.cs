﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using curriculum1;

namespace curriculum.FormMessage.Frm_P層_.表示用Form
{
    public partial class FormMessage : Form
    {

        #region ++++++++++ member variable ++++++++++

        private FrmStructure FormSt;

        private int showTopicNO;

        #endregion

        #region++++++++++ constructor/destructor ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        public FormMessage(FrmStructure.TopicNo eTopic)
        {
            InitializeComponent();
            showTopicNO = (int)eTopic;
            fmMessageLoad();
            EntryEvents();
            InitMember();
        }
        public FormMessage(FrmObject.TopicNo eTopic)
        {
            InitializeComponent();
            showTopicNO = (int)eTopic;
            fmMessageLoad();
            EntryEvents();
            InitMember();
        }
        public FormMessage(FrmFunction.TopicNo eTopic)
        {
            InitializeComponent();
            showTopicNO = (int)eTopic;
            fmMessageLoad();
            EntryEvents();
            InitMember();
        }

        #endregion

        #region ++++++++++ event ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// fmMessageLoad時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        public void fmMessageLoad()
        {
            switch (showTopicNO)
            {
                case (int)FrmStructure.TopicNo.Topic14_1:
                    this.textBox1.Text = "0";
                    this.label2.Text = "■整数を入力してください。";
                    this.label3.Text = "※0以外が入力された場合、入力された数値の逆数を求めて表示";
                    break;
                case (int)FrmStructure.TopicNo.Topic14_2:
                    this.textBox1.Text = "0";
                    this.label2.Text = "何等ですか？";
                    break;
                case (int)FrmStructure.TopicNo.Topic14_3:
                    this.textBox1.Text = "0";
                    this.label2.Text = "1つ目の整数を入力してください。";
                    this.btn1.Tag = 0;
                    break;
                case (int)FrmStructure.TopicNo.Topic14_4:
                    this.textBox1.Text = "0";
                    this.label2.Text = "何等ですか？";
                    break;
                case (int)FrmStructure.TopicNo.Topic15_1:
                    this.textBox1.Text = "0";
                    this.label2.Text = "1つ目の整数を入力してください。";
                    this.btn1.Tag = 0;
                    break;
                case (int)FrmStructure.TopicNo.Topic15_2:
                    this.textBox1.Text = "0";
                    this.label2.Text = "1～5のいずれかの数値を入力してください。";
                    this.btn1.Tag = 0;
                    break;
                case (int)FrmStructure.TopicNo.Topic15_3:
                case (int)FrmStructure.TopicNo.Topic15_4:
                    btnClickEvent15();
                    break;
                case (int)FrmStructure.TopicNo.Topic16_1:
                    this.textBox1.Text = "0";
                    this.label2.Text = "1つ目の整数を入力してください。";
                    this.btn1.Tag = 0;
                    break;
                case (int)FrmStructure.TopicNo.Topic16_2:
                case (int)FrmStructure.TopicNo.Topic16_3:
                    btnClickEvent16();
                    break;
                case (int)FrmStructure.TopicNo.Topic17_1:
                case (int)FrmStructure.TopicNo.Topic17_2:
                case (int)FrmStructure.TopicNo.Topic17_3:
                case (int)FrmStructure.TopicNo.Topic17_4:
                case (int)FrmStructure.TopicNo.Topic17_5:
                case (int)FrmStructure.TopicNo.Topic17_6:
                case (int)FrmStructure.TopicNo.Topic17_7:
                    btnClickEvent17();
                    break;
                case (int)FrmObject.TopicNo.Topic28_3:
                    this.textBox1.Text = "0";
                    this.label2.Text = "実部を入力してください。";
                    this.btn1.Tag = 0;
                    break;
                case (int)FrmFunction.TopicNo.Topic43_2:
                    this.textBox1.Text = "";
                    this.label2.Text = String.Empty;
                    this.label3.Text = 
                                        "使い方\n" +
                                        "r (run)    : 時刻表示を開始します。\n" +
                                        "s (suspend): 時刻表示を一時停止します。\n" +
                                        "f (full)   : 時刻の表示形式を“日付＋時刻”にします。\n" +
                                        "d (date)   : 時刻の表示形式を“日付のみ”にします。\n" +
                                        "t (time)   : 時刻の表示形式を“時刻のみ”にします。\n" +
                                        "q (quit)   : プログラムを終了します。\n";
                    this.btn1.Tag = 0;
                    break;
            }
        }

        /// ********************************************************************************
        /// <summary>
        /// イベントハンドラ登録
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void EntryEvents(){

            this.Load += new EventHandler(this.FormMain_Load);
            this.btn1.Click += new EventHandler(this.btn1_Click);

        }

        /// ********************************************************************************
        /// <summary>
        /// イベント Form　Load時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void FormMain_Load(object sender, EventArgs e) {

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = !this.MaximizeBox;
            
        }

        /// ********************************************************************************
        /// <summary>
        /// OKボタンクリック時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn1_Click(object sender, EventArgs e)
        {
            switch (showTopicNO)
            {
                case (int)FrmStructure.TopicNo.Topic14_1:
                case (int)FrmStructure.TopicNo.Topic14_2:
                case (int)FrmStructure.TopicNo.Topic14_3:
                case (int)FrmStructure.TopicNo.Topic14_4:
                    btnClickEvent14();
                    break;
                case (int)FrmStructure.TopicNo.Topic15_1:
                case (int)FrmStructure.TopicNo.Topic15_2:
                case (int)FrmStructure.TopicNo.Topic15_3:
                    btnClickEvent15();
                    break;
                case (int)FrmStructure.TopicNo.Topic16_1:
                case (int)FrmStructure.TopicNo.Topic16_2:
                case (int)FrmStructure.TopicNo.Topic16_3:
                    btnClickEvent16();
                    break;
                case (int)FrmObject.TopicNo.Topic28_3:
                    btnClickEvent28_3();
                    break;
            }
        }

        #endregion

        #region ++++++++++ method ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// メンバ初期化
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.02.09   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void InitMember()
        {
            FormSt = new FrmStructure();
        }
        /// ********************************************************************************
        /// <summary>
        /// 14.ボタンクリック時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.02.09   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btnClickEvent14()
        {
             switch (showTopicNO)
            {
                case (int)FrmStructure.TopicNo.Topic14_1:
                    {
                        // 整数を入力してもらう
                        int x;
                        string a;
                        //Text = "整数を入力してください : ";
                        if (textBox1.Text.Length == 0)
                        {
                            MessageBox.Show("整数を入力してください");
                            return;
                        }

                        x = int.Parse(textBox1.Text);

                        if (x == 0)
                        {
                            // 0が入力された場合、エラーメッセージだけ表示
                            MessageBox.Show("0が入力されました");
                        }
                        else
                        {
                            // 0以外が入力された場合、入力された数値の逆数を求めて表示
                            double x_inv = 1.0 / x;
                            a = Convert.ToString(1 / x) + "=" + Convert.ToString(x_inv);
                            MessageBox.Show(a);
                        }
                        break;
                    }
                case (int)FrmStructure.TopicNo.Topic14_2:
                    {
                        // 整数を入力してもらう
                        int x;
                        string a;
                        //Text = "整数を入力してください : ";
                        if (textBox1.Text.Length == 0)
                        {
                            MessageBox.Show("整数を入力してください");
                            return;
                        }
                        x = int.Parse(textBox1.Text);

                        switch (x)
                        {
                            case 1:
                                a = "ハワイ旅行"; // 変数の値 == 値1 のとき実行される
                                break;
                            case 2:
                                a = "マッサージ券";// 変数の値 == 値2 のとき実行される
                                break;

                            default:
                                a = "ティッシュ"; // 変数の値がどの値とも異なるとき実行される
                                break;
                        }
                        this.label3.Text = a;
                        break;
                    }
                case (int)FrmStructure.TopicNo.Topic14_3:
                    {

                        if ((int)this.btn1.Tag == 0)
                        {

                            this.textBox1.Tag = int.Parse(this.textBox1.Text);
                            this.textBox1.Text = "0";
                            this.label2.Text = "2つ目の整数を入力してください : ";

                            this.btn1.Tag = 1;

                        }
                        else if ((int)this.btn1.Tag == 1)
                        {

                            this.label2.Tag = int.Parse(this.textBox1.Text);
                            this.textBox1.Text = "";
                            this.label2.Text = "行いたい操作を入力してください(+ - / *)";

                            this.btn1.Tag = 2;

                        }
                        else if ((int)this.btn1.Tag == 2)
                        {


                            // 整数を2つ入力してもらう
                            int x, y;

                            x = (int)textBox1.Tag;
                            y = (int)label2.Tag;

                            switch (this.textBox1.Text)
                            {
                                case "+":
                                    MessageBox.Show(string.Format("{0} + {1} = {2}", x, y, x + y));
                                    break;
                                case "-":
                                    MessageBox.Show(string.Format("{0} - {1} = {2}", x, y, x - y));
                                    break;
                                case "*":
                                    MessageBox.Show(string.Format("{0} × {1} = {2}", x, y, x * y));
                                    break;
                                case "/":
                                    if (y != 0)
                                        MessageBox.Show(string.Format("{0} ÷ {1} = {2} … {3}", x, y, x / y, x % y));
                                    break;
                                default:
                                    MessageBox.Show("対応していない操作です");
                                    break;
                            }
                        }
                        break;
                    }
                case (int)FrmStructure.TopicNo.Topic14_4:
                    {
                        // 整数を入力してもらう
                        int x;
                        string a;
                        //Text = "整数を入力してください : ";
                        if (textBox1.Text.Length == 0)
                        {
                            MessageBox.Show("整数を入力してください");
                            return;
                        }
                        x = int.Parse(textBox1.Text);

                        switch (x)
                        {
                            case 1:
                                //a = "ハワイ旅行"; // 変数の値 == 値1 のとき実行される
                                goto case 2;

                            case 2:
                                a = "マッサージ券";// 変数の値 == 値2 のとき実行される
                                break;

                            default:
                                a = "ティッシュ"; // 変数の値がどの値とも異なるとき実行される
                                break;
                        }
                        this.label3.Text = a;
                        break;
                    }
            }
        }
        /// ********************************************************************************
        /// <summary>
        /// 15.ボタンクリック時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.02.09   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btnClickEvent15()
        {
             switch (showTopicNO)
            {
                case (int)FrmStructure.TopicNo.Topic15_1:
                    {

                        if ((int)this.btn1.Tag == 0)
                        {

                            this.textBox1.Tag = int.Parse(this.textBox1.Text);
                            this.textBox1.Text = "0";
                            this.label2.Text = "2つ目の整数を入力してください : ";

                            this.btn1.Tag = 1;

                        }
                        else if ((int)this.btn1.Tag == 1)
                        {
                            int a, b;
                            a = (int)this.textBox1.Tag;
                            b = int.Parse(this.textBox1.Text);
                            
                        // ユークリッド互除法を使ってaとbの最大公約数を求める
                        while (b != 0)
                        {
                            // b が 0 になるまで繰り返し実行される
                            int r = a % b;
                            a = b;
                            b = r;
                        }
                            MessageBox.Show(string.Format("{0}と{1}の最大公約数は{2}", (int)this.textBox1.Tag, int.Parse(this.textBox1.Text), a));

                        }
                        break;
                    }
                case (int)FrmStructure.TopicNo.Topic15_2:
                    {
                        int n;
                        n = int.Parse(this.textBox1.Text);
                        do
                        {
                            if (n < 1 || n > 5)
                            {
                                MessageBox.Show("入力された文字が不正です。");
                                textBox1.Focus();
                                return;
                            }
                        }
                        while (n < 1 || n > 5); // nの値が1～5の範囲に入るまで繰り返し

                        MessageBox.Show(string.Format("あなたの入力した数値は{0}です", n));
                        
                        break;
                    }
                case (int)FrmStructure.TopicNo.Topic15_3:
                    {
                        string str99 = string.Empty;
                        //九九表を作成
                        for (int x = 1; x <= 9; ++x) // xを1～9まで、1ずつ増やして繰り返し
                        {
                            for (int y = 1; y <= 9; ++y) // yを1～9まで、1ずつ増やして繰り返し
                            {
                                // xy の値を、幅をそろえて表示
                                str99 += (string)((x * y).ToString().PadLeft(3, ' '));
                            }
                            str99 += "\n";
                        }
                        MessageBox.Show(str99);
                        this.Close();
                       
                        break;
                    }
                case (int)FrmStructure.TopicNo.Topic15_4:
                    {
                        int[] a = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
                        double y = 0;
                        int z;
                        foreach (double x in a)
                        {
                            y += x;
                        }

                        z = (int)y / a.Length;
                        MessageBox.Show(string.Format("{0}",z));
                        this.Close();
                        break;
                    }
            }
        }
        /// ********************************************************************************
        /// <summary>
        /// 16.ボタンクリック時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.02.09   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btnClickEvent16()
        {
            switch (showTopicNO)
            {
                case (int)FrmStructure.TopicNo.Topic16_1:
                    {

                        if ((int)this.btn1.Tag == 0)
                        {

                            this.textBox1.Tag = int.Parse(this.textBox1.Text);
                            this.textBox1.Text = "0";
                            this.label2.Text = "2つ目の整数を入力してください : ";

                            this.btn1.Tag = 1;

                        }
                        else if ((int)this.btn1.Tag == 1)
                        {
                            // フィボナッチ数列の20項目までを求める
                            int[] sequence = new int[20];
                            sequence[0] = (int)this.textBox1.Tag;
                            sequence[1] = int.Parse(this.textBox1.Text);

                            // 漸化式を使って20項目までを計算
                            for (int i = 2; i < sequence.Length; ++i)
                            {
                                sequence[i] = sequence[i - 1] + sequence[i - 2];
                            }

                            // 結果の出力
                            Console.Write("{");
                            string strOutPut = string.Empty;
                            strOutPut = "{";
                            for (int i = 0; i < sequence.Length - 1; ++i)
                            {
                                strOutPut += sequence[i] + ", ";
                            }
                            strOutPut += sequence[sequence.Length - 1] + "}";
                            MessageBox.Show(strOutPut);

                        }
                        break;
                    }
                case (int)FrmStructure.TopicNo.Topic16_2:
                    {
                        double[,] a = new double[,] { { 1, 2 }, { 2, 1 }, { 0, 1 } }; // 3行2列の行列
                        double[,] b = new double[,] { { 1, 2, 0 }, { 0, 1, 2 } };   // 2行3列の行列
                        double[,] c = new double[3, 3];                      // 3行3列の行列

                        for (int i = 0; i < a.GetLength(0); ++i) // a.GetLength(0) は a の行数を表す。
                        {
                            for (int j = 0; j < b.GetLength(1); ++j) // b.GetLength(1) は b の列数を表す。
                            {
                                c[i, j] = 0;
                                for (int k = 0; k < a.GetLength(1); ++k) // a.GetLength(1) は a の列数を表す。
                                {
                                    c[i, j] += a[i, k] * b[k, j];
                                    MessageBox.Show(string.Format("c[{0}],", c[i,j]));
                                }
                            }
                        }

                        break;
                    }
                case (int)FrmStructure.TopicNo.Topic16_3:
                    {
                        double[][] a = new double[][]{  // 3行2列の行列
                            new double[]{1, 2},
                            new double[]{2, 1},
                            new double[]{0, 1}
                        };
                                        double[][] b = new double[][]{  // 2行3列の行列
                            new double[]{1, 2, 0},
                            new double[]{0, 1, 2}
                        };
                        double[][] c = new double[3][]; // 3行3列の行列
                        for (int i = 0; i < c.Length; ++i)
                            c[i] = new double[3];

                        for (int i = 0; i < a.Length; ++i) // a.Length は a の行数を表す。
                        {
                            for (int j = 0; j < b[0].Length; ++j) // b[0].Length は b の列数を表す。
                            {
                                c[i][j] = 0;
                                for (int k = 0; k < a[0].Length; ++k) // a[0].Length は a の列数を表す。
                                {
                                    c[i][j] += a[i][k] * b[k][j];
                                    MessageBox.Show(string.Format("c[{0}],",c[i][j]));
                                }
                            }
                        }
                        break;
                    }
            }
        }
        /// ********************************************************************************
        /// <summary>
        /// 17.ボタンクリック時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.02.09   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btnClickEvent17()
        {
            switch (showTopicNO)
            {
                case (int)FrmStructure.TopicNo.Topic17_1:
                    {
                        for (int i = 0; i < 10; ++i)
                        {
                            double x = 0.01 * i;

                            double y = Sin(x); // 関数呼び出し

                            MessageBox.Show(string.Format("sin({0:f2}) = {1:f6}\n", x, y));
                        }
                         break;
                    }
                case (int)FrmStructure.TopicNo.Topic17_2:
                    {

                        int[] array = new int[3];

                        // 乱数を使って値を生成
                        for (int i = 0; i < array.Length; ++i)
                        {
                            array[i] = (int)(Random() >> 58); // [0,63] の整数乱数生成
                        }

                        // ノルムを計算
                        double norm = Norm(array[0], array[1], array[2]);

                        // 値の出力
                        WriteArray(array);
                         MessageBox.Show(string.Format("norm = {0}\n", norm));

                        break;
                    }
                case (int)FrmStructure.TopicNo.Topic17_3:
                    {
                        // 引数の省略(optional parameter)
                        var s1 = Sum17_3();     // Sum17_3(0, 0, 0); と同じ意味。
                        var s2 = Sum17_3(1);    // Sum17_3(1, 0, 0); と同じ意味。
                        var s3 = Sum17_3(1, 2); // Sum17_3(1, 2, 0); と同じ意味。
                        MessageBox.Show(String.Format("s1 = {0},s2 = {1},s3 = {2}",s1,s2,s3));
                        // 名前付きで引数を与える(named parameter)
                        var s4 = Sum17_3(x: 1, y: 2, z: 3); // Sum17_3(1, 2, 3); と同じ意味。
                        var s5 = Sum17_3(z: 3);             // Sum17_3(0, 0, 3); と同じ意味。
                        MessageBox.Show(String.Format("s1 = {0},s2 = {1}", s4, s5));
                        break;
                    }
                case (int)FrmStructure.TopicNo.Topic17_4:
                    {
                        var norm = Norm(3, 4, 5); // 3, 4, 5 が実引数
                        MessageBox.Show(Convert.ToString(norm));
                        break;
                    }
                case (int)FrmStructure.TopicNo.Topic17_6:
                    {
                        WriteTypeAndValue("サンプル"); // WriteTypeAndValue(string) が呼ばれる
                        WriteTypeAndValue(13);         // WriteTypeAndValue(int)    が呼ばれる
                        WriteTypeAndValue(3.14159265); // WriteTypeAndValue(double) が呼ばれる
                        break;
                    }
            }
        }
        /// <summary>
        /// sin(x) の値を求める。
        /// 実装は割りと適当。
        /// </summary>
        static double Sin(double x) // 関数定義
        {
            double xx = -x * x;
            double fact = 1;
            double sin = x;
            for (int i = 1; i < 100; )
            {
                fact *= i; ++i; fact *= i; ++i;
                x *= xx;
                sin += x / fact;
            }
            return sin;
        }
        static ulong seed = 4275646293455673547UL;
        /// <summary>
        /// 線形合同法による乱数の生成
        /// </summary>
        static ulong Random()
        {
            unchecked { seed = seed * 1566083941UL + 1; }
            return seed;
        }

        /// <summary>
        /// 入力した3つの値のノルムを計算
        /// <summary>
        static double Norm(double x, double y, double z)
        {
            return x * x + y * y + z * z;
        }

        /// <summary>
        /// 配列を , で各要素を区切って、{}で括った形式で出力
        /// <summary>
        static void WriteArray(int[] array)
        {
            Console.Write("{");
            for (int i = 0; i < array.Length - 1; ++i)
            {
                Console.Write("{0}, ", array[i]);
            }
            Console.Write(array[array.Length - 1] + "}\n");
        }
        static int Sum17_3(int x = 0, int y = 0, int z = 0)
        {
            return x + y + z;
        }
        /// <summary>
        /// 型名と値を出力する(string 版)。
        /// </summary>
        static void WriteTypeAndValue(string s)
        {
            MessageBox.Show(string.Format("文字列 : {0}\n", s));
        }

        /// <summary>
        /// 型名と値を出力する(int 版)。
        /// </summary>
        static void WriteTypeAndValue(int n)
        {
            MessageBox.Show(string.Format("整数   : {0}\n", n));
        }

        /// <summary>
        /// 型名と値を出力する(double 版)。
        /// </summary>
        static void WriteTypeAndValue(double x)
        {
            MessageBox.Show(string.Format("実数   : {0}\n", x));
        }

        /// ********************************************************************************
        /// <summary>
        /// 28_3 OKボタンクリック時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btnClickEvent28_3()
        {
            if((int)btn1.Tag == 0){
            try{
            double.Parse(textBox1.Text);
            label2.Text = "虚部を入力してください";
            label3.Text = String.Empty;
            label2.Tag = double.Parse(textBox1.Text);
            btn1.Tag = 1;

            }catch(Exception){
            label3.Text = "error : 正しい値が入力されませんでした\n入力しなおしてください";
            }
            }else if((int)btn1.Tag == 1){
            try{
            double.Parse(textBox1.Text);
            label2.Text = "虚部を入力してください";
            label3.Text = String.Empty;
            label3.Tag = double.Parse(textBox1.Text);
            }catch(Exception){
            label3.Text = "error : 正しい値が入力されませんでした\n入力しなおしてください";
            }
            
            
            } 
            
               

        }

        #endregion
    }
}
