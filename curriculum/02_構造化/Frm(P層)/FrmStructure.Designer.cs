﻿namespace curriculum1
{
    partial class FrmStructure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn13_1_1 = new System.Windows.Forms.Button();
            this.btn13_1_2 = new System.Windows.Forms.Button();
            this.btn13_1_3 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn14_4 = new System.Windows.Forms.Button();
            this.btn14_3 = new System.Windows.Forms.Button();
            this.btn14_2 = new System.Windows.Forms.Button();
            this.btn14_1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn15_4 = new System.Windows.Forms.Button();
            this.btn15_3 = new System.Windows.Forms.Button();
            this.btn15_2 = new System.Windows.Forms.Button();
            this.btn15_1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn16_4 = new System.Windows.Forms.Button();
            this.btn16_3 = new System.Windows.Forms.Button();
            this.btn16_2 = new System.Windows.Forms.Button();
            this.btn16_1 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btn18_2 = new System.Windows.Forms.Button();
            this.btn18_1 = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btn19_2 = new System.Windows.Forms.Button();
            this.btn19_1 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btn17_6 = new System.Windows.Forms.Button();
            this.btn17_7 = new System.Windows.Forms.Button();
            this.btn17_5 = new System.Windows.Forms.Button();
            this.btn17_4 = new System.Windows.Forms.Button();
            this.btn17_3 = new System.Windows.Forms.Button();
            this.btn17_2 = new System.Windows.Forms.Button();
            this.btn17_1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.btn26_3 = new System.Windows.Forms.Button();
            this.btn26_2 = new System.Windows.Forms.Button();
            this.btn26_1 = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.btn25_4 = new System.Windows.Forms.Button();
            this.btn25_3 = new System.Windows.Forms.Button();
            this.btn25_2 = new System.Windows.Forms.Button();
            this.btn25_1 = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.btn24_3 = new System.Windows.Forms.Button();
            this.btn24_2 = new System.Windows.Forms.Button();
            this.btn24_1 = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btn21_1 = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.btn22_3 = new System.Windows.Forms.Button();
            this.btn22_2 = new System.Windows.Forms.Button();
            this.btn22_1 = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.btn23_3 = new System.Windows.Forms.Button();
            this.btn23_2 = new System.Windows.Forms.Button();
            this.btn23_1 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btn20_1 = new System.Windows.Forms.Button();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.btn27_2 = new System.Windows.Forms.Button();
            this.btn27_1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(75, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(269, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "構造化(13-1_27-2)";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1057, 53);
            this.panel1.TabIndex = 2;
            // 
            // btn13_1_1
            // 
            this.btn13_1_1.Location = new System.Drawing.Point(10, 18);
            this.btn13_1_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn13_1_1.Name = "btn13_1_1";
            this.btn13_1_1.Size = new System.Drawing.Size(98, 37);
            this.btn13_1_1.TabIndex = 3;
            this.btn13_1_1.Text = "制御フロー(13-1-1)";
            this.btn13_1_1.UseVisualStyleBackColor = true;
            // 
            // btn13_1_2
            // 
            this.btn13_1_2.Location = new System.Drawing.Point(144, 18);
            this.btn13_1_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn13_1_2.Name = "btn13_1_2";
            this.btn13_1_2.Size = new System.Drawing.Size(98, 37);
            this.btn13_1_2.TabIndex = 3;
            this.btn13_1_2.Text = "制御フロー(13-1-2)";
            this.btn13_1_2.UseVisualStyleBackColor = true;
            // 
            // btn13_1_3
            // 
            this.btn13_1_3.Location = new System.Drawing.Point(275, 18);
            this.btn13_1_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn13_1_3.Name = "btn13_1_3";
            this.btn13_1_3.Size = new System.Drawing.Size(98, 37);
            this.btn13_1_3.TabIndex = 3;
            this.btn13_1_3.Text = "制御フロー(13-1-3)";
            this.btn13_1_3.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn13_1_3);
            this.groupBox1.Controls.Add(this.btn13_1_2);
            this.groupBox1.Controls.Add(this.btn13_1_1);
            this.groupBox1.Location = new System.Drawing.Point(15, 12);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(381, 66);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "制御フロー";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn14_4);
            this.groupBox2.Controls.Add(this.btn14_3);
            this.groupBox2.Controls.Add(this.btn14_2);
            this.groupBox2.Controls.Add(this.btn14_1);
            this.groupBox2.Location = new System.Drawing.Point(15, 90);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(381, 114);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "条件分岐";
            // 
            // btn14_4
            // 
            this.btn14_4.Location = new System.Drawing.Point(10, 65);
            this.btn14_4.Margin = new System.Windows.Forms.Padding(2);
            this.btn14_4.Name = "btn14_4";
            this.btn14_4.Size = new System.Drawing.Size(98, 37);
            this.btn14_4.TabIndex = 3;
            this.btn14_4.Text = "goto 文(14-4)";
            this.btn14_4.UseVisualStyleBackColor = true;
            // 
            // btn14_3
            // 
            this.btn14_3.Location = new System.Drawing.Point(275, 18);
            this.btn14_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn14_3.Name = "btn14_3";
            this.btn14_3.Size = new System.Drawing.Size(98, 37);
            this.btn14_3.TabIndex = 3;
            this.btn14_3.Text = "フォースルーの禁止(14-3)";
            this.btn14_3.UseVisualStyleBackColor = true;
            // 
            // btn14_2
            // 
            this.btn14_2.Location = new System.Drawing.Point(144, 18);
            this.btn14_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn14_2.Name = "btn14_2";
            this.btn14_2.Size = new System.Drawing.Size(98, 37);
            this.btn14_2.TabIndex = 3;
            this.btn14_2.Text = "swich文(14-2)";
            this.btn14_2.UseVisualStyleBackColor = true;
            // 
            // btn14_1
            // 
            this.btn14_1.Location = new System.Drawing.Point(10, 18);
            this.btn14_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn14_1.Name = "btn14_1";
            this.btn14_1.Size = new System.Drawing.Size(98, 37);
            this.btn14_1.TabIndex = 3;
            this.btn14_1.Text = "if文(14-1)";
            this.btn14_1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn15_4);
            this.groupBox3.Controls.Add(this.btn15_3);
            this.groupBox3.Controls.Add(this.btn15_2);
            this.groupBox3.Controls.Add(this.btn15_1);
            this.groupBox3.Location = new System.Drawing.Point(15, 208);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(381, 114);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "反復処理";
            // 
            // btn15_4
            // 
            this.btn15_4.Location = new System.Drawing.Point(10, 65);
            this.btn15_4.Margin = new System.Windows.Forms.Padding(2);
            this.btn15_4.Name = "btn15_4";
            this.btn15_4.Size = new System.Drawing.Size(98, 37);
            this.btn15_4.TabIndex = 3;
            this.btn15_4.Text = "foreach文(15-4)";
            this.btn15_4.UseVisualStyleBackColor = true;
            // 
            // btn15_3
            // 
            this.btn15_3.Location = new System.Drawing.Point(275, 18);
            this.btn15_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn15_3.Name = "btn15_3";
            this.btn15_3.Size = new System.Drawing.Size(98, 37);
            this.btn15_3.TabIndex = 3;
            this.btn15_3.Text = "for 文(15-3)";
            this.btn15_3.UseVisualStyleBackColor = true;
            // 
            // btn15_2
            // 
            this.btn15_2.Location = new System.Drawing.Point(144, 18);
            this.btn15_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn15_2.Name = "btn15_2";
            this.btn15_2.Size = new System.Drawing.Size(98, 37);
            this.btn15_2.TabIndex = 3;
            this.btn15_2.Text = "do-while 文(15-2)";
            this.btn15_2.UseVisualStyleBackColor = true;
            // 
            // btn15_1
            // 
            this.btn15_1.Location = new System.Drawing.Point(10, 18);
            this.btn15_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn15_1.Name = "btn15_1";
            this.btn15_1.Size = new System.Drawing.Size(98, 37);
            this.btn15_1.TabIndex = 3;
            this.btn15_1.Text = "while文(15-1)";
            this.btn15_1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn16_4);
            this.groupBox4.Controls.Add(this.btn16_3);
            this.groupBox4.Controls.Add(this.btn16_2);
            this.groupBox4.Controls.Add(this.btn16_1);
            this.groupBox4.Location = new System.Drawing.Point(15, 326);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(381, 114);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "整列";
            // 
            // btn16_4
            // 
            this.btn16_4.Location = new System.Drawing.Point(10, 65);
            this.btn16_4.Margin = new System.Windows.Forms.Padding(2);
            this.btn16_4.Name = "btn16_4";
            this.btn16_4.Size = new System.Drawing.Size(98, 37);
            this.btn16_4.TabIndex = 3;
            this.btn16_4.Text = "比較(16-4)";
            this.btn16_4.UseVisualStyleBackColor = true;
            // 
            // btn16_3
            // 
            this.btn16_3.Location = new System.Drawing.Point(275, 18);
            this.btn16_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn16_3.Name = "btn16_3";
            this.btn16_3.Size = new System.Drawing.Size(98, 37);
            this.btn16_3.TabIndex = 3;
            this.btn16_3.Text = "配列の配列(16-3)";
            this.btn16_3.UseVisualStyleBackColor = true;
            // 
            // btn16_2
            // 
            this.btn16_2.Location = new System.Drawing.Point(144, 18);
            this.btn16_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn16_2.Name = "btn16_2";
            this.btn16_2.Size = new System.Drawing.Size(98, 37);
            this.btn16_2.TabIndex = 3;
            this.btn16_2.Text = "四角い多次元配列(16-2)";
            this.btn16_2.UseVisualStyleBackColor = true;
            // 
            // btn16_1
            // 
            this.btn16_1.Location = new System.Drawing.Point(10, 18);
            this.btn16_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn16_1.Name = "btn16_1";
            this.btn16_1.Size = new System.Drawing.Size(98, 37);
            this.btn16_1.TabIndex = 3;
            this.btn16_1.Text = "暗黙的型付け配列(16-1)";
            this.btn16_1.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btn18_2);
            this.groupBox6.Controls.Add(this.btn18_1);
            this.groupBox6.Location = new System.Drawing.Point(403, 217);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox6.Size = new System.Drawing.Size(381, 66);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "ライブラリ";
            // 
            // btn18_2
            // 
            this.btn18_2.Location = new System.Drawing.Point(144, 18);
            this.btn18_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn18_2.Name = "btn18_2";
            this.btn18_2.Size = new System.Drawing.Size(98, 37);
            this.btn18_2.TabIndex = 3;
            this.btn18_2.Text = ".NET framework(18-2)";
            this.btn18_2.UseVisualStyleBackColor = true;
            // 
            // btn18_1
            // 
            this.btn18_1.Location = new System.Drawing.Point(10, 18);
            this.btn18_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn18_1.Name = "btn18_1";
            this.btn18_1.Size = new System.Drawing.Size(98, 37);
            this.btn18_1.TabIndex = 3;
            this.btn18_1.Text = ".NET framework(18-1)";
            this.btn18_1.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btn19_2);
            this.groupBox7.Controls.Add(this.btn19_1);
            this.groupBox7.Location = new System.Drawing.Point(405, 295);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox7.Size = new System.Drawing.Size(381, 66);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "列挙型";
            // 
            // btn19_2
            // 
            this.btn19_2.Location = new System.Drawing.Point(144, 18);
            this.btn19_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn19_2.Name = "btn19_2";
            this.btn19_2.Size = new System.Drawing.Size(98, 37);
            this.btn19_2.TabIndex = 3;
            this.btn19_2.Text = "フラグ(19-2)";
            this.btn19_2.UseVisualStyleBackColor = true;
            // 
            // btn19_1
            // 
            this.btn19_1.Location = new System.Drawing.Point(10, 18);
            this.btn19_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn19_1.Name = "btn19_1";
            this.btn19_1.Size = new System.Drawing.Size(98, 37);
            this.btn19_1.TabIndex = 3;
            this.btn19_1.Text = "列挙型の値(19-1)";
            this.btn19_1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btn17_6);
            this.groupBox5.Controls.Add(this.btn17_7);
            this.groupBox5.Controls.Add(this.btn17_5);
            this.groupBox5.Controls.Add(this.btn17_4);
            this.groupBox5.Controls.Add(this.btn17_3);
            this.groupBox5.Controls.Add(this.btn17_2);
            this.groupBox5.Controls.Add(this.btn17_1);
            this.groupBox5.Location = new System.Drawing.Point(410, 19);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(381, 195);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "関数";
            // 
            // btn17_6
            // 
            this.btn17_6.Font = new System.Drawing.Font("MS UI Gothic", 8F);
            this.btn17_6.Location = new System.Drawing.Point(275, 72);
            this.btn17_6.Margin = new System.Windows.Forms.Padding(2);
            this.btn17_6.Name = "btn17_6";
            this.btn17_6.Size = new System.Drawing.Size(98, 37);
            this.btn17_6.TabIndex = 3;
            this.btn17_6.Text = "関数のオーバーロード(17-6)";
            this.btn17_6.UseVisualStyleBackColor = true;
            // 
            // btn17_7
            // 
            this.btn17_7.Font = new System.Drawing.Font("MS UI Gothic", 8F);
            this.btn17_7.Location = new System.Drawing.Point(10, 128);
            this.btn17_7.Margin = new System.Windows.Forms.Padding(2);
            this.btn17_7.Name = "btn17_7";
            this.btn17_7.Size = new System.Drawing.Size(98, 37);
            this.btn17_7.TabIndex = 3;
            this.btn17_7.Text = "expression-bodied~(17-7)";
            this.btn17_7.UseVisualStyleBackColor = true;
            // 
            // btn17_5
            // 
            this.btn17_5.Location = new System.Drawing.Point(144, 72);
            this.btn17_5.Margin = new System.Windows.Forms.Padding(2);
            this.btn17_5.Name = "btn17_5";
            this.btn17_5.Size = new System.Drawing.Size(98, 37);
            this.btn17_5.TabIndex = 3;
            this.btn17_5.Text = "特殊な引数(17-5)";
            this.btn17_5.UseVisualStyleBackColor = true;
            // 
            // btn17_4
            // 
            this.btn17_4.Location = new System.Drawing.Point(10, 72);
            this.btn17_4.Margin = new System.Windows.Forms.Padding(2);
            this.btn17_4.Name = "btn17_4";
            this.btn17_4.Size = new System.Drawing.Size(98, 37);
            this.btn17_4.TabIndex = 3;
            this.btn17_4.Text = "実引数と仮引数(17-4)";
            this.btn17_4.UseVisualStyleBackColor = true;
            // 
            // btn17_3
            // 
            this.btn17_3.Location = new System.Drawing.Point(275, 18);
            this.btn17_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn17_3.Name = "btn17_3";
            this.btn17_3.Size = new System.Drawing.Size(98, 37);
            this.btn17_3.TabIndex = 3;
            this.btn17_3.Text = "オプション引数～(17-3)";
            this.btn17_3.UseVisualStyleBackColor = true;
            // 
            // btn17_2
            // 
            this.btn17_2.Location = new System.Drawing.Point(144, 18);
            this.btn17_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn17_2.Name = "btn17_2";
            this.btn17_2.Size = new System.Drawing.Size(98, 37);
            this.btn17_2.TabIndex = 3;
            this.btn17_2.Text = "引数～(17-2)";
            this.btn17_2.UseVisualStyleBackColor = true;
            // 
            // btn17_1
            // 
            this.btn17_1.Location = new System.Drawing.Point(10, 18);
            this.btn17_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn17_1.Name = "btn17_1";
            this.btn17_1.Size = new System.Drawing.Size(98, 37);
            this.btn17_1.TabIndex = 3;
            this.btn17_1.Text = "関数定義(17-1)";
            this.btn17_1.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(8, 59);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(830, 473);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(822, 447);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "13-19";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox15);
            this.tabPage2.Controls.Add(this.groupBox14);
            this.tabPage2.Controls.Add(this.groupBox13);
            this.tabPage2.Controls.Add(this.groupBox12);
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Controls.Add(this.groupBox11);
            this.tabPage2.Controls.Add(this.groupBox10);
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(822, 447);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "20-27";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.btn26_3);
            this.groupBox14.Controls.Add(this.btn26_2);
            this.groupBox14.Controls.Add(this.btn26_1);
            this.groupBox14.Location = new System.Drawing.Point(420, 129);
            this.groupBox14.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox14.Size = new System.Drawing.Size(381, 66);
            this.groupBox14.TabIndex = 15;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "「雑記」例外の使い方";
            // 
            // btn26_3
            // 
            this.btn26_3.Location = new System.Drawing.Point(271, 18);
            this.btn26_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn26_3.Name = "btn26_3";
            this.btn26_3.Size = new System.Drawing.Size(98, 37);
            this.btn26_3.TabIndex = 3;
            this.btn26_3.Text = "Try Parse パターン(26-3)";
            this.btn26_3.UseVisualStyleBackColor = true;
            // 
            // btn26_2
            // 
            this.btn26_2.Location = new System.Drawing.Point(144, 17);
            this.btn26_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn26_2.Name = "btn26_2";
            this.btn26_2.Size = new System.Drawing.Size(98, 37);
            this.btn26_2.TabIndex = 3;
            this.btn26_2.Text = "Tester-Doer パターン(26-2)";
            this.btn26_2.UseVisualStyleBackColor = true;
            // 
            // btn26_1
            // 
            this.btn26_1.Location = new System.Drawing.Point(10, 18);
            this.btn26_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn26_1.Name = "btn26_1";
            this.btn26_1.Size = new System.Drawing.Size(98, 37);
            this.btn26_1.TabIndex = 3;
            this.btn26_1.Text = "例外の種類(26-1)";
            this.btn26_1.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.btn25_4);
            this.groupBox13.Controls.Add(this.btn25_3);
            this.groupBox13.Controls.Add(this.btn25_2);
            this.groupBox13.Controls.Add(this.btn25_1);
            this.groupBox13.Location = new System.Drawing.Point(420, 10);
            this.groupBox13.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox13.Size = new System.Drawing.Size(381, 105);
            this.groupBox13.TabIndex = 14;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "例外処理";
            // 
            // btn25_4
            // 
            this.btn25_4.Location = new System.Drawing.Point(10, 59);
            this.btn25_4.Margin = new System.Windows.Forms.Padding(2);
            this.btn25_4.Name = "btn25_4";
            this.btn25_4.Size = new System.Drawing.Size(98, 37);
            this.btn25_4.TabIndex = 4;
            this.btn25_4.Text = "例外フィルター(25-4)";
            this.btn25_4.UseVisualStyleBackColor = true;
            // 
            // btn25_3
            // 
            this.btn25_3.Location = new System.Drawing.Point(271, 18);
            this.btn25_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn25_3.Name = "btn25_3";
            this.btn25_3.Size = new System.Drawing.Size(98, 37);
            this.btn25_3.TabIndex = 3;
            this.btn25_3.Text = "例外処理の指針(25-3)";
            this.btn25_3.UseVisualStyleBackColor = true;
            // 
            // btn25_2
            // 
            this.btn25_2.Location = new System.Drawing.Point(144, 17);
            this.btn25_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn25_2.Name = "btn25_2";
            this.btn25_2.Size = new System.Drawing.Size(98, 37);
            this.btn25_2.TabIndex = 3;
            this.btn25_2.Text = "例外クラス(25-2)";
            this.btn25_2.UseVisualStyleBackColor = true;
            // 
            // btn25_1
            // 
            this.btn25_1.Location = new System.Drawing.Point(10, 18);
            this.btn25_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn25_1.Name = "btn25_1";
            this.btn25_1.Size = new System.Drawing.Size(98, 37);
            this.btn25_1.TabIndex = 3;
            this.btn25_1.Text = "例外処理構文ボタン(25-1)";
            this.btn25_1.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.btn24_3);
            this.groupBox12.Controls.Add(this.btn24_2);
            this.groupBox12.Controls.Add(this.btn24_1);
            this.groupBox12.Location = new System.Drawing.Point(9, 334);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox12.Size = new System.Drawing.Size(381, 66);
            this.groupBox12.TabIndex = 13;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "名前空間";
            // 
            // btn24_3
            // 
            this.btn24_3.Location = new System.Drawing.Point(271, 18);
            this.btn24_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn24_3.Name = "btn24_3";
            this.btn24_3.Size = new System.Drawing.Size(98, 37);
            this.btn24_3.TabIndex = 3;
            this.btn24_3.Text = "エイリアス修飾子(24-3)";
            this.btn24_3.UseVisualStyleBackColor = true;
            // 
            // btn24_2
            // 
            this.btn24_2.Location = new System.Drawing.Point(144, 17);
            this.btn24_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn24_2.Name = "btn24_2";
            this.btn24_2.Size = new System.Drawing.Size(98, 37);
            this.btn24_2.TabIndex = 3;
            this.btn24_2.Text = "エイリアス(24-2)";
            this.btn24_2.UseVisualStyleBackColor = true;
            // 
            // btn24_1
            // 
            this.btn24_1.Location = new System.Drawing.Point(10, 18);
            this.btn24_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn24_1.Name = "btn24_1";
            this.btn24_1.Size = new System.Drawing.Size(98, 37);
            this.btn24_1.TabIndex = 3;
            this.btn24_1.Text = "名前空間の使い方(24-1)";
            this.btn24_1.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.btn21_1);
            this.groupBox9.Location = new System.Drawing.Point(9, 88);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox9.Size = new System.Drawing.Size(381, 66);
            this.groupBox9.TabIndex = 12;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "可変長引数";
            // 
            // btn21_1
            // 
            this.btn21_1.Location = new System.Drawing.Point(10, 18);
            this.btn21_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn21_1.Name = "btn21_1";
            this.btn21_1.Size = new System.Drawing.Size(98, 37);
            this.btn21_1.TabIndex = 3;
            this.btn21_1.Text = "params キーワード(21-1)";
            this.btn21_1.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.btn22_3);
            this.groupBox11.Controls.Add(this.btn22_2);
            this.groupBox11.Controls.Add(this.btn22_1);
            this.groupBox11.Location = new System.Drawing.Point(9, 168);
            this.groupBox11.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox11.Size = new System.Drawing.Size(381, 66);
            this.groupBox11.TabIndex = 9;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "オプション引数・名前付き引数";
            // 
            // btn22_3
            // 
            this.btn22_3.Location = new System.Drawing.Point(271, 18);
            this.btn22_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn22_3.Name = "btn22_3";
            this.btn22_3.Size = new System.Drawing.Size(98, 37);
            this.btn22_3.TabIndex = 3;
            this.btn22_3.Text = "内部実装(22-3)";
            this.btn22_3.UseVisualStyleBackColor = true;
            // 
            // btn22_2
            // 
            this.btn22_2.Location = new System.Drawing.Point(144, 17);
            this.btn22_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn22_2.Name = "btn22_2";
            this.btn22_2.Size = new System.Drawing.Size(98, 37);
            this.btn22_2.TabIndex = 3;
            this.btn22_2.Text = "名前付き引数(22-2)";
            this.btn22_2.UseVisualStyleBackColor = true;
            // 
            // btn22_1
            // 
            this.btn22_1.Location = new System.Drawing.Point(10, 18);
            this.btn22_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn22_1.Name = "btn22_1";
            this.btn22_1.Size = new System.Drawing.Size(98, 37);
            this.btn22_1.TabIndex = 3;
            this.btn22_1.Text = "オプション引数(22-1)";
            this.btn22_1.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.btn23_3);
            this.groupBox10.Controls.Add(this.btn23_2);
            this.groupBox10.Controls.Add(this.btn23_1);
            this.groupBox10.Location = new System.Drawing.Point(9, 253);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox10.Size = new System.Drawing.Size(381, 66);
            this.groupBox10.TabIndex = 10;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "コマンドライン引数";
            // 
            // btn23_3
            // 
            this.btn23_3.Location = new System.Drawing.Point(271, 18);
            this.btn23_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn23_3.Name = "btn23_3";
            this.btn23_3.Size = new System.Drawing.Size(98, 37);
            this.btn23_3.TabIndex = 3;
            this.btn23_3.Text = "終了コード(23-3)";
            this.btn23_3.UseVisualStyleBackColor = true;
            // 
            // btn23_2
            // 
            this.btn23_2.Location = new System.Drawing.Point(144, 17);
            this.btn23_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn23_2.Name = "btn23_2";
            this.btn23_2.Size = new System.Drawing.Size(98, 37);
            this.btn23_2.TabIndex = 3;
            this.btn23_2.Text = "C#でコマンドライン~(23-2)";
            this.btn23_2.UseVisualStyleBackColor = true;
            // 
            // btn23_1
            // 
            this.btn23_1.Location = new System.Drawing.Point(10, 18);
            this.btn23_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn23_1.Name = "btn23_1";
            this.btn23_1.Size = new System.Drawing.Size(98, 37);
            this.btn23_1.TabIndex = 3;
            this.btn23_1.Text = "コマンドライン引数とは(23-1)";
            this.btn23_1.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.btn20_1);
            this.groupBox8.Location = new System.Drawing.Point(9, 10);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox8.Size = new System.Drawing.Size(381, 66);
            this.groupBox8.TabIndex = 11;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "データの構造化";
            // 
            // btn20_1
            // 
            this.btn20_1.Location = new System.Drawing.Point(10, 18);
            this.btn20_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn20_1.Name = "btn20_1";
            this.btn20_1.Size = new System.Drawing.Size(98, 37);
            this.btn20_1.TabIndex = 3;
            this.btn20_1.Text = "複合型(20-1)";
            this.btn20_1.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.btn27_2);
            this.groupBox15.Controls.Add(this.btn27_1);
            this.groupBox15.Location = new System.Drawing.Point(420, 208);
            this.groupBox15.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox15.Size = new System.Drawing.Size(381, 66);
            this.groupBox15.TabIndex = 16;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "[雑記]例外のスタックトレース";
            // 
            // btn27_2
            // 
            this.btn27_2.Location = new System.Drawing.Point(144, 17);
            this.btn27_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn27_2.Name = "btn27_2";
            this.btn27_2.Size = new System.Drawing.Size(98, 37);
            this.btn27_2.TabIndex = 3;
            this.btn27_2.Text = "クラス定義(27-2)";
            this.btn27_2.UseVisualStyleBackColor = true;
            // 
            // btn27_1
            // 
            this.btn27_1.Location = new System.Drawing.Point(10, 18);
            this.btn27_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn27_1.Name = "btn27_1";
            this.btn27_1.Size = new System.Drawing.Size(98, 37);
            this.btn27_1.TabIndex = 3;
            this.btn27_1.Text = "スタックトレースと例外(27-1)";
            this.btn27_1.UseVisualStyleBackColor = true;
            // 
            // FrmStructure
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 537);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmStructure";
            this.Text = "FrmStructure";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox14.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn13_1_1;
        private System.Windows.Forms.Button btn13_1_2;
        private System.Windows.Forms.Button btn13_1_3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn14_4;
        private System.Windows.Forms.Button btn14_3;
        private System.Windows.Forms.Button btn14_2;
        private System.Windows.Forms.Button btn14_1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn15_4;
        private System.Windows.Forms.Button btn15_3;
        private System.Windows.Forms.Button btn15_2;
        private System.Windows.Forms.Button btn15_1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btn16_4;
        private System.Windows.Forms.Button btn16_3;
        private System.Windows.Forms.Button btn16_2;
        private System.Windows.Forms.Button btn16_1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btn18_2;
        private System.Windows.Forms.Button btn18_1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btn19_2;
        private System.Windows.Forms.Button btn19_1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btn17_6;
        private System.Windows.Forms.Button btn17_7;
        private System.Windows.Forms.Button btn17_5;
        private System.Windows.Forms.Button btn17_4;
        private System.Windows.Forms.Button btn17_3;
        private System.Windows.Forms.Button btn17_2;
        private System.Windows.Forms.Button btn17_1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button btn24_3;
        private System.Windows.Forms.Button btn24_2;
        private System.Windows.Forms.Button btn24_1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button btn21_1;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button btn22_3;
        private System.Windows.Forms.Button btn22_2;
        private System.Windows.Forms.Button btn22_1;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button btn23_3;
        private System.Windows.Forms.Button btn23_2;
        private System.Windows.Forms.Button btn23_1;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btn20_1;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Button btn25_3;
        private System.Windows.Forms.Button btn25_2;
        private System.Windows.Forms.Button btn25_1;
        private System.Windows.Forms.Button btn25_4;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Button btn26_3;
        private System.Windows.Forms.Button btn26_2;
        private System.Windows.Forms.Button btn26_1;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Button btn27_2;
        private System.Windows.Forms.Button btn27_1;
    }
}