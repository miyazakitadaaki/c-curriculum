﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices; 
using System.IO;
using curriculum.FormMessage.Frm_P層_.表示用Form;
using Ufcpp;
using st24_1 = Ufcpp;
using System.Runtime.CompilerServices;

namespace curriculum1
{
    /// ********************************************************************************
    /// <summary>
    /// 構造化フォーム
    /// </summary>
    /// <remarks>none</remarks>
    /// 
    /// <history>
    /// --------------------------------------------------------------------------------
    /// No     Date         User         Remorks
    /// --------------------------------------------------------------------------------
    /// 0001   2015.12.05   宮崎祥彰   *new*
    /// 
    /// </history>
    /// ********************************************************************************
    public partial class FrmStructure : Base_Form
    {


        #region ++++++++++ member variable ++++++++++
        public enum TopicNo
        {
            Topic14_1 = 0,
            Topic14_2,
            Topic14_3,
            Topic14_4,
            Topic15_1,
            Topic15_2,
            Topic15_3,
            Topic15_4,
            Topic16_1,
            Topic16_2,
            Topic16_3,
            Topic17_1,
            Topic17_2,
            Topic17_3,
            Topic17_4,
            Topic17_5,
            Topic17_6,
            Topic17_7
        }
        #endregion

        #region++++++++++ constructor/destructor ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰     *new*
        /// 
        /// </history>
        /// ********************************************************************************
        public FrmStructure()
        {
            // -------------------------------------------------------
            // 初期化処理
            InitializeComponent();
            this.EntryEvents();
        }

        #endregion

        #region ++++++++++ event ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// イベント Form　Load時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void FormMain_Load(object sender, EventArgs e) {

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = !this.MaximizeBox;
            
        }

        /// ********************************************************************************
        /// <summary>
        /// イベントハンドラ登録
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void EntryEvents(){

            this.Load += new EventHandler(this.FormMain_Load);
            this.btn13_1_1.Click += new EventHandler(this.btn13_1_1_Click);
            this.btn13_1_2.Click += new EventHandler(this.btn13_1_2_Click);
            this.btn13_1_3.Click += new EventHandler(this.btn13_1_3_Click);
            this.btn14_1.Click += new EventHandler(this.btn14_1_Click);
            this.btn14_2.Click += new EventHandler(this.btn14_2_Click);
            this.btn14_3.Click += new EventHandler(this.btn14_3_Click);
            this.btn14_4.Click += new EventHandler(this.btn14_4_Click);
            this.btn15_1.Click += new EventHandler(this.btn15_1_Click);
            this.btn15_2.Click += new EventHandler(this.btn15_2_Click);
            this.btn15_3.Click += new EventHandler(this.btn15_3_Click);
            this.btn15_4.Click += new EventHandler(this.btn15_4_Click);
            this.btn16_1.Click += new EventHandler(this.btn16_1_Click);
            this.btn16_2.Click += new EventHandler(this.btn16_2_Click);
            this.btn16_3.Click += new EventHandler(this.btn16_3_Click);
            this.btn16_4.Click += new EventHandler(this.btn16_4_Click);
            this.btn17_1.Click += new EventHandler(this.btn17_1_Click);
            this.btn17_2.Click += new EventHandler(this.btn17_2_Click);
            this.btn17_3.Click += new EventHandler(this.btn17_3_Click);
            this.btn17_4.Click += new EventHandler(this.btn17_4_Click);
            this.btn17_5.Click += new EventHandler(this.btn17_5_Click);
            this.btn17_6.Click += new EventHandler(this.btn17_6_Click);
            this.btn17_7.Click += new EventHandler(this.btn17_7_Click);
            this.btn18_1.Click += new EventHandler(this.btn18_1_Click);
            this.btn18_2.Click += new EventHandler(this.btn18_2_Click);
            this.btn19_1.Click += new EventHandler(this.btn19_1_Click);
            this.btn19_2.Click += new EventHandler(this.btn19_2_Click);
            this.btn20_1.Click += new EventHandler(this.btn20_1_Click);
            this.btn21_1.Click += new EventHandler(this.btn21_1_Click);
            this.btn22_1.Click += new EventHandler(this.btn22_1_Click);
            this.btn22_2.Click += new EventHandler(this.btn22_2_Click);
            this.btn22_3.Click += new EventHandler(this.btn22_3_Click);
            this.btn23_1.Click += new EventHandler(this.btn23_1_Click);
            this.btn23_2.Click += new EventHandler(this.btn23_2_Click);
            this.btn23_3.Click += new EventHandler(this.btn23_3_Click);
            this.btn24_1.Click += new EventHandler(this.btn24_1_Click);
            this.btn24_2.Click += new EventHandler(this.btn24_2_Click);
            this.btn24_3.Click += new EventHandler(this.btn24_3_Click);
            this.btn25_1.Click += new EventHandler(this.btn25_1_Click);
            this.btn25_2.Click += new EventHandler(this.btn25_2_Click);
            this.btn25_3.Click += new EventHandler(this.btn25_3_Click);
            this.btn25_4.Click += new EventHandler(this.btn25_4_Click);
            this.btn26_1.Click += new EventHandler(this.btn26_1_Click);
            this.btn26_2.Click += new EventHandler(this.btn26_2_Click);
            this.btn26_3.Click += new EventHandler(this.btn26_3_Click);
            this.btn27_1.Click += new EventHandler(this.btn27_1_Click);
            this.btn27_2.Click += new EventHandler(this.btn27_2_Click);

        }



        #region++++++++++ 制御フロー(13-1-1～13-1-3) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 制御フローボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn13_1_1_Click(object sender, EventArgs e)
        {
            //「10個の整数の中から、正の数だけの和を求める」というような処理
            int sum = 0;
            int i = 0;
            int[] a = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            string answer;

            while (i < 10)
            {
                int x = a[i];
                if (x > 0)
                {
                    sum = x + sum;
                    answer = sum.ToString();

                    MessageBox.Show("【while文】" + Environment.NewLine 
                                    + "10個の整数の中から、正の数だけの和を求める" + Environment.NewLine 
                                    + " (1, 2, 3, 4, 5, 6, 7, 8, 9, 10) " + Environment.NewLine 
                                    + "答え：" + answer);
                }
                i = i + 1;
            }
        }

        private void btn13_1_2_Click(object sender, EventArgs e)
        {

            //反復処理は「0 から N-1 まで」とか「1 から N まで」とか、 値を1ずつ増やして繰り返すという処理が多くなります。
            // こういう処理を行うための制御構文として、for 文というものがあります。
            int sum = 0;
            int[] a = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            string answer;

            for (int i = 0; i < 10; ++i)
            {
                int x = a[i];
                if (x > 0)
                {
                    sum = x + sum;
                    answer = sum.ToString();
                    MessageBox.Show("【for文】" + Environment.NewLine
                                    + "10個の整数の中から、正の数だけの和を求める" + Environment.NewLine
                                    + " (1, 2, 3, 4, 5, 6, 7, 8, 9, 10) " + Environment.NewLine
                                    + "答え：" + answer);
                }
            }
        }

        private void btn13_1_3_Click(object sender, EventArgs e)
        {
            //「各要素に対する処理」の為の構文 foreach 
            int sum = 0;
            int[] a = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            string answer;
            foreach (int x in a)
            {
                if (x > 0)
                {
                    sum = x + sum;
                    answer = sum.ToString();
                    MessageBox.Show("【foreach文】" + Environment.NewLine
                                    + "10個の整数の中から、正の数だけの和を求める" + Environment.NewLine
                                    + " (1, 2, 3, 4, 5, 6, 7, 8, 9, 10) " + Environment.NewLine
                                    + "答え：" + answer);
                }
            }
        }

        #endregion

        #region++++++++++ 条件分岐(14-1-1～14-1-4) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// if文ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn14_1_Click(object sender, EventArgs e){

            FormMessage fm14 = new FormMessage(TopicNo.Topic14_1);
            fm14.ShowDialog();
            
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// swich文ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn14_2_Click(object sender, EventArgs e){

            FormMessage fm14 = new FormMessage(TopicNo.Topic14_2);
            fm14.Show();
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// フォールスルーの禁止ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn14_3_Click(object sender, EventArgs e)
        {

            FormMessage fm14 = new FormMessage(TopicNo.Topic14_3);
            fm14.Show();

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// goto 文ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn14_4_Click(object sender, EventArgs e)
        {

            FormMessage fm14 = new FormMessage(TopicNo.Topic14_4);
            fm14.Show();

        }

        #endregion

        #region++++++++++ 反復処理(15-1～15-4) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// while 文ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn15_1_Click(object sender, EventArgs e){

            FormMessage frm15 = new FormMessage(TopicNo.Topic15_1);
            frm15.Show();

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// do-while 文 文ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn15_2_Click(object sender, EventArgs e) {

            FormMessage frm15 = new FormMessage(TopicNo.Topic15_2);
            frm15.Show();

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// for 文ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn15_3_Click(object sender, EventArgs e){

            FormMessage frm15 = new FormMessage(TopicNo.Topic15_3);

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// foreach 文ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn15_4_Click(object sender, EventArgs e){

            FormMessage frm15 = new FormMessage(TopicNo.Topic15_4);

        }

        #endregion

        #region++++++++++ 配列(16-1～16-4) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 暗黙的型付け配列 文ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn16_1_Click(object sender, EventArgs e){

            FormMessage frm16 = new FormMessage(TopicNo.Topic16_1);
            frm16.Show();

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 四角い多次元配列 文ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn16_2_Click(object sender, EventArgs e){

            FormMessage frm16 = new FormMessage(TopicNo.Topic16_2);

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 配列の配列 文ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn16_3_Click(object sender, EventArgs e){

            FormMessage frm16 = new FormMessage(TopicNo.Topic16_3);

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 比較 文ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn16_4_Click(object sender, EventArgs e){
            MessageBox.Show("削除予定");
        


        }

        #endregion

        #region++++++++++ 関数(17-1～17-7) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 関数定義ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn17_1_Click(object sender, EventArgs e)
        {
            FormMessage frm17 = new FormMessage(TopicNo.Topic17_1);
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// 引数が複数ある関数、引数のない関数、戻り値のない関数ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn17_2_Click(object sender, EventArgs e)
        {
            FormMessage frm17 = new FormMessage(TopicNo.Topic17_2);
        }

        

        ///// ********************************************************************************
        ///// <summary>
        ///// オプション引数と名前付き引数ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn17_3_Click(object sender, EventArgs e)
        {
            FormMessage frm17 = new FormMessage(TopicNo.Topic17_3);
        }


        ///// ********************************************************************************
        ///// <summary>
        ///// 実引数と仮引数ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn17_4_Click(object sender, EventArgs e){
            FormMessage frm17 = new FormMessage(TopicNo.Topic17_4);
        }

        

        ///// ********************************************************************************
        ///// <summary>
        ///// 特殊な引数ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn17_5_Click(object sender, EventArgs e){
            MessageBox.Show("別項で参照\nref, out:「引数の参照渡し」\nthis:「拡張メソッド」");

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 関数のオーバーロードボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn17_6_Click(object sender, EventArgs e)
        {
            FormMessage frm17 = new FormMessage(TopicNo.Topic17_6);
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// expression-bodied な関数ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn17_7_Click(object sender, EventArgs e){
        MessageBox.Show("C#6.0を使用する為、実装不可");
        //C# 6.0 では、関数本体の部分が1つの式だけからなる場合、以下のような書き方をすることができるようになりました。
        //これを、expression-bodied (本体が式の)関数(expression-bodied function)と呼びます。
        
        //static ulong Random17_7() => unchecked(seed = seed * 1566083941UL +  1 );
        //static double Norm17_7(double x, double y, double z) => x * x + y * y + z * z;
        }

        #endregion

        #region++++++++++ ライブラリ(18-1～18-2) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// .NET FrameWorkボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn18_1_Click(object sender, EventArgs e){
            for (double x = 0; x < 1; x += 0.1){

                MessageBox.Show("sin("+ x +") = {"+ System.Math.Sin(x) +"}");
                }
        }
        
        ///// ********************************************************************************
        ///// <summary>
        ///// .NET FrameWorkボタン_2　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn18_2_Click(object sender, EventArgs e)
        {
            for (double x = 0; x < 1; x += 0.1)
            {

                MessageBox.Show("sin(" + x + ") = {" + Math.Sin(x) + "}");
            }
        }

        #endregion

        #region++++++++++ 列挙型(19-1～19-2) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 列挙型の値ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn19_1_Click(object sender, EventArgs e){
            Structure_19 frm19 = new Structure_19(); 
            frm19.Structure_19_1();
            
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// フラグボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn19_2_Click(object sender, EventArgs e)
        {
            Structure_19 frm19 = new Structure_19();
            frm19.Structure_19_2();

        }

        #endregion

        #region++++++++++ データの構造化(20-1) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 複合型ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn20_1_Click(object sender, EventArgs e){
            Structure_20 frm20 = new Structure_20();
            frm20.Structure20_1();
        
        }

        #endregion

        #region++++++++++ 可変長引数(21-1) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// params キーワードボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn21_1_Click(object sender, EventArgs e){
            Structure_21 frm21 = new Structure_21();
            frm21.Structure21_1();
        }

        #endregion

        #region++++++++++ オプション引数・名前付き引数(22-1～22-3) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// オプション引数ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn22_1_Click(object sender, EventArgs e){
            Sum(1);
            Sum(1, 2);
            Sum(1, 2, 3);
            Sum(1, 2, 3, 4);

        }

        static int Sum(int x)
        {
            MessageBox.Show("Sum(x)");
            return x;
        }

        static int Sum(int x, int y = 0, int z = 0) // 引数2つ以上でないと呼ばれない
        {
            MessageBox.Show("Sum(x, y, z)");
            return x + y + z;
        }

        static int Sum(params int[] rest) // 引数4つ以上でないと呼ばれない
        {
            MessageBox.Show("Sum(rest)");
            var sum = 0;
            foreach (var v in rest) sum += v;
            return sum;
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 名前付引数ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn22_2_Click(object sender, EventArgs e){
            int s1 = Sum(x: 1, y: 2, z: 3); // Sum(1, 2, 3); と同じ意味。
            int s2 = Sum(y: 1, z: 2, x: 3); // Sum(3, 1, 2); と同じ意味。

            int x = 2;
            MessageBox.Show(string.Format("{0}",Square(x)));
        }

        static int Square(int x)
        {
            return x * x;
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 内部実装ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn22_3_Click(object sender, EventArgs e)
        {   
            int a = 0;
            int b = 0;
            int c = 0;
            MessageBox.Show(string.Format("{0}", Sum_22_3(a, b, c)));
            
        }

        static int Sum_22_3(int x = 0, int y = 0, int z = 0)
        {
            return x + y + z;
        }
        
        #endregion

        #region++++++++++ コマンドライン引数(23-1～23_3) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// コマンドライン引数とはボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn23_1_Click(object sender, EventArgs e)
        {
            //コマンドやプログラムを起動する際に、プログラム名の後に続けて入力した文字列はパラメータとしてプログラムに渡されます。
            // このようなプログラム起動時に渡されるパラメータのことをコマンドライン引数と呼びます。
            //削除予定
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// C#でコマンドライン引数ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn23_2_Click(object sender, EventArgs e)
        {
           string[] a = {"1","2","3","4","5"};
           Structure23_2(a);
        }

        private void Structure23_2(string[] args)
        {
            MessageBox.Show("CmdからbinフォルダのcurriculumForCommandLineを起動してください。コマンドライン引数はtest.txt");
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 終了コードボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn23_3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("CmdからbinフォルダのcurriculumForCommandLineを起動してください。コマンドライン引数はなし");
        }

        #endregion

        #region++++++++++ 名前空間(24-1～24_3) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 名前空間の使い方ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn24_1_Click(object sender, EventArgs e)
        {
            Structure24_1 frm24_1 = new Structure24_1();
            MessageBox.Show(frm24_1.Structure24());
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// エイリアスボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn24_2_Click(object sender, EventArgs e)
        {
            st24_1.Structure24_1 frm24_1 = new st24_1.Structure24_1();
            MessageBox.Show(frm24_1.Structure24());
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// エイリアス修飾子ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn24_3_Click(object sender, EventArgs e)
        {
            st24_1::Structure24_1 frm24_1 = new st24_1::Structure24_1();
            MessageBox.Show(frm24_1.Structure24());
            
        }
       
        #endregion

        #region++++++++++ 例外処理(25-1～25_4) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 例外処理構文ボタン　クリックイベント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn25_1_Click(object sender, EventArgs e)
        {

            try
            {
                Console.Write("{0}\n", int.Parse("12345"));
                Console.Write("{0}\n", int.Parse("12a45"));
                //↑ ここで FormatException 例外が投げられる。
            }
            catch (FormatException)
            {
                MessageBox.Show("想定外の文字列が入力されました");
            }
            


        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 標準で用意されている例外クラス
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn25_2_Click(object sender, EventArgs e)
        {
        /*
        System 名前空間
        ArgumentException	メソッドの引数が変な場合。ArgumentNullExceptionやArgumentOutOfRangeException以外の場合で変な時に使う。
        ArgumentNullException	引数がnullの場合。
        ArgumentOutOfRangeException	メソッドの許容範囲外の値が引数として渡された場合。
        ArithmeticException	算術演算によるエラーの基本クラス。OverflowException, DivideByZeroException, NotFiniteNumberException以外の算術エラーを示したければ使う。
        OverflowException	算術演算やキャストでオーバーフローが起きた場合。
        DivideByZeroException	0で割ったときのエラー。
        NotFiniteNumberException	浮動小数点値が無限大の場合。
        FormatException	引数の書式が仕様に一致していない場合。
        IndexOutOfRangeException	配列のインデックスが変な場合。
        InvalidCastException	無効なキャストの場合。
        InvalidOperationException	引数以外の原因でエラーが起きた場合。
        ObjectDisposedException	Dispose済みのオブジェクトで操作が実行される場合。
        NotImplementedException	メソッドが未実装の場合。
        NotSupportedException	呼び出されたメソッドがサポートされていない場合、または呼び出された機能を備えていないストリームに対して読み取り、シーク、書き込みが試行された場合。
        NullReferenceException	nullオブジェクト参照を逆参照しようとした場合。
        PlatformNotSupportException	特定のプラットフォームで機能が実行されない場合。
        TimeoutException	指定したタイムアウト時間が経過した場合。
        System.Collections.Generics 名前空間
        KeyNotFoundException	コレクションに該当するキーが無い場合。
        System.IO 名前空間
        DirectoryNotFoundException	ディレクトリが無い場合。
        FileNotFoundException	ファイルが無い場合。
        EndOfStreamException	ストリームの末尾を超えて読み込もうとしている場合。
        */
            try
            {
                readExceptionFile();
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("ファイルがありません");
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 例外処理の指針
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn25_3_Click(object sender, EventArgs e)
        {
            /*
             *一般に、tyr-catch を用いた例外処理は、 if 文などを使った値のチェックに比べて、 実行速度が遅いといわれています。 try ブロックで囲んだだけでは（例外が発生しなければ）ほとんどオーバーヘッドはないのですが、 例外発生時には少し大き目のコストが発生します。
            このコストを考えても、try-catch を用いるメリットの方が大きいのですが、 まあ、避けれるのならばコストの大きな処理は避けたいというのがプログラマの心情というものです。
            ということで、例外の使い方（特に、避けれる例外を避ける方法）について説明をします。
            26「雑記」例外の使い方にてコードを記述する為削除予定。
            */
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 例外フィルター
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.05   宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn25_4_Click(object sender, EventArgs e)
        {
            //C#
            //try
            //{
            //    F();
            //}
            //catch (Exception e) when (e is DirectoryNotFoundException || e is FileNotFoundException)
            //{
            //    // DirectoryNotFoundException のときと FileNotFoundException の時で
            //    // 全く同じ例外処理の仕方をしたい場合がある。
            //    Console.WriteLine(e);
            //}
            try
            {
                readExceptionFile();
            }
            catch (Exception ex)
                {
                    if ((ex is DirectoryNotFoundException || ex is FileNotFoundException))
                    {
                        MessageBox.Show(string.Format("{0}", ex));
                    }
                }
        }

        private void readExceptionFile(){
            StreamReader reader = null;
            reader = new StreamReader("a");
        }
        
        #endregion

        #region++++++++++ 「雑記」例外の使い方(26-1～26_3) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 例外の種類
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.23  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn26_1_Click(object sender, EventArgs e)
        {
            String text;
            try
            {
                if (!File.Exists("a"))
                {
                    text = "デフォルトのテキスト";
                    MessageBox.Show(text);
                }
                else
                {
                    text = File.ReadAllText("a");
                }
            }
            catch (FileNotFoundException)
            {
                text = "ファイルがなくてもデフォルトのテキストがあれば OK";
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// Tester-Doer パターン
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.23  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn26_2_Click(object sender, EventArgs e)
        {
            FileStream fs = new FileStream("MyFile.txt", FileMode.OpenOrCreate, FileAccess.Read);
            if (fs.CanRead && fs.CanWrite)
            {
                MessageBox.Show("MyFile.txt can be both written to and read from.");
            }
            else if (fs.CanRead)
            {
                MessageBox.Show("MyFile.txt is not writable.");
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// Try Parse パターン
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.23  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn26_3_Click(object sender, EventArgs e)
        {
            String text = "a";
            try
            {
                int x = int.Parse(text);
            }
            catch (Exception)
            {
                MessageBox.Show("エラーが発生しました。");
            }
            // 通常の Parse。変換できない場合は FormatException が発生。
            

            // TryParse。例外が発生しない代わりに、見てのとおり書き方がちょっとうっとおしい。
            int y;
            if (!int.TryParse(text, out y)) y = 0;
            MessageBox.Show(string.Format("エラーをは発生させずにyに{0}を代入します。",y));
        }

        #endregion

        #region++++++++++ [雑記] 例外のスタックトレース(27-1～27_2) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// スタックトレースと例外
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.23  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn27_1_Click(object sender, EventArgs e)
        {
            MethodBtn27_1();
        }
        static void MethodBtn27_1()
        {
            Show(X);
            Show(A);
            Show(B);
            Show(C);
        }

        static void X()
        {
            throw new Exception("throw from X ");
        }

        static void A() { X(); }
        static void B() { X(); }
        static void C() { X(); }

        static void Show(Action a)
        {
            try
            {
                MessageBox.Show("**** " + a.Method.Name + " ****");
                a();
            }
            catch (Exception ex)
            {
                Show(ex);
            }
        }

        static void Show(Exception ex)
        {
            MessageBox.Show("message: " + ex.Message);
            MessageBox.Show("stack trace: ");
            MessageBox.Show(ex.StackTrace);
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// クラス定義
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2015.12.23  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn27_2_Click(object sender, EventArgs e)
        {
            MethodBtn27_2();
        }
        static void MethodBtn27_2()
        {
            Show(ThrowEx_27_2);
            Show(ThrowWithInnerException);
            Show(Rethrow);
        }

        static void ThrowEx_27_2()
        {
            try
            {
                X_27_2();
            }
            catch (Exception ex)
            {
                throw ex;  // 21 行目
            }
        }

        static void ThrowWithInnerException()
        {
            try
            {
                X_27_2();
            }
            catch (Exception ex)
            {
                throw new Exception("throw at line " + GetLineNumber(), ex); // 33 行目
            }
        }

        static void Rethrow()
        {
            try
            {
                X_27_2();
            }
            catch (Exception)
            {
                throw;   // 45 行目
            }
        }

        static void X_27_2()
        {
            throw new Exception("throw from X at line " + GetLineNumber()); // 51 行目
        }

        static int GetLineNumber([CallerLineNumber] int line = 0) { return line; }

        static void Show_27_2(Action a)
        {
            try
            {
                MessageBox.Show("**** " + a.Method.Name + " ****");
                a();
            }
            catch (Exception ex)
            {
                ShowRecursively(ex);
            }
        }

        static void ShowRecursively(Exception ex, int rank = 0)
        {
            MessageBox.Show("rank: " + rank);
            MessageBox.Show("message: " + ex.Message);
            MessageBox.Show("stack trace: ");
            MessageBox.Show(ex.StackTrace);

            if (ex.InnerException != null)
                ShowRecursively(ex.InnerException, rank + 1);
        }

        #endregion

        #endregion
    }
    
    


}
