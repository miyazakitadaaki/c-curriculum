﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace curriculum1
{

    class Structure_21
    {
        public void Structure21_1(){
            int a = 314, b = 159, c = 265, d = 358, e = 979;
            // ↑こいつらの最大値を探したいとき、
            
            int max = Max(a, b, c, d, e);
            // ↑こうすると、自動的に配列を作って値を格納してくれる。

            MessageBox.Show(string.Format("{0}\n", max));
        }
        

        static int Max(params int[] a)
        {
            int max = a[0];
            for (int i = 1; i < a.Length; ++i)
            {
                if (max < a[i])
                    max = a[i];
            }
            return max;
        }
    }
}
