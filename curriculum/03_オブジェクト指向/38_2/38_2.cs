﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace curriculum1.Object
{
    public class C38_2 : System.IComparable
    {
        private string _name;
        //String型のプロパティ
        public string Name
        {
            get
            {
                return this._name;
            }
        }

        private int _price;
        //Int32型のプロパティ
        public int Price
        {
            get
            {
                return this._price;
            }
        }

        //コンストラクタ
        public C38_2(string C38_2Name, int C38_2Price)
        {
            this._name = C38_2Name;
            this._price = C38_2Price;
        }

        //自分自身がobjより小さいときはマイナスの数、大きいときはプラスの数、
        //同じときは0を返す
        public int CompareTo(object obj)
        {
            //nullより大きい
            if (obj == null)
            {
                return 1;
            }

            //違う型とは比較できない
            if (this.GetType() != obj.GetType())
            {
                throw new ArgumentException("別の型とは比較できません。", "obj");
            }
            //このクラスが継承されることが無い（構造体など）ならば、次のようにできる
            //if (!(other is TestClass)) { }

            //Priceを比較する
            return this.Price.CompareTo(((C38_2)obj).Price);
            //または、次のようにもできる
            //return this.Price - ((C38_2)other).Price;
        }
    }
}
