﻿namespace curriculum1
{
    partial class FrmObject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn28_1 = new System.Windows.Forms.Button();
            this.btn28_2 = new System.Windows.Forms.Button();
            this.btn28_3 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn28_6 = new System.Windows.Forms.Button();
            this.btn28_5 = new System.Windows.Forms.Button();
            this.btn28_4 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btn34_2 = new System.Windows.Forms.Button();
            this.btn34_1 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btn33_2 = new System.Windows.Forms.Button();
            this.btn33_1 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btn32_3 = new System.Windows.Forms.Button();
            this.btn32_2 = new System.Windows.Forms.Button();
            this.btn32_1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn30_2 = new System.Windows.Forms.Button();
            this.btn30_1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn31_5 = new System.Windows.Forms.Button();
            this.btn31_4 = new System.Windows.Forms.Button();
            this.btn31_3 = new System.Windows.Forms.Button();
            this.btn31_2 = new System.Windows.Forms.Button();
            this.btn31_1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn29_7 = new System.Windows.Forms.Button();
            this.btn29_6 = new System.Windows.Forms.Button();
            this.btn29_5 = new System.Windows.Forms.Button();
            this.btn29_4 = new System.Windows.Forms.Button();
            this.btn29_3 = new System.Windows.Forms.Button();
            this.btn29_2 = new System.Windows.Forms.Button();
            this.btn29_1 = new System.Windows.Forms.Button();
            this.btn38_7 = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.btn40_2 = new System.Windows.Forms.Button();
            this.btn40_1 = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.btn39_7 = new System.Windows.Forms.Button();
            this.btn39_6 = new System.Windows.Forms.Button();
            this.btn39_5 = new System.Windows.Forms.Button();
            this.btn39_4 = new System.Windows.Forms.Button();
            this.btn39_3 = new System.Windows.Forms.Button();
            this.btn39_2 = new System.Windows.Forms.Button();
            this.btn39_1 = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.btn38_4 = new System.Windows.Forms.Button();
            this.btn38_5 = new System.Windows.Forms.Button();
            this.btn38_3 = new System.Windows.Forms.Button();
            this.btn38_2 = new System.Windows.Forms.Button();
            this.btn38_1 = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.btn37_2 = new System.Windows.Forms.Button();
            this.btn37_1 = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btn36_7 = new System.Windows.Forms.Button();
            this.btn36_6 = new System.Windows.Forms.Button();
            this.btn36_5 = new System.Windows.Forms.Button();
            this.btn36_4 = new System.Windows.Forms.Button();
            this.btn36_3 = new System.Windows.Forms.Button();
            this.btn36_2 = new System.Windows.Forms.Button();
            this.btn36_1 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btn35_1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.btn38_7.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(100, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(467, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "オブジェクト指向(28-1_40-2)";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1134, 66);
            this.panel1.TabIndex = 2;
            // 
            // btn28_1
            // 
            this.btn28_1.Location = new System.Drawing.Point(13, 22);
            this.btn28_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn28_1.Name = "btn28_1";
            this.btn28_1.Size = new System.Drawing.Size(131, 46);
            this.btn28_1.TabIndex = 3;
            this.btn28_1.Text = "クラスとインスタンス(28-1)";
            this.btn28_1.UseVisualStyleBackColor = true;
            // 
            // btn28_2
            // 
            this.btn28_2.Location = new System.Drawing.Point(192, 22);
            this.btn28_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn28_2.Name = "btn28_2";
            this.btn28_2.Size = new System.Drawing.Size(131, 46);
            this.btn28_2.TabIndex = 3;
            this.btn28_2.Text = "クラス定義(28-2)";
            this.btn28_2.UseVisualStyleBackColor = true;
            // 
            // btn28_3
            // 
            this.btn28_3.Location = new System.Drawing.Point(367, 22);
            this.btn28_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn28_3.Name = "btn28_3";
            this.btn28_3.Size = new System.Drawing.Size(131, 46);
            this.btn28_3.TabIndex = 3;
            this.btn28_3.Text = "クラスの利用(28-3)";
            this.btn28_3.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn28_6);
            this.groupBox1.Controls.Add(this.btn28_5);
            this.groupBox1.Controls.Add(this.btn28_4);
            this.groupBox1.Controls.Add(this.btn28_3);
            this.groupBox1.Controls.Add(this.btn28_2);
            this.groupBox1.Controls.Add(this.btn28_1);
            this.groupBox1.Location = new System.Drawing.Point(19, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(508, 142);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "制御フロー";
            // 
            // btn28_6
            // 
            this.btn28_6.Location = new System.Drawing.Point(367, 79);
            this.btn28_6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn28_6.Name = "btn28_6";
            this.btn28_6.Size = new System.Drawing.Size(131, 46);
            this.btn28_6.TabIndex = 9;
            this.btn28_6.Text = "匿名型(28-6)";
            this.btn28_6.UseVisualStyleBackColor = true;
            // 
            // btn28_5
            // 
            this.btn28_5.Location = new System.Drawing.Point(192, 79);
            this.btn28_5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn28_5.Name = "btn28_5";
            this.btn28_5.Size = new System.Drawing.Size(131, 46);
            this.btn28_5.TabIndex = 4;
            this.btn28_5.Text = "メソッドの実装の分離(28-5)";
            this.btn28_5.UseVisualStyleBackColor = true;
            // 
            // btn28_4
            // 
            this.btn28_4.Location = new System.Drawing.Point(13, 79);
            this.btn28_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn28_4.Name = "btn28_4";
            this.btn28_4.Size = new System.Drawing.Size(131, 46);
            this.btn28_4.TabIndex = 5;
            this.btn28_4.Text = "クラスの分割定義(28-4)";
            this.btn28_4.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.btn38_7);
            this.tabControl1.Location = new System.Drawing.Point(11, 74);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1107, 591);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(1099, 562);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "28-34";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btn34_2);
            this.groupBox7.Controls.Add(this.btn34_1);
            this.groupBox7.Location = new System.Drawing.Point(559, 399);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Size = new System.Drawing.Size(508, 77);
            this.groupBox7.TabIndex = 9;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "インデクサー";
            // 
            // btn34_2
            // 
            this.btn34_2.Location = new System.Drawing.Point(197, 18);
            this.btn34_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn34_2.Name = "btn34_2";
            this.btn34_2.Size = new System.Drawing.Size(187, 46);
            this.btn34_2.TabIndex = 3;
            this.btn34_2.Text = "set/get で異なるアクセスレベルを設定(34-2)";
            this.btn34_2.UseVisualStyleBackColor = true;
            // 
            // btn34_1
            // 
            this.btn34_1.Location = new System.Drawing.Point(11, 18);
            this.btn34_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn34_1.Name = "btn34_1";
            this.btn34_1.Size = new System.Drawing.Size(131, 46);
            this.btn34_1.TabIndex = 3;
            this.btn34_1.Text = "インデクサーの定義(34-1)";
            this.btn34_1.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btn33_2);
            this.groupBox6.Controls.Add(this.btn33_1);
            this.groupBox6.Location = new System.Drawing.Point(559, 287);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(508, 77);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "演算子のオーバーロード";
            // 
            // btn33_2
            // 
            this.btn33_2.Location = new System.Drawing.Point(197, 18);
            this.btn33_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn33_2.Name = "btn33_2";
            this.btn33_2.Size = new System.Drawing.Size(131, 46);
            this.btn33_2.TabIndex = 3;
            this.btn33_2.Text = "オーバーロード可能な演算子(33-2)";
            this.btn33_2.UseVisualStyleBackColor = true;
            // 
            // btn33_1
            // 
            this.btn33_1.Location = new System.Drawing.Point(11, 18);
            this.btn33_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn33_1.Name = "btn33_1";
            this.btn33_1.Size = new System.Drawing.Size(131, 46);
            this.btn33_1.TabIndex = 3;
            this.btn33_1.Text = "演算子のオーバーロードの方法(33-1)";
            this.btn33_1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btn32_3);
            this.groupBox5.Controls.Add(this.btn32_2);
            this.groupBox5.Controls.Add(this.btn32_1);
            this.groupBox5.Location = new System.Drawing.Point(559, 183);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(508, 77);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "静的メンバー";
            // 
            // btn32_3
            // 
            this.btn32_3.Location = new System.Drawing.Point(356, 18);
            this.btn32_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn32_3.Name = "btn32_3";
            this.btn32_3.Size = new System.Drawing.Size(131, 46);
            this.btn32_3.TabIndex = 3;
            this.btn32_3.Text = "拡張メソッド(32-3)";
            this.btn32_3.UseVisualStyleBackColor = true;
            // 
            // btn32_2
            // 
            this.btn32_2.Location = new System.Drawing.Point(197, 18);
            this.btn32_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn32_2.Name = "btn32_2";
            this.btn32_2.Size = new System.Drawing.Size(131, 46);
            this.btn32_2.TabIndex = 3;
            this.btn32_2.Text = "静的クラス(32-2)";
            this.btn32_2.UseVisualStyleBackColor = true;
            // 
            // btn32_1
            // 
            this.btn32_1.Location = new System.Drawing.Point(11, 18);
            this.btn32_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn32_1.Name = "btn32_1";
            this.btn32_1.Size = new System.Drawing.Size(131, 46);
            this.btn32_1.TabIndex = 3;
            this.btn32_1.Text = "静的コンストラクター(32-1)";
            this.btn32_1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn30_2);
            this.groupBox4.Controls.Add(this.btn30_1);
            this.groupBox4.Location = new System.Drawing.Point(19, 392);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(508, 84);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "実装と隠蔽";
            // 
            // btn30_2
            // 
            this.btn30_2.Location = new System.Drawing.Point(192, 22);
            this.btn30_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn30_2.Name = "btn30_2";
            this.btn30_2.Size = new System.Drawing.Size(131, 46);
            this.btn30_2.TabIndex = 3;
            this.btn30_2.Text = "実装の隠蔽(30-2)";
            this.btn30_2.UseVisualStyleBackColor = true;
            // 
            // btn30_1
            // 
            this.btn30_1.Location = new System.Drawing.Point(13, 22);
            this.btn30_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn30_1.Name = "btn30_1";
            this.btn30_1.Size = new System.Drawing.Size(131, 46);
            this.btn30_1.TabIndex = 3;
            this.btn30_1.Text = "アクセシビリティ(30-1)";
            this.btn30_1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn31_5);
            this.groupBox3.Controls.Add(this.btn31_4);
            this.groupBox3.Controls.Add(this.btn31_3);
            this.groupBox3.Controls.Add(this.btn31_2);
            this.groupBox3.Controls.Add(this.btn31_1);
            this.groupBox3.Location = new System.Drawing.Point(548, 15);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(508, 142);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "プロパティ";
            // 
            // btn31_5
            // 
            this.btn31_5.Location = new System.Drawing.Point(197, 79);
            this.btn31_5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn31_5.Name = "btn31_5";
            this.btn31_5.Size = new System.Drawing.Size(131, 46);
            this.btn31_5.TabIndex = 6;
            this.btn31_5.Text = "expression-bodied なプロパティ(31-4)";
            this.btn31_5.UseVisualStyleBackColor = true;
            // 
            // btn31_4
            // 
            this.btn31_4.Location = new System.Drawing.Point(11, 79);
            this.btn31_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn31_4.Name = "btn31_4";
            this.btn31_4.Size = new System.Drawing.Size(131, 46);
            this.btn31_4.TabIndex = 6;
            this.btn31_4.Text = "get-only プロパティ(31-4)";
            this.btn31_4.UseVisualStyleBackColor = true;
            // 
            // btn31_3
            // 
            this.btn31_3.Location = new System.Drawing.Point(356, 18);
            this.btn31_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn31_3.Name = "btn31_3";
            this.btn31_3.Size = new System.Drawing.Size(131, 46);
            this.btn31_3.TabIndex = 3;
            this.btn31_3.Text = "自動プロパティ(31-3)";
            this.btn31_3.UseVisualStyleBackColor = true;
            // 
            // btn31_2
            // 
            this.btn31_2.Location = new System.Drawing.Point(197, 18);
            this.btn31_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn31_2.Name = "btn31_2";
            this.btn31_2.Size = new System.Drawing.Size(131, 46);
            this.btn31_2.TabIndex = 3;
            this.btn31_2.Text = "set/get アクセスレベル(31-2)";
            this.btn31_2.UseVisualStyleBackColor = true;
            // 
            // btn31_1
            // 
            this.btn31_1.Location = new System.Drawing.Point(11, 18);
            this.btn31_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn31_1.Name = "btn31_1";
            this.btn31_1.Size = new System.Drawing.Size(131, 46);
            this.btn31_1.TabIndex = 3;
            this.btn31_1.Text = "プロパティとは(31-1)";
            this.btn31_1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn29_7);
            this.groupBox2.Controls.Add(this.btn29_6);
            this.groupBox2.Controls.Add(this.btn29_5);
            this.groupBox2.Controls.Add(this.btn29_4);
            this.groupBox2.Controls.Add(this.btn29_3);
            this.groupBox2.Controls.Add(this.btn29_2);
            this.groupBox2.Controls.Add(this.btn29_1);
            this.groupBox2.Location = new System.Drawing.Point(19, 174);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(508, 190);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "コンストラクタとデストラクター";
            // 
            // btn29_7
            // 
            this.btn29_7.Location = new System.Drawing.Point(11, 132);
            this.btn29_7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn29_7.Name = "btn29_7";
            this.btn29_7.Size = new System.Drawing.Size(131, 46);
            this.btn29_7.TabIndex = 9;
            this.btn29_7.Text = "注意: Finalize(29-7)";
            this.btn29_7.UseVisualStyleBackColor = true;
            // 
            // btn29_6
            // 
            this.btn29_6.Location = new System.Drawing.Point(333, 75);
            this.btn29_6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn29_6.Name = "btn29_6";
            this.btn29_6.Size = new System.Drawing.Size(169, 46);
            this.btn29_6.TabIndex = 4;
            this.btn29_6.Text = "注意: デストラクターの呼び出しタイミング(29-6)";
            this.btn29_6.UseVisualStyleBackColor = true;
            // 
            // btn29_5
            // 
            this.btn29_5.Location = new System.Drawing.Point(188, 75);
            this.btn29_5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn29_5.Name = "btn29_5";
            this.btn29_5.Size = new System.Drawing.Size(131, 46);
            this.btn29_5.TabIndex = 5;
            this.btn29_5.Text = "デストラクター(29-5)";
            this.btn29_5.UseVisualStyleBackColor = true;
            // 
            // btn29_4
            // 
            this.btn29_4.Location = new System.Drawing.Point(11, 75);
            this.btn29_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn29_4.Name = "btn29_4";
            this.btn29_4.Size = new System.Drawing.Size(131, 46);
            this.btn29_4.TabIndex = 6;
            this.btn29_4.Text = "オブジェクト初期化子(29-4)";
            this.btn29_4.UseVisualStyleBackColor = true;
            // 
            // btn29_3
            // 
            this.btn29_3.Location = new System.Drawing.Point(356, 18);
            this.btn29_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn29_3.Name = "btn29_3";
            this.btn29_3.Size = new System.Drawing.Size(131, 46);
            this.btn29_3.TabIndex = 3;
            this.btn29_3.Text = "コンストラクター初期化子(29-3)";
            this.btn29_3.UseVisualStyleBackColor = true;
            // 
            // btn29_2
            // 
            this.btn29_2.Location = new System.Drawing.Point(188, 18);
            this.btn29_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn29_2.Name = "btn29_2";
            this.btn29_2.Size = new System.Drawing.Size(131, 46);
            this.btn29_2.TabIndex = 3;
            this.btn29_2.Text = "変数初期化子(29-2)";
            this.btn29_2.UseVisualStyleBackColor = true;
            // 
            // btn29_1
            // 
            this.btn29_1.Location = new System.Drawing.Point(11, 18);
            this.btn29_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn29_1.Name = "btn29_1";
            this.btn29_1.Size = new System.Drawing.Size(131, 46);
            this.btn29_1.TabIndex = 3;
            this.btn29_1.Text = "コンストラクター(29-1)";
            this.btn29_1.UseVisualStyleBackColor = true;
            // 
            // btn38_7
            // 
            this.btn38_7.BackColor = System.Drawing.SystemColors.Control;
            this.btn38_7.Controls.Add(this.groupBox13);
            this.btn38_7.Controls.Add(this.groupBox12);
            this.btn38_7.Controls.Add(this.groupBox11);
            this.btn38_7.Controls.Add(this.groupBox10);
            this.btn38_7.Controls.Add(this.groupBox9);
            this.btn38_7.Controls.Add(this.groupBox8);
            this.btn38_7.Location = new System.Drawing.Point(4, 25);
            this.btn38_7.Margin = new System.Windows.Forms.Padding(4);
            this.btn38_7.Name = "btn38_7";
            this.btn38_7.Padding = new System.Windows.Forms.Padding(4);
            this.btn38_7.Size = new System.Drawing.Size(1099, 562);
            this.btn38_7.TabIndex = 1;
            this.btn38_7.Text = "35-40";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.btn40_2);
            this.groupBox13.Controls.Add(this.btn40_1);
            this.groupBox13.Location = new System.Drawing.Point(564, 221);
            this.groupBox13.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox13.Size = new System.Drawing.Size(508, 77);
            this.groupBox13.TabIndex = 13;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "多様性";
            // 
            // btn40_2
            // 
            this.btn40_2.Location = new System.Drawing.Point(188, 18);
            this.btn40_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn40_2.Name = "btn40_2";
            this.btn40_2.Size = new System.Drawing.Size(131, 46);
            this.btn40_2.TabIndex = 4;
            this.btn40_2.Text = "ダウンキャスト(40-2)";
            this.btn40_2.UseVisualStyleBackColor = true;
            // 
            // btn40_1
            // 
            this.btn40_1.Location = new System.Drawing.Point(11, 18);
            this.btn40_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn40_1.Name = "btn40_1";
            this.btn40_1.Size = new System.Drawing.Size(131, 46);
            this.btn40_1.TabIndex = 3;
            this.btn40_1.Text = "静的な型、動的な型(40-1)";
            this.btn40_1.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.btn39_7);
            this.groupBox12.Controls.Add(this.btn39_6);
            this.groupBox12.Controls.Add(this.btn39_5);
            this.groupBox12.Controls.Add(this.btn39_4);
            this.groupBox12.Controls.Add(this.btn39_3);
            this.groupBox12.Controls.Add(this.btn39_2);
            this.groupBox12.Controls.Add(this.btn39_1);
            this.groupBox12.Location = new System.Drawing.Point(564, 14);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox12.Size = new System.Drawing.Size(508, 190);
            this.groupBox12.TabIndex = 12;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "継承";
            // 
            // btn39_7
            // 
            this.btn39_7.Location = new System.Drawing.Point(11, 132);
            this.btn39_7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn39_7.Name = "btn39_7";
            this.btn39_7.Size = new System.Drawing.Size(131, 46);
            this.btn39_7.TabIndex = 9;
            this.btn39_7.Text = "sealed(39-7)";
            this.btn39_7.UseVisualStyleBackColor = true;
            // 
            // btn39_6
            // 
            this.btn39_6.Location = new System.Drawing.Point(333, 75);
            this.btn39_6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn39_6.Name = "btn39_6";
            this.btn39_6.Size = new System.Drawing.Size(169, 46);
            this.btn39_6.TabIndex = 4;
            this.btn39_6.Text = "基底クラスのメンバーの隠蔽(39-6)";
            this.btn39_6.UseVisualStyleBackColor = true;
            // 
            // btn39_5
            // 
            this.btn39_5.Location = new System.Drawing.Point(188, 75);
            this.btn39_5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn39_5.Name = "btn39_5";
            this.btn39_5.Size = new System.Drawing.Size(131, 46);
            this.btn39_5.TabIndex = 5;
            this.btn39_5.Text = "protected(39-5)";
            this.btn39_5.UseVisualStyleBackColor = true;
            // 
            // btn39_4
            // 
            this.btn39_4.Location = new System.Drawing.Point(11, 75);
            this.btn39_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn39_4.Name = "btn39_4";
            this.btn39_4.Size = new System.Drawing.Size(171, 53);
            this.btn39_4.TabIndex = 6;
            this.btn39_4.Text = "基底クラスのコンストラクタを明示的に呼び出す(39-4)";
            this.btn39_4.UseVisualStyleBackColor = true;
            // 
            // btn39_3
            // 
            this.btn39_3.Location = new System.Drawing.Point(356, 18);
            this.btn39_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn39_3.Name = "btn39_3";
            this.btn39_3.Size = new System.Drawing.Size(131, 46);
            this.btn39_3.TabIndex = 3;
            this.btn39_3.Text = "コンストラクタ呼出し(39-3)";
            this.btn39_3.UseVisualStyleBackColor = true;
            // 
            // btn39_2
            // 
            this.btn39_2.Location = new System.Drawing.Point(188, 18);
            this.btn39_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn39_2.Name = "btn39_2";
            this.btn39_2.Size = new System.Drawing.Size(131, 46);
            this.btn39_2.TabIndex = 3;
            this.btn39_2.Text = "object型(39-2)";
            this.btn39_2.UseVisualStyleBackColor = true;
            // 
            // btn39_1
            // 
            this.btn39_1.Location = new System.Drawing.Point(11, 18);
            this.btn39_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn39_1.Name = "btn39_1";
            this.btn39_1.Size = new System.Drawing.Size(131, 46);
            this.btn39_1.TabIndex = 3;
            this.btn39_1.Text = "クラスの継承(39-1)";
            this.btn39_1.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.btn38_4);
            this.groupBox11.Controls.Add(this.btn38_5);
            this.groupBox11.Controls.Add(this.btn38_3);
            this.groupBox11.Controls.Add(this.btn38_2);
            this.groupBox11.Controls.Add(this.btn38_1);
            this.groupBox11.Location = new System.Drawing.Point(16, 407);
            this.groupBox11.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox11.Size = new System.Drawing.Size(506, 140);
            this.groupBox11.TabIndex = 12;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "インターフェース";
            // 
            // btn38_4
            // 
            this.btn38_4.Location = new System.Drawing.Point(9, 81);
            this.btn38_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn38_4.Name = "btn38_4";
            this.btn38_4.Size = new System.Drawing.Size(131, 46);
            this.btn38_4.TabIndex = 9;
            this.btn38_4.Text = "IDisposable(38-4)";
            this.btn38_4.UseVisualStyleBackColor = true;
            // 
            // btn38_5
            // 
            this.btn38_5.Location = new System.Drawing.Point(186, 81);
            this.btn38_5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn38_5.Name = "btn38_5";
            this.btn38_5.Size = new System.Drawing.Size(131, 46);
            this.btn38_5.TabIndex = 6;
            this.btn38_5.Text = "明示的実装(38-5)";
            this.btn38_5.UseVisualStyleBackColor = true;
            // 
            // btn38_3
            // 
            this.btn38_3.Location = new System.Drawing.Point(356, 18);
            this.btn38_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn38_3.Name = "btn38_3";
            this.btn38_3.Size = new System.Drawing.Size(144, 46);
            this.btn38_3.TabIndex = 3;
            this.btn38_3.Text = "コレクション(38-3)";
            this.btn38_3.UseVisualStyleBackColor = true;
            // 
            // btn38_2
            // 
            this.btn38_2.Location = new System.Drawing.Point(166, 18);
            this.btn38_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn38_2.Name = "btn38_2";
            this.btn38_2.Size = new System.Drawing.Size(170, 46);
            this.btn38_2.TabIndex = 3;
            this.btn38_2.Text = "IComparable(38-2)";
            this.btn38_2.UseVisualStyleBackColor = true;
            // 
            // btn38_1
            // 
            this.btn38_1.Location = new System.Drawing.Point(11, 18);
            this.btn38_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn38_1.Name = "btn38_1";
            this.btn38_1.Size = new System.Drawing.Size(131, 46);
            this.btn38_1.TabIndex = 3;
            this.btn38_1.Text = "C# のインターフェース(38-1)";
            this.btn38_1.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.btn37_2);
            this.groupBox10.Controls.Add(this.btn37_1);
            this.groupBox10.Location = new System.Drawing.Point(14, 317);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox10.Size = new System.Drawing.Size(508, 77);
            this.groupBox10.TabIndex = 11;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "ジェネリクスの共変性・反変性";
            // 
            // btn37_2
            // 
            this.btn37_2.Location = new System.Drawing.Point(188, 18);
            this.btn37_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn37_2.Name = "btn37_2";
            this.btn37_2.Size = new System.Drawing.Size(131, 46);
            this.btn37_2.TabIndex = 4;
            this.btn37_2.Text = "in/out 修飾子(37-2)";
            this.btn37_2.UseVisualStyleBackColor = true;
            // 
            // btn37_1
            // 
            this.btn37_1.Location = new System.Drawing.Point(11, 18);
            this.btn37_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn37_1.Name = "btn37_1";
            this.btn37_1.Size = new System.Drawing.Size(131, 46);
            this.btn37_1.TabIndex = 3;
            this.btn37_1.Text = "ジェネリックの共変性・反変性(37-1)";
            this.btn37_1.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.btn36_7);
            this.groupBox9.Controls.Add(this.btn36_6);
            this.groupBox9.Controls.Add(this.btn36_5);
            this.groupBox9.Controls.Add(this.btn36_4);
            this.groupBox9.Controls.Add(this.btn36_3);
            this.groupBox9.Controls.Add(this.btn36_2);
            this.groupBox9.Controls.Add(this.btn36_1);
            this.groupBox9.Location = new System.Drawing.Point(14, 108);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox9.Size = new System.Drawing.Size(508, 190);
            this.groupBox9.TabIndex = 11;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "ジェネリック";
            // 
            // btn36_7
            // 
            this.btn36_7.Location = new System.Drawing.Point(11, 132);
            this.btn36_7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn36_7.Name = "btn36_7";
            this.btn36_7.Size = new System.Drawing.Size(131, 46);
            this.btn36_7.TabIndex = 9;
            this.btn36_7.Text = "既定値(36-7)";
            this.btn36_7.UseVisualStyleBackColor = true;
            // 
            // btn36_6
            // 
            this.btn36_6.Location = new System.Drawing.Point(333, 75);
            this.btn36_6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn36_6.Name = "btn36_6";
            this.btn36_6.Size = new System.Drawing.Size(169, 46);
            this.btn36_6.TabIndex = 4;
            this.btn36_6.Text = "複雑な型引数の使い方(36-6)";
            this.btn36_6.UseVisualStyleBackColor = true;
            // 
            // btn36_5
            // 
            this.btn36_5.Location = new System.Drawing.Point(188, 75);
            this.btn36_5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn36_5.Name = "btn36_5";
            this.btn36_5.Size = new System.Drawing.Size(131, 46);
            this.btn36_5.TabIndex = 5;
            this.btn36_5.Text = "インスタンス化(36-5)";
            this.btn36_5.UseVisualStyleBackColor = true;
            // 
            // btn36_4
            // 
            this.btn36_4.Location = new System.Drawing.Point(11, 75);
            this.btn36_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn36_4.Name = "btn36_4";
            this.btn36_4.Size = new System.Drawing.Size(131, 46);
            this.btn36_4.TabIndex = 6;
            this.btn36_4.Text = "制約条件(36-4)";
            this.btn36_4.UseVisualStyleBackColor = true;
            // 
            // btn36_3
            // 
            this.btn36_3.Location = new System.Drawing.Point(356, 18);
            this.btn36_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn36_3.Name = "btn36_3";
            this.btn36_3.Size = new System.Drawing.Size(131, 46);
            this.btn36_3.TabIndex = 3;
            this.btn36_3.Text = "C# のジェネリック(36-3)";
            this.btn36_3.UseVisualStyleBackColor = true;
            // 
            // btn36_2
            // 
            this.btn36_2.Location = new System.Drawing.Point(188, 18);
            this.btn36_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn36_2.Name = "btn36_2";
            this.btn36_2.Size = new System.Drawing.Size(131, 46);
            this.btn36_2.TabIndex = 3;
            this.btn36_2.Text = "ジェネリッククラス(36-2)";
            this.btn36_2.UseVisualStyleBackColor = true;
            // 
            // btn36_1
            // 
            this.btn36_1.Location = new System.Drawing.Point(11, 18);
            this.btn36_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn36_1.Name = "btn36_1";
            this.btn36_1.Size = new System.Drawing.Size(131, 46);
            this.btn36_1.TabIndex = 3;
            this.btn36_1.Text = "ジェネリックメソッド(36-1)";
            this.btn36_1.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.btn35_1);
            this.groupBox8.Location = new System.Drawing.Point(14, 14);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox8.Size = new System.Drawing.Size(508, 77);
            this.groupBox8.TabIndex = 10;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "抽象メソッド・抽象クラス";
            // 
            // btn35_1
            // 
            this.btn35_1.Location = new System.Drawing.Point(11, 18);
            this.btn35_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn35_1.Name = "btn35_1";
            this.btn35_1.Size = new System.Drawing.Size(131, 46);
            this.btn35_1.TabIndex = 3;
            this.btn35_1.Text = "抽象化(35-1)";
            this.btn35_1.UseVisualStyleBackColor = true;
            // 
            // FrmObject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 671);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmObject";
            this.Text = "FrmStructure";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.btn38_7.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn28_1;
        private System.Windows.Forms.Button btn28_2;
        private System.Windows.Forms.Button btn28_3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage btn38_7;
        private System.Windows.Forms.Button btn28_6;
        private System.Windows.Forms.Button btn28_5;
        private System.Windows.Forms.Button btn28_4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn29_7;
        private System.Windows.Forms.Button btn29_6;
        private System.Windows.Forms.Button btn29_5;
        private System.Windows.Forms.Button btn29_4;
        private System.Windows.Forms.Button btn29_3;
        private System.Windows.Forms.Button btn29_2;
        private System.Windows.Forms.Button btn29_1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn31_4;
        private System.Windows.Forms.Button btn31_3;
        private System.Windows.Forms.Button btn31_2;
        private System.Windows.Forms.Button btn31_1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btn30_2;
        private System.Windows.Forms.Button btn30_1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btn32_3;
        private System.Windows.Forms.Button btn32_2;
        private System.Windows.Forms.Button btn32_1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btn33_2;
        private System.Windows.Forms.Button btn33_1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btn34_2;
        private System.Windows.Forms.Button btn34_1;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btn35_1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Button btn36_7;
        private System.Windows.Forms.Button btn36_6;
        private System.Windows.Forms.Button btn36_5;
        private System.Windows.Forms.Button btn36_4;
        private System.Windows.Forms.Button btn36_3;
        private System.Windows.Forms.Button btn36_2;
        private System.Windows.Forms.Button btn36_1;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button btn37_2;
        private System.Windows.Forms.Button btn37_1;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button btn38_4;
        private System.Windows.Forms.Button btn38_5;
        private System.Windows.Forms.Button btn38_3;
        private System.Windows.Forms.Button btn38_2;
        private System.Windows.Forms.Button btn38_1;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button btn39_7;
        private System.Windows.Forms.Button btn39_6;
        private System.Windows.Forms.Button btn39_5;
        private System.Windows.Forms.Button btn39_4;
        private System.Windows.Forms.Button btn39_3;
        private System.Windows.Forms.Button btn39_2;
        private System.Windows.Forms.Button btn39_1;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Button btn40_2;
        private System.Windows.Forms.Button btn40_1;
        private System.Windows.Forms.Button btn31_5;
    }
}