﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices; 
using System.IO;
using curriculum.FormMessage.Frm_P層_.表示用Form;
using curriculum1.Object;

namespace curriculum1
{
    /// ********************************************************************************
    /// <summary>
    /// 構造化フォーム
    /// </summary>
    /// <remarks>none</remarks>
    /// 
    /// <history>
    /// --------------------------------------------------------------------------------
    /// No     Date         User         Remorks
    /// --------------------------------------------------------------------------------
    /// 0001   2015.12.05   宮崎祥彰   *new*
    /// 
    /// </history>
    /// ********************************************************************************
    public partial class FrmObject : Base_Form
    {


        #region ++++++++++ member variable ++++++++++
        public enum TopicNo
        {
            Topic28_3 = FrmStructure.TopicNo.Topic17_7 + 1,
            Topic28_4
            
        }
        #endregion

        #region++++++++++ constructor/destructor ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰     *new*
        /// 
        /// </history>
        /// ********************************************************************************
        public FrmObject()
        {
            // -------------------------------------------------------
            // 初期化処理
            InitializeComponent();
            this.EntryEvents();
        }

        #endregion

        #region ++++++++++ event ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// イベント Form　Load時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void FormMain_Load(object sender, EventArgs e) {

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = !this.MaximizeBox;
            
        }

        /// ********************************************************************************
        /// <summary>
        /// イベントハンドラ登録
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void EntryEvents(){

            this.Load += new EventHandler(this.FormMain_Load);
            this.btn28_1.Click += new EventHandler(this.btn28_1_Click);
            this.btn28_2.Click += new EventHandler(this.btn28_2_Click);
            this.btn28_3.Click += new EventHandler(this.btn28_3_Click);
            this.btn28_4.Click += new EventHandler(this.btn28_4_Click);
            this.btn28_5.Click += new EventHandler(this.btn28_5_Click);
            this.btn28_6.Click += new EventHandler(this.btn28_6_Click);
            this.btn29_1.Click += new EventHandler(this.btn29_1_Click);
            this.btn29_2.Click += new EventHandler(this.btn29_2_Click);
            this.btn29_3.Click += new EventHandler(this.btn29_3_Click);
            this.btn29_4.Click += new EventHandler(this.btn29_4_Click);
            this.btn29_5.Click += new EventHandler(this.btn29_5_Click);
            this.btn29_6.Click += new EventHandler(this.btn29_6_Click);
            this.btn29_7.Click += new EventHandler(this.btn29_7_Click);
            this.btn30_1.Click += new EventHandler(this.btn30_1_Click);
            this.btn30_2.Click += new EventHandler(this.btn30_2_Click);
            this.btn31_1.Click += new EventHandler(this.btn31_1_Click);
            this.btn31_2.Click += new EventHandler(this.btn31_2_Click);
            this.btn31_3.Click += new EventHandler(this.btn31_3_Click);
            this.btn31_4.Click += new EventHandler(this.btn31_4_Click);
            this.btn31_5.Click += new EventHandler(this.btn31_5_Click);
            this.btn32_1.Click += new EventHandler(this.btn32_1_Click);
            this.btn32_2.Click += new EventHandler(this.btn32_2_Click);
            this.btn32_3.Click += new EventHandler(this.btn32_3_Click);
            this.btn33_1.Click += new EventHandler(this.btn33_1_Click);
            this.btn33_2.Click += new EventHandler(this.btn33_2_Click);
            this.btn34_1.Click += new EventHandler(this.btn34_1_Click);
            this.btn34_2.Click += new EventHandler(this.btn34_2_Click);
            this.btn35_1.Click += new EventHandler(this.btn35_1_Click);
            this.btn36_1.Click += new EventHandler(this.btn36_1_Click);
            this.btn36_2.Click += new EventHandler(this.btn36_2_Click);
            this.btn36_3.Click += new EventHandler(this.btn36_3_Click);
            this.btn36_4.Click += new EventHandler(this.btn36_4_Click);
            this.btn36_5.Click += new EventHandler(this.btn36_5_Click);
            this.btn36_6.Click += new EventHandler(this.btn36_6_Click);
            this.btn36_7.Click += new EventHandler(this.btn36_7_Click);
            this.btn37_1.Click += new EventHandler(this.btn37_1_Click);
            this.btn37_2.Click += new EventHandler(this.btn37_2_Click);
            this.btn38_1.Click += new EventHandler(this.btn38_1_Click);
            this.btn38_2.Click += new EventHandler(this.btn38_2_Click);
            this.btn38_3.Click += new EventHandler(this.btn38_3_Click);
            this.btn38_4.Click += new EventHandler(this.btn38_4_Click);
            this.btn38_5.Click += new EventHandler(this.btn38_5_Click);
            this.btn39_1.Click += new EventHandler(this.btn39_1_Click);
            this.btn39_2.Click += new EventHandler(this.btn39_2_Click);
            this.btn39_3.Click += new EventHandler(this.btn39_3_Click);
            this.btn39_4.Click += new EventHandler(this.btn39_4_Click);
            this.btn39_5.Click += new EventHandler(this.btn39_5_Click);
            this.btn39_6.Click += new EventHandler(this.btn39_6_Click);
            this.btn39_7.Click += new EventHandler(this.btn39_7_Click);
            this.btn40_1.Click += new EventHandler(this.btn40_1_Click);
            this.btn40_2.Click += new EventHandler(this.btn40_2_Click);

        }

        #region++++++++++ クラス(28-1～28_7) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// クラスとインスタンス
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn28_1_Click(object sender, EventArgs e)
        {
            C28_1 c28　= new C28_1();
            MessageBox.Show(c28.a);
            
        }
        private class C28_1
        {
            //メンバー変数の定義
            public string a = "28_1のstring a をインスタンス";

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// クラス定義
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn28_2_Click(object sender, EventArgs e)
        {
            C28_2 c28_2 = new C28_2();
            c28_2.Set(1,2);
            MessageBox.Show(String.Format("C28のxの値は{0}、yの値は{1}",c28_2.GetX(),c28_2.GetY()));

        }
        private class C28_2
        {
            // メンバー変数の定義 ここから↓
            private int x;
            private int y;
            // メンバー変数の定義 ここまで↑

            // メソッドの定義 ここから↓
            public int GetX()
            {
                return x;
            }

            public int GetY()
            {
                return y;
            }

            public void Set(int a, int b)
            {
                x = a;
                y = b;
            }
            // メソッドの定義 ここまで↑
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// クラスの利用
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn28_3_Click(object sender, EventArgs e)
        {
            Complex z;         // インスタンスを格納するための変数を定義
            z = new Complex(); // new を使ってインスタンスを生成

            z.re = 3;             // 実部の値を変更
            z.im = 4;             // 虚部の値を変更
            double abs = z.Abs(); // z の絶対値を取得

            MessageBox.Show(String.Format("abs = {0}\n", abs));
        }

        class Complex
        {
            public double re; // 実部を記憶しておく(外部からの読み出し・書き換えも可能)
            public double im; // 虚部を記憶しておく(外部からの読み出し・書き換えも可能)

            // 絶対値を取り出す
            public double Abs()
            {
                return Math.Sqrt(re * re + im * im);// Math.Sqrt は平方根を求める関数
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// クラスの分割定義
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn28_4_Click(object sender, EventArgs e)
        {
            C28_4 c28_4 = new C28_4();
            MessageBox.Show(Convert.ToString(c28_4.Abs()));
        }
        partial class C28_4
        {
            public double re;
            public double im;
        }
        partial class C28_4
        {
            public double Abs()
            {
                re = 0.1;
                im = 0.2;
                return Math.Sqrt(re * re + im * im);
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// メソッドの実装の分離
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn28_5_Click(object sender, EventArgs e)
        {
            C28_5 c28_5 = new C28_5();   
            c28_5.start();
        }
        partial class C28_5
        {
            public void start()
            {
                OnBeginProgram();
                MessageBox.Show("program body");
                OnEndProgram();
            }

            static partial void OnBeginProgram();
            static partial void OnEndProgram();
        }

        partial class C28_5
        {
            static partial void OnBeginProgram()
            {
                MessageBox.Show("check pre-condition");
            }

            static partial void OnEndProgram()
            {
                MessageBox.Show("check post-condition");
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 匿名型
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn28_6_Click(object sender, EventArgs e)
        {
            var rectangle = new { Width = 2, Height = 3 };

            MessageBox.Show(String.Format("幅 :{0} 高さ:{1} 面積:{2}",
            rectangle.Width,
            rectangle.Height,
            rectangle.Width * rectangle.Height));
        }
        
        #endregion

        #region++++++++++ コンストラクタとデストラクター(29-1～29_5) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// コンストラクター
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn29_1_Click(object sender, EventArgs e)
        {
            Console.Write("btn29_1_Clickイベント の先頭\n");

            C29_1 t = new C29_1(); // ここで Test のコンストラクターが呼ばれる

            Console.Write("btn29_1_Clickイベント の末尾\n");
        }
        class C29_1
        {
            public C29_1()
            {
                MessageBox.Show("C29_1 クラスのコンストラクターが呼ばれました\n");
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 変数初期化子
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn29_2_Click(object sender, EventArgs e)
        {
            C29_2 person = new C29_2();
            MessageBox.Show(string.Format("name:{0} age:{1}",person.name,person.age));
        }
        class C29_2
        {
            public string name = "テスト";
            public int age = 10;
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// コンストラクター初期化子
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn29_3_Click(object sender, EventArgs e)
        {
            new C29_3();
        }
        class C29_3Member
        {
            public C29_3Member(int n)
            {
                MessageBox.Show(string.Format("変数初期化子 {0}\n", n));
            }
        }
        class C29_3
        {
            C29_3Member x = new C29_3Member(1);
            C29_3Member y = new C29_3Member(2);

            public C29_3()
                : this(0)
            {
                MessageBox.Show("引数なしのコンストラクター\n");
            }

            public C29_3(int x)
            {
                MessageBox.Show(string.Format("引数 x = {0} 付きのコンストラクター\n", x));
            }
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// オブジェクト初期化子
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn29_4_Click(object sender, EventArgs e)
        {
            Point p = new Point { X = 0, Y = 1 };
            MessageBox.Show(string.Format("PointX:{0} PointY:{1}",p.X, p.Y));
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// デストラクター
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn29_5_Click(object sender, EventArgs e)
        {
            Console.Write("1\n");
            C29_5 c = new C29_5();
            c = null;
            Console.Write("2\n");
           
        }

        class C29_5
        {
            public C29_5()
            {
                MessageBox.Show("C29_5のクラスのコンストラクターが呼ばれました。");
            }
            ~C29_5()
            {
                MessageBox.Show("C29_5のクラスのデストラクタ―が呼ばれました。");
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 注意: デストラクターの呼び出しタイミング
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn29_6_Click(object sender, EventArgs e)
        {
            MessageBox.Show("1\n");
            C29_5 t = new C29_5(); // ここで Test のコンストラクターが呼ばれる
            MessageBox.Show("2\n");
            t = null;            // ↑で作成したインスタンスはもう利用されなくなる
            // でも、デストラクターはまだ呼ばれない
            MessageBox.Show("3\n");
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 注意: Finalize
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn29_7_Click(object sender, EventArgs e)
        {
            using (FileStream reader = new FileStream("test.txt", FileMode.Open))
            {
                // 先頭のNバイトを読み出して画面に表示

                const int N = 10;
                byte[] buf = new byte[N];
                reader.Read(buf, 0, N);
                for (int i = 0; i < N; ++i)
                {
                    MessageBox.Show(string.Format("{0,4}", (int)buf[i]));
                }
            }
        }

        #endregion

        #region++++++++++ 実装の隠蔽(30-1～31_2) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// アクセスシビティ
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn30_1_Click(object sender, EventArgs e)
        {
            A a = new A();
            // クラス A の外部
            a.pub = 1; // OK
            //a.pro = 2; // エラー
            //a.pri = 3; // エラー
            MessageBox.Show("アクセス権限のない場所からクラスのメンバーにアクセスしようとする為\nコンパイルエラー");

        }

        class A
        {
            public int pub; // どこからでもアクセス可能
            protected int pro; // クラス内部と派生クラス内部からアクセス可能
            private int pri; // クラス内部からのみアクセス可能

            public void function1()
            {
                // クラス内部
                pub = 1; // OK
                pro = 2; // OK
                pri = 3; // OK
            }
        }

        class B : A
        {
            public void function2()
            {
                // 派生クラス内部
                pub = 1; // OK
                pro = 2; // OK
                //pri = 3; // エラー
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 実装の隠蔽
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn30_2_Click(object sender, EventArgs e)
        {
            Complex30 c = new Complex30();
            c.Re(4); // メソッドを通してオブジェクトの状態を変更
            c.Im(3);
            MessageBox.Show(String.Format("c = {0}", c.Abs()));

        }

        // クラス定義
        class Complex30
        {
            // 実装は外部から隠蔽(privateにしておく)
            private double re; // 実部を記憶しておく
            private double im; // 虚部を記憶しておく

            public double Re() { return this.re; }    // 実部を取り出す
            public void Re(double x) { this.re = x; } // 実部を書き換え

            public double Im() { return this.im; }    // 虚部を取り出す
            public void Im(double y) { this.im = y; } // 虚部を書き換え

            public double Abs() { return Math.Sqrt(re * re + im * im); }  // 絶対値を取り出す
        }


        #endregion

        #region++++++++++ プロパティ(31-1～31_5) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// プロパティとは
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn31_1_Click(object sender, EventArgs e)
        {
            Complex_31_1 c = new Complex_31_1();
            c.Re = 4; // クラス利用側は一切変更せず
            c.Im = 3;
            MessageBox.Show(string.Format("|{0} + {1}i| = {2}", c.Re, c.Im, c.Abs));
        }
        // 「実装の隠蔽」で作った複素数クラス
        class Complex_31_1
        {
            // 実装は外部から隠蔽(privateにしておく)
            private double abs; // 絶対値を記憶しておく
            private double arg; // 偏角を記憶しておく

            // 実部の取得・変更用のプロパティ
            public double Re
            {
                set
                {
                    double im = this.abs * Math.Sin(this.arg);
                    this.abs = Math.Sqrt(value * value + im * im);
                    this.arg = Math.Atan2(im, value);
                }
                get
                {
                    return this.abs * Math.Cos(this.arg);
                }
            }

            // 実部の取得・変更用のプロパティ
            public double Im
            {
                set
                {
                    double re = this.abs * Math.Cos(this.arg);
                    this.abs = Math.Sqrt(value * value + re * re);
                    this.arg = Math.Atan2(value, re);
                }
                get
                {
                    return this.abs * Math.Sin(this.arg);
                }
            }

            // 絶対値の取得用のプロパティ
            public double Abs
            {
                get { return this.abs; }
            }
        }
    
        ///// ********************************************************************************
        ///// <summary>
        ///// set/get アクセスレベル
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn31_2_Click(object sender, EventArgs e)
        {
            A_31_2 a = new A_31_2();
            MessageBox.Show(string.Format("A_31_2のNの値は{0}", a.N));
            a.setn(2);
            MessageBox.Show(string.Format("A_31_2のNの値は{0}", a.N));
        }
        class A_31_2
        {
            private int n;

            public int N
            {
                get { return this.n; }
                protected set { this.n = value; }
            }

            public void setn(int i)
            {
                n = i;
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 自動プロパティ
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn31_3_Click(object sender, EventArgs e)
        {
            Complex_31_3 c = new Complex_31_3();
            c.Re = 2.0;
            c.Im = 3.0;
            MessageBox.Show(string.Format("Abs : {0}", c.Abs));
        }

        // 「実装の隠蔽」で作った複素数クラス
        class Complex_31_3
        {
            public double Re { get; set; }
            public double Im { get; set; }

            public double Abs
            {
                get { return Math.Sqrt(Re * Re + Im * Im); }
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// get-only プロパティ
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn31_4_Click(object sender, EventArgs e)
        {
            MessageBox.Show("C#のバージョンが6.0の場合のみ実装可能\n本環境はVisualStudio2012の為実装不可");
            //C#のバージョンが6.0の場合のみ実装可能
            //本環境はVisualStudio2012の為実装不可である為、コメントアウトします。
            //Complex_31_4 c = new Complex_31_4(2.0, 1.0);
        }
        // 「実装の隠蔽」で作った複素数クラス
        //public class Complex_31_4
        //{
        //    public double Re { get; }
        //    public double Im { get; }

        //    public Complex_31_4(double re, double im)
        //    {
        //        // コンストラクター内でだけ代入可能。
        //        Re = re;
        //        Im = im;
        //    }

        //}

        /// ********************************************************************************
        /// <summary>
        /// expression-bodied なプロパティ
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2016.1.03  宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn31_5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("C#のバージョンが6.0の場合のみ実装可能\n本環境はVisualStudio2012の為実装不可");
            //C#のバージョンが6.0の場合のみ実装可能
            //本環境はVisualStudio2012の為実装不可
        }

        #endregion

        #region++++++++++ 静的メンバー(32-1～32_5) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 静的コンストラクター
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn32_1_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                Product p = new Product();

                MessageBox.Show(String.Format("ID:{0}", p.ID));
            }
        }
        // 1台ごとに固有のIDが振られるような何らかの製品。
        class Product
        {
            static int id_generator;
            int id;

            static Product()
            {
                // 最初に1度だけ呼ばれ、id_generator を 0 に初期化。
                id_generator = 0;
            }

            public Product()
            {
                // 新しい製品が製造されるたびに新しい id を振る。
                id = id_generator;
                id_generator++;
            }

            /// <summary>
            /// その製品のIDを取得する。
            /// </summary>
            public int ID
            {
                get { return id; }
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 静的クラス
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn32_2_Click(object sender, EventArgs e)
        {
            
            MessageBox.Show(Convert.ToString(Math32_2.Sin(0.1)));
            
        }
        
        ///// ********************************************************************************
        ///// <summary>
        ///// 拡張メソッド
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn32_3_Click(object sender, EventArgs e)
        {
            //今まで
            //int x = int.Parse("1"); // "1" よりも Parse が前
            int x = "1".Parse();
            MessageBox.Show(Convert.ToString(x));
        }
        
        ///// ********************************************************************************
        ///// <summary>
        ///// using staticと列挙型
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn32_4_Click(object sender, EventArgs e)
        {
            //C#のバージョンが6.0の場合のみ実装可能
            //本環境はVisualStudio2012の為実装不可である為、コメントアウトします。

            //using System;
            //using static System.Math;

            //class Program
            //{
            //    static void Main()
            //    {
            //        var pi = 2 * Asin(1);
            //        MessageBox.Show(PI == pi);
            //    }
            //}
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// using staticと拡張メソッド
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn32_5_Click(object sender, EventArgs e)
        {
            //C#のバージョンが6.0の場合のみ実装可能
            //本環境はVisualStudio2012の為実装不可である為、コメントアウトします。
            //class UsingStaticSample
            //{
            //    public void X()
            //    {
            //        // 普通の静的メソッド
            //        // Enumerable.Range が呼ばれる
            //        var input = Range(0, 10);

            //        // 拡張メソッド
            //        // Enumerable.Select が呼ばれる
            //        var output1 = input.Select(x => x * x);

            //        // 拡張メソッドを普通の静的メソッドとして呼ぼうとすると
            //        // コンパイル エラー
            //        var output2 = Select(input, x => x * x);
            //    }
            //}
        }

        #endregion

        #region++++++++++ 演算子のオーバーロード(33-1～33_2) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 演算子のオーバーロードの方法
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn33_1_Click(object sender, EventArgs e)
        {
            Complex33_1 cComp33_1 = new Complex33_1(0,0);
            // x = 5 + 1i
            var x = cComp33_1.Re = 5;  // x.re = 5
            var y = cComp33_1.Im = 1;  // x.im = 1

            MessageBox.Show(String.Format("{0},{1}", Convert.ToString(x), Convert.ToString(y)));
        }

        class Complex33_1
        {
            // 実装は外部から隠蔽(privateにしておく)
            private double re; // 実部を記憶しておく
            private double im; // 虚部を記憶しておく

            // 実部の取得・変更用のプロパティ
            public double Re
            {
                set { this.re = value; }
                get { return this.re; }
            }
            /* ↑のコードは意味的には以下のコードと同じ。
            public void SetRe(double value){this.re = value;}
            public double GetRe(){return this.re;}
            メソッドと同じ感覚で使える。
            */

            // 実部の取得・変更用のプロパティ
            public double Im
            {
                set { this.im = value; }
                get { return this.im; }
            }

            // 絶対値の取得用のプロパティ
            public double Abs
            {
                // 読み取り専用プロパティ。
                // setブロックを書かない。
                get { return Math.Sqrt(re * re + im * im); }
            }

            //コンストラクタ
            public Complex33_1 (double x , double y){
            }

            public static Complex33_1 operator +(Complex33_1 z, Complex33_1 w)
            {
                return new Complex33_1(z.Re + w.Re, z.Im + w.Im);
            }

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// オーバーロード可能な演算子
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn33_2_Click(object sender, EventArgs e)
        {
            Bool b = new Bool(0);

            if (b) // 条件式として利用できる
                MessageBox.Show("b == true");
            else
                MessageBox.Show("b == false");
        }
        class Bool
        {
            int i;
            public Bool(int i) { this.i = i; }
            public static bool operator true(Bool b) { return b.i != 0; }
            public static bool operator false(Bool b) { return b.i == 0; }
        }

        #endregion

        #region++++++++++ インデクサー(34-1～34_2) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// インデクサーの定義
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn34_1_Click(object sender, EventArgs e)
        {
            BoundArray a = new BoundArray(1, 9);

            for (int i = 1; i <= 9; ++i)
                a[i] = i;

            for (int i = 1; i <= 9; ++i)
                MessageBox.Show(String.Format("a[{0}] = {1}", i, a[i]));

        }

        class BoundArray
        {
            int[] array;
            int lower;   // 配列添字の下限

            public BoundArray(int lower, int upper)
            {
                this.lower = lower;
                array = new int[upper - lower + 1];
            }

            public int this[int i]
            {
                set { this.array[i - lower] = value; }
                get { return this.array[i - lower]; }
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// set/get で異なるアクセスレベルを設定
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn34_2_Click(object sender, EventArgs e)
        {
            C34_2 c34_2 = new C34_2();
            c34_2.x = new int[2]{1,2};
            MessageBox.Show(String.Format("{0},{1}",Convert.ToString(c34_2.x[0]),Convert.ToString(c34_2.x[1])));
            
        }

        class C34_2 
        {
            public int[] x;
            public int this[int i]
            {
                get { return x[i]; }
                private set { x[i] = value; }
            }    
        }

        #endregion

        #region++++++++++ 抽象メソッド・抽象クラス(35-1) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 抽象化
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn35_1_Click(object sender, EventArgs e)
        {
                Introduce(new Truepenny("Ky Kiske", 24)); //正直者のカイさん24歳。
                Introduce(new Liar("Axl Low", 24)); //嘘つきのアクセルさん24歳。
                Introduce(new Equivocator("Sol Badguy", 24)); //いい加減なソルさん24歳。
                Introduce(new Seventeenist("Ino", 24)); // 時空を超えるイノさん24歳。
        }

        abstract class Person
        {
            protected string name;
            protected int age;

            public Person(string name, int age)
            {
                this.name = name;
                this.age = age;
            }

            public string Name { get { return this.name; } }
            public abstract int Age { get; } // 抽象メソッドには定義は要らない
        }

        /// <summary>
        /// 正直者。
        /// 年齢を偽らない。
        /// </summary>
        class Truepenny : Person
        {
            public Truepenny(string name, int age) : base(name, age) { }

            public override int Age
            {
                get
                {
                    // 実年齢をそのまま返す。
                    return this.age;
                }
            }
        }

        /// <summary>
        /// 嘘つき。
        /// 鯖を読む(しかも、歳取るにつれ大幅に)。
        /// </summary>
        class Liar : Person
        {
            public Liar(string name, int age) : base(name, age) { }

            public override int Age
            {
                get
                {
                    // 年齢を偽る。
                    if (this.age < 20) return this.age;
                    if (this.age < 25) return this.age - 1;
                    if (this.age < 30) return this.age - 2;
                    if (this.age < 35) return this.age - 3;
                    if (this.age < 40) return this.age - 4;
                    return this.age - 5;
                }
            }
        }

        /// <summary>
        /// いいかげん。
        /// 大体の歳しか答えない。
        /// </summary>
        class Equivocator : Person
        {
            public Equivocator(string name, int age) : base(name, age) { }

            public override int Age
            {
                get
                {
                    // 年齢を四捨五入した値を返す。
                    return ((this.age + 5) / 10) * 10;
                }
            }
        }

        /// <summary>
        /// いくつになったって気持ちは17歳。
        /// </summary>
        class Seventeenist : Person
        {
            public Seventeenist(string name, int age) : base(name, age) { }

            public override int Age
            {
                get
                {
                    // 「おいおい」って突っ込み入れてあげてね。
                    return 17;
                }
            }
        }

        /// <summary>
        /// p さんの自己紹介をする。
        /// </summary>
        static void Introduce(Person p)
        {
            MessageBox.Show(String.Format("My name is {0}", p.Name));
            MessageBox.Show(String.Format("I'm {0} years old", Convert.ToString(p.Age)));
        }

        #endregion

        #region++++++++++ ジェネリック(36-1) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// ジェネリックメソッド
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn36_1_Click(object sender, EventArgs e)
        {
            int n1 = Max36_1<int>(5, 10);   // int 版の Max を明示的に呼び出し
            MessageBox.Show(Convert.ToString(n1));
        }
        public static Type Max36_1<Type>(Type a, Type b)
         where Type : IComparable
        {
            return a.CompareTo(b) > 0 ? a : b;
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// ジェネリッククラス
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn36_2_Click(object sender, EventArgs e)
        {
            const int SIZE = 5;
            Stack<int> si = new Stack<int>(SIZE);    // int型を格納できるスタックになる
            Stack<double> sd = new Stack<double>(SIZE); // double型を格納できるスタックになる

            for (int i = 1; i <= SIZE; ++i)
            {
                si.Push(i);
                sd.Push(1.0 / i);
            }

            while (si.Size != 0)
            {
                MessageBox.Show(String.Format("1/{0} = {1}", si.Pop(), sd.Pop()));
            }
        }

        class Stack<Type>
        {
            Type[] buf;
            int top;
            public Stack(int max) { this.buf = new Type[max]; this.top = 0; }
            public void Push(Type val) { this.buf[this.top++] = val; }
            public Type Pop() { return this.buf[--this.top]; }
            public int Size { get { return this.top; } }
            public int MaxSize { get { return this.buf.Length; } }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// C# のジェネリック
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn36_3_Click(object sender, EventArgs e)
        {
            //定義方法を記載
            MessageBox.Show("class クラス名<型引数>\nwhere 型引数中の型が満たすべき条件\n{\n  クラス定義\n}\nアクセスレベル 戻り値の型 メソッド名<型引数>(引数リスト)\n  where 型引数中の型が満たすべき条件\n{\n  メソッド定義\n} ");
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 制約条件
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn36_4_Click(object sender, EventArgs e)
        {
            int n1 = Max36_4<int>(5, 10);   // int 版の Max を明示的に呼び出し
            MessageBox.Show(Convert.ToString(n1));
        }

        // 一番目の引数だけを帰す単純なメソッド。
        static Type First<Type>(Type a, Type b)
        {
            // 特にメソッド呼び出し等はないのでこれは OK。
            return a;
        }

        
        static Type Max36_4<Type>(Type a, Type b) where Type : IComparable
        {
            // ↑この制約条件のお陰で、
            // ↓Type 型 は CompareTo を持っているというのが分かる。
            return a.CompareTo(b) > 0 ? a : b;
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// インスタンス化
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn36_5_Click(object sender, EventArgs e)
        {
            const int SIZE = 5;
            Stack<int> si = new Stack<int>(SIZE);    // Stack を int でインスタンス化
            Stack<double> sd = new Stack<double>(SIZE); // Stack を double でインスタンス化

            int n = Max36_4(5, 10);        // Max を int でインスタンス化
            double x = Max36_4(5.0, 10.0);    // Max を double でインスタンス化
            string s = Max36_4("abc", "cat"); // Max を string でインスタンス化

            MessageBox.Show(String.Format("{0},{1},{2}",n,x,s));

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 複雑な型引数の使い方
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn36_6_Click(object sender, EventArgs e)
        {
            int[] i = new int[]{1, 2, 3, 4, 5};
            Show(i);
        }

        // リスト中の要素を Console.Write で画面に出力。
        static void Show<Type>(System.Collections.Generic.IList<Type> list)
        {
        foreach(Type x in list)
            MessageBox.Show(Convert.ToString(x));
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 既定値
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn36_7_Click(object sender, EventArgs e)
        {
            int[] i = new int[5];
            string[] s = new string[5];

            FillWithDefault(i);
            FillWithDefault(s);
            MessageBox.Show(String.Format("NullであればOK\n{0},{1}",i,s));
        }

        // 配列を 0 または null で満たします。
        static void FillWithDefault<Type>(Type[] array)
        {
            for (int i = 0; i < array.Length; ++i)
                array[i] = default(Type);
        }
        #endregion

        #region++++++++++ ジェネリックの共変性・反変性(37-1) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// ジェネリックの共変性・反変性
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn37_1_Click(object sender, EventArgs e)
        {
            
            MessageBox.Show("共変性\n            IEnumerable<string> strings = new[] { aa, bb, cc };\n            IEnumerable<object> objs = strings;\n            反変性\n            Action<object> objAction = x => { MessageBox.Show((string)x); };\n            Action<string> strAction = objAction;");            
        }
        
        ///// ********************************************************************************
        ///// <summary>
        ///// in/out 修飾子
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn37_2_Click(object sender, EventArgs e)
        {   
            Func<object, object, string> f1 = (x, y) => string.Format("({0}, {1})", x, y);
            Func<string, string, object> f2 = f1;
            MessageBox.Show(f1("a","b"));
        }
        public delegate TResult Func<in T1, in T2, out TResult>(T1 arg1, T2 arg2);

        #endregion

        #region++++++++++ インターフェース(38-1～38-5) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// C# のインターフェース
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn38_1_Click(object sender, EventArgs e)
        {
            C38_1_1 c38_1_1 = new C38_1_1();
            C38_1_2 c38_1_2 = new C38_1_2();
            c38_1_1.task();
            c38_1_2.task();
            
        }

        //--------38_1------------------------
        interface I38_1
        {
            void task();
        }

        //38_1_1のクラス
        class C38_1_1 : I38_1
        {
            public void task()
            {
                MessageBox.Show("C38_1_1のtask");
            }
        }
        //38_1_2のクラス
        class C38_1_2 : I38_1
        {
            public void task()
            {
                MessageBox.Show("C38_1_2のtask");
            }
        }
        
        ///// ********************************************************************************
        ///// <summary>
        ///// IComparable
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn38_2_Click(object sender, EventArgs e)
        {
            //ArrayListの作成
            System.Collections.ArrayList al = new System.Collections.ArrayList();
            al.Add(new C38_2("キャンディ", 50));
            al.Add(new C38_2("キャラメル", 100));
            al.Add(new C38_2("チョコレート", 10));

            //はじめの状態を表示
            MessageBox.Show("並び替えなし");
            foreach (C38_2 c in al)
            {
                MessageBox.Show(String.Format("Name: {0} / Price: {1}", c.Name, c.Price));
            }

            //並び替える
            al.Sort();
            //結果を表示
            MessageBox.Show("並び替え後");
            foreach (C38_2 c in al)
            {
                MessageBox.Show(String.Format("Name: {0} / Price: {1}", c.Name, c.Price));
            }

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// コレクション
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn38_3_Click(object sender, EventArgs e)
        {
            MyCollection c38_3 = new MyCollection();
            foreach (int i in c38_3)
            {
                MessageBox.Show(Convert.ToString(i));
            }
        }

        //IEnumerable
        //foreachはIEnumerableインターフェースを実装しているクラスしか処理することができません。
        public class MyCollection:IEnumerable
        {
            int[] hoge;
            public MyCollection()
            {
                hoge=new int[]{0,1,2,3,4,5,6,7,8,9,10};
            }

            public IEnumerator GetEnumerator()
            {
                //偶数のみ取得
                for (int i = 0; i < hoge.Length; i++) 
                {
                    if(i%2==0)
                    yield return hoge[i];
                }
            }
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// IDisposable
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn38_4_Click(object sender, EventArgs e)
        {
            string str = string.Empty;
            using (DisposableStreamReader reader = new DisposableStreamReader())
            {
                str = reader.ReadAll();
            }
            MessageBox.Show(str);
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 明示的実装
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn38_5_Click(object sender, EventArgs e)
        {
            TestA t1 = new TestA();
            t1.Test();  // OK
            TestB t2 = new TestB();
            //t2.Test();   // NG
            ITestable t3 = t2 as TestB;
            t3.Test();  // OK
        }

        interface ITestable
        {
            void Test();
        }

        class TestA : ITestable
        {
            //IBarkable メンバ
            public void Test()
            {
                MessageBox.Show("TestA");
            }
            
        }

        class TestB : ITestable
        {
            //ITestable メンバ
            void ITestable.Test()
            {
                MessageBox.Show("TestB");
            }
            
        }

        #endregion

        #region++++++++++ 継承(39-1～39-7) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// クラスの継承
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn39_1_Click(object sender, EventArgs e)
        {
            C39_1Person p1 = new C39_1Person();
            p1.name = "天野舞耶";
            p1.age = 23;

            C39_1Student s1 = new C39_1Student();
            s1.name = "周防達也"; // Person のメンバーをそのまま利用出来る
            s1.age = 18;
            s1.id = 50012;

            C39_1Person p2 = s1; // Student は Person として扱うことが出来る。
            MessageBox.Show(String.Format("s1の名前:{0},年齢:{1}",p2.name,p2.age));
            //Student s2 = p1; // でも、Person は Student として扱っちゃ駄目。
            //↑この1行はエラーになる。

        }

        class C39_1Person
        {
            public string name; // 名前
            public int age;  // 年齢
        }

        class C39_1Student : C39_1Person
        {
            public int id;   // 学籍番号
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// object型
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn39_2_Click(object sender, EventArgs e)
        {
            //本項はプログラムか不可の為、MessageBOXにのみその旨表示する。
            MessageBox.Show("39_2は実装不可");
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// コンストラクタ呼び出し
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn39_3_Click(object sender, EventArgs e)
        {
            CDerived39_3 d = new CDerived39_3();
        }

        class CBase39_3
        {
            public CBase39_3()
            {
                MessageBox.Show("base constractor called");
            }
        }

        class CDerived39_3 : CBase39_3
        {
            public CDerived39_3()
            {
                MessageBox.Show("derived constractor called\n");
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 基底クラスのコンストラクタを明示的に呼び出す
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn39_4_Click(object sender, EventArgs e)
        {
            CPerson39_4 p1 = new CPerson39_4("天野舞耶", 23);
            CStudent39_4 s1 = new CStudent39_4("周防達也",18,50012);
            
            CPerson39_4 p2 = s1; // Student は Person として扱うことが出来る。
            MessageBox.Show(String.Format("s1の名前:{0},年齢:{1}", p2.Name, p2.Age));
            //Student s2 = p1; // でも、Person は Student として扱っちゃ駄目。
            //↑この1行はエラーになる。
        }

        class CPerson39_4
        {
            private string name; // 名前
            private int age;  // 年齢

            public CPerson39_4(string name, int age)
            {
                this.name = name;
                this.age = age;
            }

            public string Name
            {
                set { this.name = value; }
                get { return this.name; }
            }

            public int Age
            {
                set { this.age = value; }
                get { return this.age; }
            }
        }

        class CStudent39_4 : CPerson39_4
        {
            private int id;   // 学籍番号

            public CStudent39_4(string name, int age, int id)
                : base(name, age)
            {
                this.id = id;
            }

            public int Id
            {
                set { this.id = value; }
                get { return this.id; }
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// protected
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn39_5_Click(object sender, EventArgs e)
        {
            CBase39_5 b = new CBase39_5();

            b.public_val = "呼び出し元定義情報"; // OK
            //b.protected_val = 0; // エラー(protected は外部からアクセス不能)
            //b.private_val = 0; // エラー(private   は外部からアクセス不能)
            MessageBox.Show(string.Format("{0}",b.public_val));
        }

        class CBase39_5
        {
            public string public_val;
            protected string protected_val;
            private string private_val;

            void BaseTest()
            {
                public_val = string.Empty; // OK
                protected_val = string.Empty; ; // OK
                private_val = string.Empty; ; // OK
            }
        }

        class CDerived39_5 : CBase39_5
        {
            void DerivedTest()
            {
                public_val = string.Empty; ; // OK
                protected_val = string.Empty; ; // OK   (protected は派生クラスからアクセス可能)
                //private_val = string.Empty;; // エラー(private   は派生クラスからアクセス不能)
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 基底クラスのメンバーの隠蔽
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn39_6_Click(object sender, EventArgs e)
        {
            CBase39_6 b = new CBase39_6();
            b.Test(); // Base の Test が呼ばれる

            CDerived39_6 d = new CDerived39_6();
            d.Test(); // Derived の Test が呼ばれる

            ((CBase39_6)d).Test();
            // Base に キャストしてから Test を呼ぶと Base の Test が呼ばれる
        }

        class CBase39_6
        {
            public void Test()
            {
                MessageBox.Show("Base.Test()");
            }
        }

        class CDerived39_6 : CBase39_6
        {
            public new void Test() //基底クラスの Test() と同名のメソッド
            {
                MessageBox.Show("Derived.Test()");
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// sealed
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn39_7_Click(object sender, EventArgs e)
        {
            //継承不可である事をダイアログで表示
            MessageBox.Show("sealed class SealedClass { }\nclass Derived : SealedClass // SealedClass は継承不可なので、エラーになる。");
         
        }

        #endregion

        #region++++++++++ 多様性(40-1～40-2) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 静的な型、動的な型
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn40_1_Click(object sender, EventArgs e)
        {
            //多態性(polymorphism: ポリモーフィズム)とは、 同じメソッド呼び出し(オブジェクト指向用語的には「メッセージ」という)
            //に対して異なるオブジェクトが異なる動作をすること
            //「継承」で説明したとおり、 派生クラスのインスタンスは基底クラスの変数に格納することが出来ます。
            // このとき、変数の型を静的な型といい、 実際に格納されているインスタンスの型を動的な型といいます。
            ShowDinamicType(new CBase40_1());
            ShowDinamicType(new CDerived40_1());
        }

        class CBase40_1 { }
        class CDerived40_1 : CBase40_1 { }
        // Base 型の変数 b に格納されているインスタンスの動的な型の名前を表示する。
        static void ShowDinamicType(CBase40_1 b)
        {
            Type t = b.GetType();
            MessageBox.Show(t.Name);
        }
        

        ///// ********************************************************************************
        ///// <summary>
        ///// ダウンキャスト
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn40_2_Click(object sender, EventArgs e)
        {
            //基底クラスの変数に派生クラスの変数を渡すことをアップキャスト（upcast）
            //派生クラスの変数に基底クラスの変数を渡すことをダウンキャスト（downcast）
            CBase40_2 a = new CBase40_2();
            a.Test(); // Base の Test が呼ばれる。

            CBase40_2 b = new CDerived40_2();
            b.Test(); // Derived の Test が呼ばれる。

            CDerived40_2 c = new CDerived40_2();
            c.Test(); // Derived の Test が呼ばれる。
        }

        class CBase40_2
        {
            public virtual void Test() 
            {
                 MessageBox.Show("Base.Test()"); 
            }
        }

        class CDerived40_2 : CBase40_2
        {
            public override void Test() 
            {
                 MessageBox.Show("Derived.Test()"); 
            }
        }


        #endregion

    }




        #region++++++++++ static class ++++++++++
    ///// ********************************************************************************
        ///// <summary>
        ///// 静的コンストラクター
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.1.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        static class Math32_2
        {
            // double x; というような、非 static な変数・メソッドは定義できない。

            // sin x を求める関数。
            public static double Sin(double x)
            {
                double xx = -x * x;
                double fact = 1;
                double sin = x;
                for (int i = 0; i < 100; ++i)
                {
                    fact *= i; ++i; fact *= i; ++i;
                    x *= xx;
                    sin += x / fact;
                }
                return sin;
            }
        }

        static class Extensions32_3
        {
            public static int Parse(this string str)
            {
                return int.Parse(str);
            }
        }
    
        class Pair<K, V>
            {
                K key;
                V val;

                public K Key { get { return this.key; } set { this.key = value; } }
                public V Value { get { return this.val; } set { this.val = value; } }
            }
    
        //---------38_4---------------
        /// <summary>
        /// <see cref="IDisposable"/> を実装している = 使い終わったら明示的に Dispose を呼ぶ必要がある。
        /// </summary>
        public class DisposableStreamReader : IDisposable
        {
            StreamReader reader = null;
            public DisposableStreamReader()
            {
                reader = new StreamReader("hoge.txt");
            }

            public string ReadAll()
            {
                return reader.ReadToEnd();
            }
            public void Dispose()
            {
                reader.Dispose();
            }
        }

    #endregion

    #endregion
}
