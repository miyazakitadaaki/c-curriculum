﻿namespace curriculum1
{
    partial class FrmFunction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn41_1 = new System.Windows.Forms.Button();
            this.btn41_2 = new System.Windows.Forms.Button();
            this.btn41_3 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn41_6 = new System.Windows.Forms.Button();
            this.btn41_8 = new System.Windows.Forms.Button();
            this.btn41_7 = new System.Windows.Forms.Button();
            this.btn41_5 = new System.Windows.Forms.Button();
            this.btn41_4 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn42_3 = new System.Windows.Forms.Button();
            this.btn42_2 = new System.Windows.Forms.Button();
            this.btn42_1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn43_4 = new System.Windows.Forms.Button();
            this.btn43_3 = new System.Windows.Forms.Button();
            this.btn43_2 = new System.Windows.Forms.Button();
            this.btn43_1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn44_7 = new System.Windows.Forms.Button();
            this.btn44_6 = new System.Windows.Forms.Button();
            this.btn44_5 = new System.Windows.Forms.Button();
            this.btn44_4 = new System.Windows.Forms.Button();
            this.btn44_3 = new System.Windows.Forms.Button();
            this.btn44_2 = new System.Windows.Forms.Button();
            this.btn44_1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(75, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(301, 33);
            this.label1.TabIndex = 0;
            this.label1.Text = "関数指向(41-1_44-7)";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1057, 53);
            this.panel1.TabIndex = 2;
            // 
            // btn41_1
            // 
            this.btn41_1.Location = new System.Drawing.Point(10, 18);
            this.btn41_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn41_1.Name = "btn41_1";
            this.btn41_1.Size = new System.Drawing.Size(98, 37);
            this.btn41_1.TabIndex = 3;
            this.btn41_1.Text = "デリゲートインスタンスメソッド(41-1)";
            this.btn41_1.UseVisualStyleBackColor = true;
            // 
            // btn41_2
            // 
            this.btn41_2.Location = new System.Drawing.Point(144, 18);
            this.btn41_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn41_2.Name = "btn41_2";
            this.btn41_2.Size = new System.Drawing.Size(98, 37);
            this.btn41_2.TabIndex = 3;
            this.btn41_2.Text = "複数のメソッドを代入(41-2)";
            this.btn41_2.UseVisualStyleBackColor = true;
            // 
            // btn41_3
            // 
            this.btn41_3.Location = new System.Drawing.Point(275, 18);
            this.btn41_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn41_3.Name = "btn41_3";
            this.btn41_3.Size = new System.Drawing.Size(98, 37);
            this.btn41_3.TabIndex = 3;
            this.btn41_3.Text = "非同期呼び出し(41-3)";
            this.btn41_3.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn41_6);
            this.groupBox1.Controls.Add(this.btn41_8);
            this.groupBox1.Controls.Add(this.btn41_7);
            this.groupBox1.Controls.Add(this.btn41_5);
            this.groupBox1.Controls.Add(this.btn41_4);
            this.groupBox1.Controls.Add(this.btn41_3);
            this.groupBox1.Controls.Add(this.btn41_2);
            this.groupBox1.Controls.Add(this.btn41_1);
            this.groupBox1.Location = new System.Drawing.Point(11, 69);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(381, 164);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "デリゲート";
            // 
            // btn41_6
            // 
            this.btn41_6.Location = new System.Drawing.Point(275, 63);
            this.btn41_6.Margin = new System.Windows.Forms.Padding(2);
            this.btn41_6.Name = "btn41_6";
            this.btn41_6.Size = new System.Drawing.Size(98, 37);
            this.btn41_6.TabIndex = 9;
            this.btn41_6.Text = "ラムダ式(41-6)";
            this.btn41_6.UseVisualStyleBackColor = true;
            // 
            // btn41_8
            // 
            this.btn41_8.Location = new System.Drawing.Point(130, 109);
            this.btn41_8.Margin = new System.Windows.Forms.Padding(2);
            this.btn41_8.Name = "btn41_8";
            this.btn41_8.Size = new System.Drawing.Size(124, 37);
            this.btn41_8.TabIndex = 4;
            this.btn41_8.Text = "contravariance(41-8)";
            this.btn41_8.UseVisualStyleBackColor = true;
            // 
            // btn41_7
            // 
            this.btn41_7.Location = new System.Drawing.Point(10, 109);
            this.btn41_7.Margin = new System.Windows.Forms.Padding(2);
            this.btn41_7.Name = "btn41_7";
            this.btn41_7.Size = new System.Drawing.Size(98, 37);
            this.btn41_7.TabIndex = 5;
            this.btn41_7.Text = "covariance(41-7)";
            this.btn41_7.UseVisualStyleBackColor = true;
            // 
            // btn41_5
            // 
            this.btn41_5.Location = new System.Drawing.Point(144, 63);
            this.btn41_5.Margin = new System.Windows.Forms.Padding(2);
            this.btn41_5.Name = "btn41_5";
            this.btn41_5.Size = new System.Drawing.Size(98, 37);
            this.btn41_5.TabIndex = 4;
            this.btn41_5.Text = "匿名メソッド式(41-5)";
            this.btn41_5.UseVisualStyleBackColor = true;
            // 
            // btn41_4
            // 
            this.btn41_4.Location = new System.Drawing.Point(10, 63);
            this.btn41_4.Margin = new System.Windows.Forms.Padding(2);
            this.btn41_4.Name = "btn41_4";
            this.btn41_4.Size = new System.Drawing.Size(98, 37);
            this.btn41_4.TabIndex = 5;
            this.btn41_4.Text = "述語(41-4)";
            this.btn41_4.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn42_3);
            this.groupBox2.Controls.Add(this.btn42_2);
            this.groupBox2.Controls.Add(this.btn42_1);
            this.groupBox2.Location = new System.Drawing.Point(11, 247);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(381, 68);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ラムダ式";
            // 
            // btn42_3
            // 
            this.btn42_3.Location = new System.Drawing.Point(275, 18);
            this.btn42_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn42_3.Name = "btn42_3";
            this.btn42_3.Size = new System.Drawing.Size(98, 37);
            this.btn42_3.TabIndex = 3;
            this.btn42_3.Text = "初期化子(42-3)";
            this.btn42_3.UseVisualStyleBackColor = true;
            // 
            // btn42_2
            // 
            this.btn42_2.Location = new System.Drawing.Point(144, 18);
            this.btn42_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn42_2.Name = "btn42_2";
            this.btn42_2.Size = new System.Drawing.Size(98, 37);
            this.btn42_2.TabIndex = 3;
            this.btn42_2.Text = "式木(42-2)";
            this.btn42_2.UseVisualStyleBackColor = true;
            // 
            // btn42_1
            // 
            this.btn42_1.Location = new System.Drawing.Point(10, 18);
            this.btn42_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn42_1.Name = "btn42_1";
            this.btn42_1.Size = new System.Drawing.Size(120, 37);
            this.btn42_1.TabIndex = 3;
            this.btn42_1.Text = "匿名メソッドの記法の簡略化(42-1)";
            this.btn42_1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn43_4);
            this.groupBox3.Controls.Add(this.btn43_3);
            this.groupBox3.Controls.Add(this.btn43_2);
            this.groupBox3.Controls.Add(this.btn43_1);
            this.groupBox3.Location = new System.Drawing.Point(11, 329);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(381, 110);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "イベント";
            // 
            // btn43_4
            // 
            this.btn43_4.Location = new System.Drawing.Point(10, 64);
            this.btn43_4.Margin = new System.Windows.Forms.Padding(2);
            this.btn43_4.Name = "btn43_4";
            this.btn43_4.Size = new System.Drawing.Size(98, 37);
            this.btn43_4.TabIndex = 4;
            this.btn43_4.Text = "C# の event 構文(43-4)";
            this.btn43_4.UseVisualStyleBackColor = true;
            // 
            // btn43_3
            // 
            this.btn43_3.Location = new System.Drawing.Point(275, 18);
            this.btn43_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn43_3.Name = "btn43_3";
            this.btn43_3.Size = new System.Drawing.Size(98, 37);
            this.btn43_3.TabIndex = 3;
            this.btn43_3.Text = "イベント ハンドラー(43-3)";
            this.btn43_3.UseVisualStyleBackColor = true;
            // 
            // btn43_2
            // 
            this.btn43_2.Location = new System.Drawing.Point(144, 18);
            this.btn43_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn43_2.Name = "btn43_2";
            this.btn43_2.Size = new System.Drawing.Size(110, 37);
            this.btn43_2.TabIndex = 3;
            this.btn43_2.Text = "イベント駆動型プログラムの例(43-2)";
            this.btn43_2.UseVisualStyleBackColor = true;
            // 
            // btn43_1
            // 
            this.btn43_1.Location = new System.Drawing.Point(10, 18);
            this.btn43_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn43_1.Name = "btn43_1";
            this.btn43_1.Size = new System.Drawing.Size(120, 37);
            this.btn43_1.TabIndex = 3;
            this.btn43_1.Text = "イベント駆動型(43-1)";
            this.btn43_1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn44_7);
            this.groupBox4.Controls.Add(this.btn44_6);
            this.groupBox4.Controls.Add(this.btn44_5);
            this.groupBox4.Controls.Add(this.btn44_4);
            this.groupBox4.Controls.Add(this.btn44_3);
            this.groupBox4.Controls.Add(this.btn44_2);
            this.groupBox4.Controls.Add(this.btn44_1);
            this.groupBox4.Location = new System.Drawing.Point(445, 69);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(381, 164);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "拡張メソッド";
            // 
            // btn44_7
            // 
            this.btn44_7.Location = new System.Drawing.Point(10, 109);
            this.btn44_7.Margin = new System.Windows.Forms.Padding(2);
            this.btn44_7.Name = "btn44_7";
            this.btn44_7.Size = new System.Drawing.Size(112, 37);
            this.btn44_7.TabIndex = 6;
            this.btn44_7.Text = "拡張メソッドのデリゲートへの代入(44-7)";
            this.btn44_7.UseVisualStyleBackColor = true;
            // 
            // btn44_6
            // 
            this.btn44_6.Location = new System.Drawing.Point(274, 63);
            this.btn44_6.Margin = new System.Windows.Forms.Padding(2);
            this.btn44_6.Name = "btn44_6";
            this.btn44_6.Size = new System.Drawing.Size(98, 37);
            this.btn44_6.TabIndex = 6;
            this.btn44_6.Text = "拡張メソッドの意義(44-6)";
            this.btn44_6.UseVisualStyleBackColor = true;
            // 
            // btn44_5
            // 
            this.btn44_5.Location = new System.Drawing.Point(156, 63);
            this.btn44_5.Margin = new System.Windows.Forms.Padding(2);
            this.btn44_5.Name = "btn44_5";
            this.btn44_5.Size = new System.Drawing.Size(98, 37);
            this.btn44_5.TabIndex = 5;
            this.btn44_5.Text = "拡張メソッドの問題点(44-5)";
            this.btn44_5.UseVisualStyleBackColor = true;
            // 
            // btn44_4
            // 
            this.btn44_4.Location = new System.Drawing.Point(10, 63);
            this.btn44_4.Margin = new System.Windows.Forms.Padding(2);
            this.btn44_4.Name = "btn44_4";
            this.btn44_4.Size = new System.Drawing.Size(124, 37);
            this.btn44_4.TabIndex = 4;
            this.btn44_4.Text = "インターフェースに拡張メソッドを追加(44-4)";
            this.btn44_4.UseVisualStyleBackColor = true;
            // 
            // btn44_3
            // 
            this.btn44_3.Location = new System.Drawing.Point(274, 18);
            this.btn44_3.Margin = new System.Windows.Forms.Padding(2);
            this.btn44_3.Name = "btn44_3";
            this.btn44_3.Size = new System.Drawing.Size(98, 37);
            this.btn44_3.TabIndex = 3;
            this.btn44_3.Text = "優先順位(44-3)";
            this.btn44_3.UseVisualStyleBackColor = true;
            // 
            // btn44_2
            // 
            this.btn44_2.Location = new System.Drawing.Point(121, 18);
            this.btn44_2.Margin = new System.Windows.Forms.Padding(2);
            this.btn44_2.Name = "btn44_2";
            this.btn44_2.Size = new System.Drawing.Size(147, 37);
            this.btn44_2.TabIndex = 3;
            this.btn44_2.Text = "using ディレクティブによる拡張メソッドのインポート(44-2)";
            this.btn44_2.UseVisualStyleBackColor = true;
            // 
            // btn44_1
            // 
            this.btn44_1.Location = new System.Drawing.Point(10, 18);
            this.btn44_1.Margin = new System.Windows.Forms.Padding(2);
            this.btn44_1.Name = "btn44_1";
            this.btn44_1.Size = new System.Drawing.Size(98, 37);
            this.btn44_1.TabIndex = 3;
            this.btn44_1.Text = "拡張メソッド(44-1)";
            this.btn44_1.UseVisualStyleBackColor = true;
            // 
            // FrmFunction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 537);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmFunction";
            this.Text = "FrmStructure";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn41_1;
        private System.Windows.Forms.Button btn41_2;
        private System.Windows.Forms.Button btn41_3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn41_6;
        private System.Windows.Forms.Button btn41_5;
        private System.Windows.Forms.Button btn41_4;
        private System.Windows.Forms.Button btn41_8;
        private System.Windows.Forms.Button btn41_7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn42_3;
        private System.Windows.Forms.Button btn42_2;
        private System.Windows.Forms.Button btn42_1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn43_3;
        private System.Windows.Forms.Button btn43_2;
        private System.Windows.Forms.Button btn43_1;
        private System.Windows.Forms.Button btn43_4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btn44_4;
        private System.Windows.Forms.Button btn44_3;
        private System.Windows.Forms.Button btn44_2;
        private System.Windows.Forms.Button btn44_1;
        private System.Windows.Forms.Button btn44_5;
        private System.Windows.Forms.Button btn44_7;
        private System.Windows.Forms.Button btn44_6;
    }
}