﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices; 
using System.IO;
using curriculum.FormMessage.Frm_P層_.表示用Form;
using System.Threading;
using System.Linq.Expressions;

namespace curriculum1
{
    using NamespaceA;
    //using NamespaceB;
    // ↑
    // ここのコメントを外してもやっぱりエラー。
    // using NamespaceA をコメントアウトして、
    // 代りに using NamespaceB するなら OK（表示結果が変わる）。

    /// ********************************************************************************
    /// <summary>
    /// 構造化フォーム
    /// </summary>
    /// <remarks>none</remarks>
    /// 
    /// <history>
    /// --------------------------------------------------------------------------------
    /// No     Date         User         Remorks
    /// --------------------------------------------------------------------------------
    /// 0001   2015.12.05   宮崎祥彰   *new*
    /// 
    /// </history>
    /// ********************************************************************************
    public partial class FrmFunction : Base_Form
    {


        #region ++++++++++ member variable ++++++++++
        public enum TopicNo
        {
            Topic43_2 = FrmObject.TopicNo.Topic28_4 + 1
        }
        #endregion

        #region++++++++++ constructor/destructor ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰     *new*
        /// 
        /// </history>
        /// ********************************************************************************
        public FrmFunction()
        {
            // -------------------------------------------------------
            // 初期化処理
            InitializeComponent();
            this.EntryEvents();
        }

        #endregion

        #region ++++++++++ event ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// イベント Form　Load時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void FormMain_Load(object sender, EventArgs e)
        {

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = !this.MaximizeBox;

        }

        /// ********************************************************************************
        /// <summary>
        /// イベントハンドラ登録
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void EntryEvents()
        {

            this.Load += new EventHandler(this.FormMain_Load);
            this.btn41_1.Click += new EventHandler(this.btn41_1_Click);
            this.btn41_2.Click += new EventHandler(this.btn41_2_Click);
            this.btn41_3.Click += new EventHandler(this.btn41_3_Click);
            this.btn41_4.Click += new EventHandler(this.btn41_4_Click);
            this.btn41_5.Click += new EventHandler(this.btn41_5_Click);
            this.btn41_6.Click += new EventHandler(this.btn41_6_Click);
            this.btn41_7.Click += new EventHandler(this.btn41_7_Click);
            this.btn41_8.Click += new EventHandler(this.btn41_8_Click);
            this.btn42_1.Click += new EventHandler(this.btn42_1_Click);
            this.btn42_2.Click += new EventHandler(this.btn42_2_Click);
            this.btn42_3.Click += new EventHandler(this.btn42_3_Click);
            this.btn43_1.Click += new EventHandler(this.btn43_1_Click);
            this.btn43_2.Click += new EventHandler(this.btn43_2_Click);
            this.btn43_3.Click += new EventHandler(this.btn43_3_Click);
            this.btn43_4.Click += new EventHandler(this.btn43_4_Click);
            this.btn44_1.Click += new EventHandler(this.btn44_1_Click);
            this.btn44_2.Click += new EventHandler(this.btn44_2_Click);
            this.btn44_3.Click += new EventHandler(this.btn44_3_Click);
            this.btn44_4.Click += new EventHandler(this.btn44_4_Click);
            this.btn44_5.Click += new EventHandler(this.btn44_5_Click);
            this.btn44_6.Click += new EventHandler(this.btn44_6_Click);
            this.btn44_7.Click += new EventHandler(this.btn44_7_Click);

        }

        #region++++++++++デリゲート(41-1～41_8) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// デリゲートインスタンスメソッド
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************

        /// <summary>
        /// メッセージを表示するだけのデリゲート
        /// </summary>
        delegate void ShowMessage();
        private void btn41_1_Click(object sender, EventArgs e)
        {
            Person_41_1 p = new Person_41_1("鬼丸美輝");

            // インスタンスメソッドを代入。
            ShowMessage show = new ShowMessage(p.ShowName);

            show();
        }
        class Person_41_1
        {
            string name;
            public Person_41_1(string name) { this.name = name; }
            public void ShowName() { MessageBox.Show(string.Format("名前: {0}\n", this.name)); }
        };

        ///// ********************************************************************************
        ///// <summary>
        ///// 複数のメソッドを代入
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn41_2_Click(object sender, EventArgs e)
        {
            Person_41_1 p1 = new Person_41_1("鬼丸美輝");
            Person_41_1 p2 = new Person_41_1("神無月めぐみ");

            ShowMessage show = new ShowMessage(p1.ShowName);
            show += new ShowMessage(p2.ShowName);
            show += new ShowMessage(A41_2);
            show += new ShowMessage(B41_2);
            

            show();
        }
        static void A41_2() { MessageBox.Show("A が呼ばれました。\n"); }
        static void B41_2() { MessageBox.Show("B が呼ばれました。\n"); }
        ///// ********************************************************************************
        ///// <summary>
        ///// 非同期呼び出し
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        /// <summary>
        /// メッセージを表示するだけのデリゲート
        /// </summary>
        public delegate void ShowMessage41_3(int n);
        private void btn41_3_Click(object sender, EventArgs e)
        {
            const int N = 6;
            ShowMessage41_3 asyncCall = new ShowMessage41_3(AsynchronousMethod41_3);

            // asyncCall を非同期で呼び出す。
            IAsyncResult ar = asyncCall.BeginInvoke(N, null, null);

            // ↓この部分は asyncCall によって呼び出されるメソッドと同時に実行されます。
            for (int i = 0; i < N; ++i)
            {
                Thread.Sleep(600);
                Console.Write("Main ({0})\n", i);
            }

            // asyncCall の処理が終わるのを待つ。
            asyncCall.EndInvoke(ar);

            MessageBox.Show(" 処理完了\n");
        }
        static void AsynchronousMethod41_3(int n)
        {
          for(int i=0; i<n; ++i)
          {
            Thread.Sleep(1000);
            MessageBox.Show(string.Format("AsynchronousMethod ({0})\n", i));
          }
        }
        static void B(IAsyncResult ar){}

        ///// ********************************************************************************
        ///// <summary>
        ///// 述語
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        delegate bool Predicate41_4(int n);
        private void btn41_4_Click(object sender, EventArgs e)
        {
            int[] x = new int[] { 1, 8, 4, 11, 8, 15, 12, 19 };

            // x の中から値が 10 以上のもだけ取り出す
            int[] y = Select(x, new Predicate41_4(IsOver10));
            foreach (int i in y)
                MessageBox.Show(string.Format("{0}  ", i));

            // x の中から値が (5, 15) の範囲にあるものだけ取り出す
            int[] z = Select(x, new Predicate41_4(Is5to15));
            foreach (int i in z)
                MessageBox.Show(string.Format("{0}  ", i));
        }
        static bool IsOver10(int n) { return n > 10; }
        static bool Is5to15(int n) { return (n > 5) && (n < 15); }
        /// <summary>
        /// x の中から条件 pred を満たすものだけを取り出す。
        /// </summary>
        /// <param name="x">対象となる配列</param>
        /// <param name="pred">述語</param>
        /// <returns>条件を満たすものだけを取り出した配列</returns>
        static int[] Select(int[] x, Predicate41_4 pred)
        {
            int n = 0;
            foreach (int i in x)
                if (pred(i)) ++n;

            int[] y = new int[n];

            n = 0;
            foreach (int i in x)
                if (pred(i))
                {
                    y[n] = i;
                    ++n;
                }

            return y;
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// 匿名メソッド式
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn41_5_Click(object sender, EventArgs e)
        {
            int[] x = new int[] { 1, 8, 4, 11, 8, 15, 12, 19 };

            // x の中から値が 10 以上のもだけ取り出す
            int[] y = Select(x,
              delegate(int n) { return n > 10; }
            );
            foreach (int i in y)
               MessageBox.Show(string.Format("{0}  ", i));

            // x の中から値が (5, 15) の範囲にあるものだけ取り出す
            int[] z = Select(x,
              delegate(int n) { return (n > 5) && (n < 15); }
            );
            foreach (int i in z)
                MessageBox.Show(string.Format("{0}  ", i));
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// ラムダ式
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn41_6_Click(object sender, EventArgs e)
        { int[] x = new int[] { 1, 8, 4, 11, 8, 15, 12, 19 };

            // x の中から値が 10 以上のもだけ取り出す
            int[] y = Select(x,
              (int n) => { return n > 10; }
            );
            foreach (int i in y)
               MessageBox.Show(string.Format("{0}  ", i));

            // x の中から値が (5, 15) の範囲にあるものだけ取り出す
            int[] z = Select(x,
              (int n) => { return (n > 5) && (n < 15); }
            );
            foreach (int i in z)
                MessageBox.Show(string.Format("{0}  ", i));
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// covariance
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        class Base41_7 {
            string x = "Baseクラスの文字列";
            public void ShowName() { MessageBox.Show(string.Format("{0}", this.x)); }
            }
        class Derived41_7 : Base41_7 {}
        delegate Base41_7 DelegateBaseReturn();
        private void btn41_7_Click(object sender, EventArgs e)
        {
            Base41_7 xb;
            xb = BaseReturn();      // 型が完全一致。
            xb = DerivedReturn();   // 基底クラスへのキャストは合法。

            DelegateBaseReturn db;
            db = BaseReturn;       // 型が完全一致。
            db += DerivedReturn;    // 戻り値の型が違うけど、これも OK。
            xb = db();
            xb.ShowName();
        }
        static Base41_7 BaseReturn() { return new Base41_7(); }
        static Derived41_7 DerivedReturn() { return new Derived41_7(); }

        ///// ********************************************************************************
        ///// <summary>
        ///// contravariance
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        delegate void DelegateDerivedParameter(Derived41_7 x);
        private void btn41_8_Click(object sender, EventArgs e)
        {
            Derived41_7 xd = new Derived41_7();
            DerivedParameter(xd);   // 型が完全一致。
            BaseParameter(xd);      // 基底クラスへのキャストは合法。
            xd.ShowName();

            DelegateDerivedParameter dd;
            dd = DerivedParameter; // 型が完全一致。
            dd += BaseParameter;    // 引数の型が違うけど、これも OK。
            dd(xd);
        }
        static void BaseParameter(Base41_7 x) { }
        static void DerivedParameter(Derived41_7 x) { }

        #endregion

        #region++++++++++ラムダ式(42-1～42_3) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 匿名メソッドの記法の簡略化
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn42_1_Click(object sender, EventArgs e)
        {

            Func<int, int, int> f =
                 (x, y) =>
                 {
                     int sum = x + y;
                     int prod = x * y;
                     return sum * prod;
                 };

            MessageBox.Show(string.Format("{0}",f(1,2)));
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// 式木
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn42_2_Click(object sender, EventArgs e)
        {
            Expression<Func<int, bool>> predict = n => n > 0;
            BinaryExpression lt = (BinaryExpression)predict.Body;
            ParameterExpression en = (ParameterExpression)lt.Left;
            ConstantExpression zero = (ConstantExpression)lt.Right;

            MessageBox.Show(string.Format("{0}\n{1}\n{2}", predict, lt.Left, lt.Right));
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 初期化子
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn42_3_Click(object sender, EventArgs e)
        {
            Triangle42_3 A = new Triangle42_3();
            MessageBox.Show(string.Format("{0}:{1}:{2}",A.A,A.B,A.C));        
        }
        class Triangle42_3
        {
            public Point A = new Point { X = 0, Y = 0 };
            public Point B = new Point { X = 1, Y = 0 };
            public Point C = new Point { X = 0, Y = 1 };
            // ↑メンバー変数の初期化に複文は書けないの。
        }
        #endregion

        #region++++++++++イベント(43-1～43_4) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// イベント駆動型
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        //「キーボードのボタンが押された」とか「マウスが移動した」等の、
        //コンピュータ上で発生するなんらかの事象のことをイベント（event）といい、
        //イベントが発生したときに行う処理のことをイベント ハンドラー（event handler）と呼びます。
        //このように、イベントとそれに対する処理により動作するようなプログラムのことをイベント駆動型（event drive）プログラムと呼びます。
        private void btn43_1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("削除予定");
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// イベント駆動型プログラムの例
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        // 時刻の表示形式
        const string FULL = "yyyy/dd/MM hh:mm:ss\n";
        const string DATE = "yyyy/dd/MM\n";
        const string TIME = "hh:mm:ss\n";
        static bool isSuspended = true;  // プログラムの一時停止フラグ。
        static string timeFormat = TIME; // 時刻の表示形式。
        static FormMessage frmMessage;
        static Thread keyPush;
        private void btn43_2_Click(object sender, EventArgs e)
        {
            frmMessage = new FormMessage(TopicNo.Topic43_2);
            frmMessage.Show();

            keyPush = new Thread(startTask43_2);
            keyPush.Start();
        }
        // 毎秒時刻表示のループ
        private static async Task TimerLoop(CancellationToken ct)
        {
            while (!ct.IsCancellationRequested)
            {
                if (!isSuspended)
                {
                    // 1秒おきに現在時刻を表示。
                    //frmMessage.label2.Text = DateTime.Now.ToString(timeFormat);
                    MessageBoxTimeout.Show(DateTime.Now.ToString(timeFormat),999);
                
                }
                await Task.Delay(1000);
                
            }
        }

        private void startTask43_2()
        {
            var cts = new CancellationTokenSource();
            Task.WhenAll(
                Task.Run(() => EventLoop(cts)),
                TimerLoop(cts.Token)
                ).Wait();
        }

        // キー受付のループ
        static void EventLoop(CancellationTokenSource cts)
        {
            while (!cts.IsCancellationRequested)
            {
                try
                {
                    // 文字を読み込む
                    // (「キーが押される」というイベントの発生を待つ)
                    if (frmMessage == null) cts.Cancel();
                    string line = frmMessage.textBox1.Text;
                    char eventCode = line.Length == 0 ? '\0' : char.Parse(line);

                    // イベント処理
                    switch (eventCode)
                    {
                        case 'r': // run
                            isSuspended = false;
                            break;
                        case 's': // suspend
                            isSuspended = true;
                            break;
                        case 'f': // full
                            timeFormat = FULL;
                            break;
                        case 'd': // date
                            timeFormat = DATE;
                            break;
                        case 't': // time
                            timeFormat = TIME;
                            break;
                        case 'q': // quit
                            cts.Cancel();
                            break;
                    }
                }
                catch (Exception)
                {
                    cts.Cancel();
                }
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// イベント ハンドラー
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        static KeyboardEventLoop43_3 eventLoop;
        private void btn43_3_Click(object sender, EventArgs e)
        {
            frmMessage = new FormMessage(TopicNo.Topic43_2);
            frmMessage.Show();

            keyPush = new Thread(startTask43_3);
            keyPush.Start();
        }
        private void startTask43_3()
        {
            var cts = new CancellationTokenSource();
            eventLoop = new KeyboardEventLoop43_3(code => OnKeyDown(code, cts));

            Task.WhenAll(
                eventLoop.Start(cts.Token),
                TimerLoop(cts.Token)
                ).Wait();
        }

        // イベント処理部。
        static void OnKeyDown(char eventCode, CancellationTokenSource cts)
        {
            switch (eventCode)
            {
                case 'r': // run
                    isSuspended = false;
                    break;
                case 's': // suspend
                    if (!isSuspended)
                    {
                        MessageBox.Show("\n一時停止します\n");
                        isSuspended = true;
                    }
                    break;
                case 'f': // full
                    timeFormat = FULL;
                    break;
                case 'd': // date
                    timeFormat = DATE;
                    break;
                case 't': // time
                    timeFormat = TIME;
                    break;
                case 'q': // quit
                    cts.Cancel();
                    break;
            }
        }

        // イベント処理用のデリゲート
        delegate void KeyboadEventHandler43_3(char eventCode);

        /// <summary>
        /// キーボードからの入力イベント待受けクラス。
        /// </summary>
        class KeyboardEventLoop43_3
        {
            KeyboadEventHandler43_3 _onKeyDown;

            public KeyboardEventLoop43_3(KeyboadEventHandler43_3 onKeyDown)
            {
                _onKeyDown = onKeyDown;
            }

            /// <summary>
            /// 待受け開始。
            /// </summary>
            /// <param name="ct">待ち受けを終了したいときにキャンセルする。</param>
            public Task Start(CancellationToken ct)
            {
                return Task.Run(() => EventLoop(ct));
            }

            /// <summary>
            /// イベント待受けループ。
            /// </summary>
            void EventLoop(CancellationToken ct)
            {
                // イベントループ
                while (!ct.IsCancellationRequested)
                {
                    try
                    {
                        // 文字を読み込む
                        // (「キーが押される」というイベントの発生を待つ)
                        string line = frmMessage.textBox1.Text;
                        char eventCode = line.Length == 0 ? '\0' : char.Parse(line);

                        // イベント処理はデリゲートを通して他のメソッドに任せる。
                        _onKeyDown(eventCode);
                    }
                    catch (Exception)
                    {
                        _onKeyDown(char.Parse("q"));
                    }
                }
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// C# の event 構文
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        // イベント処理用のデリゲート
        delegate void KeyboadEventHandler43_4(char eventCode);
        static KeyboardEventLoop43_4 eventLoop43_4;
        private void btn43_4_Click(object sender, EventArgs e)
        {
            frmMessage = new FormMessage(TopicNo.Topic43_2);
            frmMessage.Show();

            keyPush = new Thread(startTask43_4);
            keyPush.Start();
        }
        private void startTask43_4()
        {
            var cts = new CancellationTokenSource();
            eventLoop43_4 = new KeyboardEventLoop43_4(code => OnKeyDown(code, cts));

            Task.WhenAll(
                eventLoop43_4.Start(cts.Token),
                TimerLoop(cts.Token)
                ).Wait();
        }

        /// <summary>
        /// キーボードからの入力イベント待受けクラス。
        /// </summary>
        class KeyboardEventLoop43_4
        {
            /// <summary>
            /// キー入力があった時に呼ばれるイベント。
            /// </summary>
            public event KeyboadEventHandler43_4 OnKeyDown;

            public KeyboardEventLoop43_4() { }
            public KeyboardEventLoop43_4(KeyboadEventHandler43_4 onKeyDown)
            {
                OnKeyDown += onKeyDown;
            }

            /// <summary>
            /// 待受け開始。
            /// </summary>
            /// <param name="ct">待ち受けを終了したいときにキャンセルする。</param>
            public Task Start(CancellationToken ct)
            {
                return Task.Run(() => EventLoop(ct));
            }

            /// <summary>
            /// イベント待受けループ。
            /// </summary>
            void EventLoop(CancellationToken ct)
            {
                // イベントループ
                while (!ct.IsCancellationRequested)
                {
                    try
                    {
                        // 文字を読み込む
                        // (「キーが押される」というイベントの発生を待つ)
                        string line = frmMessage.textBox1.Text;
                        char eventCode = line.Length == 0 ? '\0' : char.Parse(line);

                        // イベント処理はデリゲートを通して他のメソッドに任せる。
                        OnKeyDown(eventCode);
                    }
                    catch (Exception)
                    {
                        OnKeyDown(char.Parse("q"));
                    }
                }
            }
        }

        #endregion

        #region++++++++++拡張メソッド(44-1～44_7) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 拡張メソッド
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn44_1_Click(object sender, EventArgs e)
        {
            string s = "This Is a Test String.";
            MessageBox.Show(s.ToggleCase());
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// using ディレクティブによる拡張メソッドのインポート
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn44_2_Click(object sender, EventArgs e)
        {
            1.WriteToConsole();
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 優先順位
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn44_3_Click(object sender, EventArgs e)
        {
            string x = "44_3";
            x.showMessage();
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// インターフェースに拡張メソッドを追加
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn44_4_Click(object sender, EventArgs e)
        {
            IEnumerable<int> data = new int[] { 1, 2, 3 };

            // ↓インターフェースに対してメソッドを追加できる
            data = data.Duplicate();

            foreach (var x in data)
                MessageBox.Show(string.Format("{0}\n", x));
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 拡張メソッドの問題点
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        //拡張メソッドの濫用は避けた方がいいでしょう。
        //拡張メソッドの濫用には不便な点もありますし、 いくつか問題を起こす可能性があります。
        //削除予定
        private void btn44_5_Click(object sender, EventArgs e)
        {
            MessageBox.Show("削除予定");
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 拡張メソッドの意義
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn44_6_Click(object sender, EventArgs e)
        {
            var input = new[] { 8, 9, 10, 11, 12, 13 };

            var output = input
                .Where(x => x > 10)
                .Select(x => x * x);
            foreach (var x in output)
                MessageBox.Show(string.Format("{0}\n", x));
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 拡張メソッドのデリゲートへの代入
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn44_7_Click(object sender, EventArgs e)
        {
            Func<string> f = "test".Duplicate;
            // ↑
            // 実行結果的には
            // Func<string> f = () => Extensions.Duplicate("test");
            // と同じ。
            // コンパイル結果的には、こんな余計な匿名デリゲートはできないらしい。
            // 直接 f に Extensions.Duplicate("test") が代入されるようなイメージ。
            MessageBox.Show(string.Format("{0}\n", f));
        }

        #endregion

        #endregion
    }

    static class Extensions
    {

        public static void showMessage(this string x)
        {
            MessageBox.Show("thisClass:{0}", x);
        }

        public static IEnumerable<T> Duplicate<T>(this IEnumerable<T> list)
        {
            foreach (var x in list)
            {
                yield return x;
                yield return x;
            }
        }

        public static string Duplicate(this string x)
        {
            return x + x;
        }

        public static IEnumerable<int> Where(this IEnumerable<int> array, Func<int, bool> pred)
        {
            foreach (var x in array)
                if (pred(x))
                    yield return x;
        }

        public static IEnumerable<int> Select(this IEnumerable<int> array, Func<int, int> filter)
        {
            foreach (var x in array)
                yield return filter(x);
        }
    }
}
namespace NamespaceA
{
    static class Extensions
    {
        public static void WriteToConsole(this int x)
        {
            MessageBox.Show(string.Format("A {0}", x));
        }

        public static void showMessage(this string x)
        {
            MessageBox.Show("A:{0}", x);
        }
    }
}

namespace NamespaceB
{
    static class Extensions
    {
        public static void WriteToConsole(this int x)
        {
            MessageBox.Show(string.Format("B {0}", x));
        }
    }
}