﻿namespace curriculum1
{
    partial class FrmMemory_Resource
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn45_3 = new System.Windows.Forms.Button();
            this.btn45_2 = new System.Windows.Forms.Button();
            this.btn45_1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn45_6 = new System.Windows.Forms.Button();
            this.btn45_5 = new System.Windows.Forms.Button();
            this.btn45_4 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btn51_3 = new System.Windows.Forms.Button();
            this.btn51_2 = new System.Windows.Forms.Button();
            this.btn51_1 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.btn50_3 = new System.Windows.Forms.Button();
            this.btn50_2 = new System.Windows.Forms.Button();
            this.btn50_1 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btn49_3 = new System.Windows.Forms.Button();
            this.btn49_2 = new System.Windows.Forms.Button();
            this.btn49_1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn48_3 = new System.Windows.Forms.Button();
            this.btn48_2 = new System.Windows.Forms.Button();
            this.btn48_1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn47_3 = new System.Windows.Forms.Button();
            this.btn47_2 = new System.Windows.Forms.Button();
            this.btn47_1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn46_3 = new System.Windows.Forms.Button();
            this.btn46_2 = new System.Windows.Forms.Button();
            this.btn46_1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(100, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(516, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "メモリとリソース管理(45-1_51-3)";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-1, 3);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1134, 66);
            this.panel1.TabIndex = 10;
            // 
            // btn45_3
            // 
            this.btn45_3.Location = new System.Drawing.Point(367, 22);
            this.btn45_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn45_3.Name = "btn45_3";
            this.btn45_3.Size = new System.Drawing.Size(131, 46);
            this.btn45_3.TabIndex = 3;
            this.btn45_3.Text = "引数なしコンストラクター(45-3)";
            this.btn45_3.UseVisualStyleBackColor = true;
            // 
            // btn45_2
            // 
            this.btn45_2.Location = new System.Drawing.Point(192, 22);
            this.btn45_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn45_2.Name = "btn45_2";
            this.btn45_2.Size = new System.Drawing.Size(131, 46);
            this.btn45_2.TabIndex = 3;
            this.btn45_2.Text = "構造体の規定値(45-2)";
            this.btn45_2.UseVisualStyleBackColor = true;
            // 
            // btn45_1
            // 
            this.btn45_1.Location = new System.Drawing.Point(13, 22);
            this.btn45_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn45_1.Name = "btn45_1";
            this.btn45_1.Size = new System.Drawing.Size(131, 46);
            this.btn45_1.TabIndex = 3;
            this.btn45_1.Text = "構造体の制限(45-1)";
            this.btn45_1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn45_6);
            this.groupBox1.Controls.Add(this.btn45_5);
            this.groupBox1.Controls.Add(this.btn45_4);
            this.groupBox1.Controls.Add(this.btn45_3);
            this.groupBox1.Controls.Add(this.btn45_2);
            this.groupBox1.Controls.Add(this.btn45_1);
            this.groupBox1.Location = new System.Drawing.Point(19, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(508, 142);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "構造体";
            // 
            // btn45_6
            // 
            this.btn45_6.Location = new System.Drawing.Point(353, 79);
            this.btn45_6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn45_6.Name = "btn45_6";
            this.btn45_6.Size = new System.Drawing.Size(145, 46);
            this.btn45_6.TabIndex = 9;
            this.btn45_6.Text = "メンバー毎コピー、メンバー毎比較(45-6)";
            this.btn45_6.UseVisualStyleBackColor = true;
            // 
            // btn45_5
            // 
            this.btn45_5.Location = new System.Drawing.Point(192, 79);
            this.btn45_5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn45_5.Name = "btn45_5";
            this.btn45_5.Size = new System.Drawing.Size(131, 46);
            this.btn45_5.TabIndex = 4;
            this.btn45_5.Text = "自動プロパティの扱い変更(45-5)";
            this.btn45_5.UseVisualStyleBackColor = true;
            // 
            // btn45_4
            // 
            this.btn45_4.Location = new System.Drawing.Point(13, 79);
            this.btn45_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn45_4.Name = "btn45_4";
            this.btn45_4.Size = new System.Drawing.Size(131, 46);
            this.btn45_4.TabIndex = 5;
            this.btn45_4.Text = "確実な初期化(45-4)";
            this.btn45_4.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(10, 76);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1107, 591);
            this.tabControl1.TabIndex = 11;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(1099, 562);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "45-51";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btn51_3);
            this.groupBox7.Controls.Add(this.btn51_2);
            this.groupBox7.Controls.Add(this.btn51_1);
            this.groupBox7.Location = new System.Drawing.Point(571, 128);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Size = new System.Drawing.Size(508, 86);
            this.groupBox7.TabIndex = 12;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "リソースの破棄";
            // 
            // btn51_3
            // 
            this.btn51_3.Location = new System.Drawing.Point(367, 22);
            this.btn51_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn51_3.Name = "btn51_3";
            this.btn51_3.Size = new System.Drawing.Size(131, 46);
            this.btn51_3.TabIndex = 3;
            this.btn51_3.Text = "式だけの using ステートメント(51-3)";
            this.btn51_3.UseVisualStyleBackColor = true;
            // 
            // btn51_2
            // 
            this.btn51_2.Location = new System.Drawing.Point(192, 22);
            this.btn51_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn51_2.Name = "btn51_2";
            this.btn51_2.Size = new System.Drawing.Size(131, 46);
            this.btn51_2.TabIndex = 3;
            this.btn51_2.Text = "using ステートメント(51-2)";
            this.btn51_2.UseVisualStyleBackColor = true;
            // 
            // btn51_1
            // 
            this.btn51_1.Location = new System.Drawing.Point(13, 22);
            this.btn51_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn51_1.Name = "btn51_1";
            this.btn51_1.Size = new System.Drawing.Size(131, 46);
            this.btn51_1.TabIndex = 3;
            this.btn51_1.Text = "リソース破棄の例(51-1)";
            this.btn51_1.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.btn50_3);
            this.groupBox6.Controls.Add(this.btn50_2);
            this.groupBox6.Controls.Add(this.btn50_1);
            this.groupBox6.Location = new System.Drawing.Point(571, 15);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(508, 86);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Nullable型";
            // 
            // btn50_3
            // 
            this.btn50_3.Location = new System.Drawing.Point(367, 22);
            this.btn50_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn50_3.Name = "btn50_3";
            this.btn50_3.Size = new System.Drawing.Size(131, 46);
            this.btn50_3.TabIndex = 3;
            this.btn50_3.Text = "?? 演算(50-3)";
            this.btn50_3.UseVisualStyleBackColor = true;
            // 
            // btn50_2
            // 
            this.btn50_2.Location = new System.Drawing.Point(192, 22);
            this.btn50_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn50_2.Name = "btn50_2";
            this.btn50_2.Size = new System.Drawing.Size(131, 46);
            this.btn50_2.TabIndex = 3;
            this.btn50_2.Text = "Nullable 型のメンバー(50-2)";
            this.btn50_2.UseVisualStyleBackColor = true;
            // 
            // btn50_1
            // 
            this.btn50_1.Location = new System.Drawing.Point(13, 22);
            this.btn50_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn50_1.Name = "btn50_1";
            this.btn50_1.Size = new System.Drawing.Size(131, 46);
            this.btn50_1.TabIndex = 3;
            this.btn50_1.Text = "Nullable 型(50-1)";
            this.btn50_1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btn49_3);
            this.groupBox5.Controls.Add(this.btn49_2);
            this.groupBox5.Controls.Add(this.btn49_1);
            this.groupBox5.Location = new System.Drawing.Point(19, 479);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.groupBox5.Size = new System.Drawing.Size(508, 79);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "ボックス型";
            // 
            // btn49_3
            // 
            this.btn49_3.Location = new System.Drawing.Point(367, 22);
            this.btn49_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn49_3.Name = "btn49_3";
            this.btn49_3.Size = new System.Drawing.Size(131, 46);
            this.btn49_3.TabIndex = 3;
            this.btn49_3.Text = "ボックス化を避ける(49-3)";
            this.btn49_3.UseVisualStyleBackColor = true;
            // 
            // btn49_2
            // 
            this.btn49_2.Location = new System.Drawing.Point(192, 22);
            this.btn49_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn49_2.Name = "btn49_2";
            this.btn49_2.Size = new System.Drawing.Size(131, 46);
            this.btn49_2.TabIndex = 3;
            this.btn49_2.Text = "ボックス化(49-2)";
            this.btn49_2.UseVisualStyleBackColor = true;
            // 
            // btn49_1
            // 
            this.btn49_1.Location = new System.Drawing.Point(13, 22);
            this.btn49_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn49_1.Name = "btn49_1";
            this.btn49_1.Size = new System.Drawing.Size(131, 46);
            this.btn49_1.TabIndex = 3;
            this.btn49_1.Text = "値型も object(49-1)";
            this.btn49_1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn48_3);
            this.groupBox4.Controls.Add(this.btn48_2);
            this.groupBox4.Controls.Add(this.btn48_1);
            this.groupBox4.Location = new System.Drawing.Point(19, 385);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(508, 79);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "引数の参照渡し";
            // 
            // btn48_3
            // 
            this.btn48_3.Location = new System.Drawing.Point(367, 22);
            this.btn48_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn48_3.Name = "btn48_3";
            this.btn48_3.Size = new System.Drawing.Size(131, 46);
            this.btn48_3.TabIndex = 3;
            this.btn48_3.Text = "出力引数(48-3)";
            this.btn48_3.UseVisualStyleBackColor = true;
            // 
            // btn48_2
            // 
            this.btn48_2.Location = new System.Drawing.Point(192, 22);
            this.btn48_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn48_2.Name = "btn48_2";
            this.btn48_2.Size = new System.Drawing.Size(131, 46);
            this.btn48_2.TabIndex = 3;
            this.btn48_2.Text = "参照渡し(48-2)";
            this.btn48_2.UseVisualStyleBackColor = true;
            // 
            // btn48_1
            // 
            this.btn48_1.Location = new System.Drawing.Point(13, 22);
            this.btn48_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn48_1.Name = "btn48_1";
            this.btn48_1.Size = new System.Drawing.Size(131, 46);
            this.btn48_1.TabIndex = 3;
            this.btn48_1.Text = "値渡し(48-1)";
            this.btn48_1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn47_3);
            this.groupBox3.Controls.Add(this.btn47_2);
            this.groupBox3.Controls.Add(this.btn47_1);
            this.groupBox3.Location = new System.Drawing.Point(19, 283);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(508, 79);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "規定値";
            // 
            // btn47_3
            // 
            this.btn47_3.Location = new System.Drawing.Point(347, 22);
            this.btn47_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn47_3.Name = "btn47_3";
            this.btn47_3.Size = new System.Drawing.Size(151, 46);
            this.btn47_3.TabIndex = 3;
            this.btn47_3.Text = "default(T) と構造体のコンストラクター(47-3)";
            this.btn47_3.UseVisualStyleBackColor = true;
            // 
            // btn47_2
            // 
            this.btn47_2.Location = new System.Drawing.Point(192, 22);
            this.btn47_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn47_2.Name = "btn47_2";
            this.btn47_2.Size = new System.Drawing.Size(131, 46);
            this.btn47_2.TabIndex = 3;
            this.btn47_2.Text = "default(T)(47-2)";
            this.btn47_2.UseVisualStyleBackColor = true;
            // 
            // btn47_1
            // 
            this.btn47_1.Location = new System.Drawing.Point(13, 22);
            this.btn47_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn47_1.Name = "btn47_1";
            this.btn47_1.Size = new System.Drawing.Size(131, 46);
            this.btn47_1.TabIndex = 3;
            this.btn47_1.Text = "規定値(47-1)";
            this.btn47_1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn46_3);
            this.groupBox2.Controls.Add(this.btn46_2);
            this.groupBox2.Controls.Add(this.btn46_1);
            this.groupBox2.Location = new System.Drawing.Point(19, 178);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(508, 86);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "値型と参照型";
            // 
            // btn46_3
            // 
            this.btn46_3.Location = new System.Drawing.Point(367, 22);
            this.btn46_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn46_3.Name = "btn46_3";
            this.btn46_3.Size = new System.Drawing.Size(131, 46);
            this.btn46_3.TabIndex = 3;
            this.btn46_3.Text = "値型と参照型の利点(46-3)";
            this.btn46_3.UseVisualStyleBackColor = true;
            // 
            // btn46_2
            // 
            this.btn46_2.Location = new System.Drawing.Point(192, 22);
            this.btn46_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn46_2.Name = "btn46_2";
            this.btn46_2.Size = new System.Drawing.Size(131, 46);
            this.btn46_2.TabIndex = 3;
            this.btn46_2.Text = "値型と参照型の違い(46-2)";
            this.btn46_2.UseVisualStyleBackColor = true;
            // 
            // btn46_1
            // 
            this.btn46_1.Location = new System.Drawing.Point(13, 22);
            this.btn46_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn46_1.Name = "btn46_1";
            this.btn46_1.Size = new System.Drawing.Size(131, 46);
            this.btn46_1.TabIndex = 3;
            this.btn46_1.Text = "おさらい: C# の型の分類(46-1)";
            this.btn46_1.UseVisualStyleBackColor = true;
            // 
            // FrmMemory_Resource
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 671);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmMemory_Resource";
            this.Text = "FrmMemory_Resource";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn45_3;
        private System.Windows.Forms.Button btn45_2;
        private System.Windows.Forms.Button btn45_1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn45_6;
        private System.Windows.Forms.Button btn45_5;
        private System.Windows.Forms.Button btn45_4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn46_3;
        private System.Windows.Forms.Button btn46_2;
        private System.Windows.Forms.Button btn46_1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn47_3;
        private System.Windows.Forms.Button btn47_2;
        private System.Windows.Forms.Button btn47_1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btn48_3;
        private System.Windows.Forms.Button btn48_2;
        private System.Windows.Forms.Button btn48_1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btn49_3;
        private System.Windows.Forms.Button btn49_2;
        private System.Windows.Forms.Button btn49_1;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button btn50_3;
        private System.Windows.Forms.Button btn50_2;
        private System.Windows.Forms.Button btn50_1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btn51_3;
        private System.Windows.Forms.Button btn51_2;
        private System.Windows.Forms.Button btn51_1;
    }
}