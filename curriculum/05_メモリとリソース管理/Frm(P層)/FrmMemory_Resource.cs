﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using curriculum.FormMessage.Frm_P層_.表示用Form;

namespace curriculum1
{
    /// ********************************************************************************
    /// <summary>
    /// メモリとリソース管理フォーム
    /// </summary>
    /// <remarks>none</remarks>
    /// 
    /// <history>
    /// --------------------------------------------------------------------------------
    /// No     Date         User         Remorks
    /// --------------------------------------------------------------------------------
    /// 0001   2015.12.05   宮崎祥彰   *new*
    /// 
    /// </history>
    /// ********************************************************************************
    public partial class FrmMemory_Resource : Base_Form
    {

        #region ++++++++++ member variable ++++++++++
      
        #endregion

        #region++++++++++ constructor/destructor ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰     *new*
        /// 
        /// </history>
        /// ********************************************************************************
        public FrmMemory_Resource()
        {
            // -------------------------------------------------------
            // 初期化処理
            InitializeComponent();
            this.EntryEvents();
        }

        #endregion

        #region ++++++++++ event ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// イベント Form　Load時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void FormMain_Load(object sender, EventArgs e)
        {

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = !this.MaximizeBox;

        }

        /// ********************************************************************************
        /// <summary>
        /// イベントハンドラ登録
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void EntryEvents()
        {
            this.Load += new EventHandler(this.FormMain_Load);
            this.btn45_1.Click += new EventHandler(this.btn45_1_Click);
            this.btn45_2.Click += new EventHandler(this.btn45_2_Click);
            this.btn45_3.Click += new EventHandler(this.btn45_3_Click);
            this.btn45_4.Click += new EventHandler(this.btn45_4_Click);
            this.btn45_5.Click += new EventHandler(this.btn45_5_Click);
            this.btn45_6.Click += new EventHandler(this.btn45_6_Click);
            this.btn46_1.Click += new EventHandler(this.btn46_1_Click);
            this.btn46_2.Click += new EventHandler(this.btn46_2_Click);
            this.btn46_3.Click += new EventHandler(this.btn46_3_Click);
            this.btn47_1.Click += new EventHandler(this.btn47_1_Click);
            this.btn47_2.Click += new EventHandler(this.btn47_2_Click);
            this.btn47_3.Click += new EventHandler(this.btn47_3_Click);
            this.btn48_1.Click += new EventHandler(this.btn48_1_Click);
            this.btn48_2.Click += new EventHandler(this.btn48_2_Click);
            this.btn48_3.Click += new EventHandler(this.btn48_3_Click);
            this.btn49_1.Click += new EventHandler(this.btn49_1_Click);
            this.btn49_2.Click += new EventHandler(this.btn49_2_Click);
            this.btn49_3.Click += new EventHandler(this.btn49_3_Click);
            this.btn50_1.Click += new EventHandler(this.btn50_1_Click);
            this.btn50_2.Click += new EventHandler(this.btn50_2_Click);
            this.btn50_3.Click += new EventHandler(this.btn50_3_Click);
            this.btn51_1.Click += new EventHandler(this.btn51_1_Click);
            this.btn51_2.Click += new EventHandler(this.btn51_2_Click);
            this.btn51_3.Click += new EventHandler(this.btn51_3_Click);
            
        }

        #region++++++++++ 構造体(45-1～45_6) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 構造体の制限
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn45_1_Click(object sender, EventArgs e)
        {
            SSampleStruct45_1 sSampleStruct = new SSampleStruct45_1(0, 1);
            MessageBox.Show(String.Format("構造体Aの値 = {0}、構造体Bの値 = {1}", sSampleStruct.A, sSampleStruct.B));
            CSampleClass45_1 cSampleClass = new CSampleClass45_1(0, 1);
            MessageBox.Show(String.Format("クラスAの値 = {0}、クラスBの値 = {1}", cSampleClass.A, cSampleClass.B));
        }

        //構造体
        public struct SSampleStruct45_1 : InterfaceA, InterfaceB
        {
            public int A { get; set;}
            public int B { get; set;}

            //コンストラクタ
            public SSampleStruct45_1(int a, int b) : this() { A = a; B = b; }

            public static SSampleStruct45_1 operator -(SSampleStruct45_1 x)
            {
                return new SSampleStruct45_1(-x.A, -x.B);
            }
        }

        public interface InterfaceA { int A { get; } }
        public interface InterfaceB { int B { get; } }

        //クラス
        public sealed class CSampleClass45_1 :InterfaceA, InterfaceB
        {
            public int A { get; set; }
            public int B { get; set; }

            //コンストラクタ
            public CSampleClass45_1() { }
            public CSampleClass45_1(int a, int b) { A = a; B = b; }

        }

        public static class CStaticClass45_1
        {
            public static string Hex(int x)
            {
               return x.ToString("X");
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 構造体の規定値
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn45_2_Click(object sender, EventArgs e)
        {
            //これも値型の性質になりますが、 クラス(newするまでメモリ領域を確保しない)と違って、 
            //構造体は宣言した時点でデータを記録するためのメモリ領域が確保されます。
            //クラス型のフィールドの場合は、newするなり他のインスタンスを代入するなりして
            //初期化するまでの間、 null (何のインスタンスも指していない状態)が入ります。
            //構造体の場合、いわゆる「0初期化」状態になっています。 
            //全てのメンバーに対して、0、もしくはそれに類する以下のような値が入ります。
            //数値型(int, doubleなど)の場合は0
            //列挙型も、0 に相当する値
            //bool 型の場合は false
            //参照型(string、配列、クラス、デリゲートなど)やNull許容型の場合は null
            //これら、0初期化状態にある値を、構造体の規定値(default value)と呼びます。
            SSample45_2 s = new SSample45_2(); ;
            MessageBox.Show(Convert.ToString(s.I));
            MessageBox.Show(Convert.ToString(s.D));
            MessageBox.Show(Convert.ToString(s.B));
            MessageBox.Show(s.S);
            
        }

        struct SSample45_2
        {
            public int I;
            public double D;
            public bool B;
            public string S;
        }

        

        ///// ********************************************************************************
        ///// <summary>
        ///// 確実な初期化
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn45_3_Click(object sender, EventArgs e)
        {
            var p1 = new SPoint45_3(); // 規定値、つまり、「XもYも0に初期化」という意味で使われる
            var p2 = new SPoint45_3(10, 20);
            var p3 = default(SPoint45_3); // C# 2.0 以降。p1と同じ意味

            MessageBox.Show(Convert.ToString(p1));
            MessageBox.Show(Convert.ToString(p2));
            MessageBox.Show(Convert.ToString(p3));
        }

        struct SPoint45_3
        {
            public int X { get; set; }
            public int Y { get; set;}
            public SPoint45_3(int x, int y) : this() { X = x; Y = y; }
            public override string ToString() 
            {
               return "({"+ X +"},{"+ Y +"})";
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 確実な初期化
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn45_4_Click(object sender, EventArgs e)
        {
            //new T() やdefault(T)で作る「規定値」とは違って、 
            //引数付きのコンストラクターを使う場合は、コンストラクター内で全てのメンバーを
            //きっちり自分の手で初期化する必要があります。
            var p1 = new SSample45_4(10, 20);
            MessageBox.Show(Convert.ToString(p1));
        }

        struct SSample45_4
        {
            int _x;
            int _y;

            public SSample45_4(int x, int y)
            {
                //M(); // エラー: _x, _y の初期化より前に呼んじゃダメ。
                _x = x;
                _y = y;
                M(); // この順ならOK。
            }
            public override string ToString()
            {
                return "({" + _x + "},{" + _y + "})";
            }
            void M() { }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 自動プロパティの扱い変更
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn45_5_Click(object sender, EventArgs e)
        {
            //本項はC#6.0を使用する為実施不可。(VidualStudio2012ではC#5.0まで)
            MessageBox.Show("実施不可");
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// メンバー毎コピー、メンバー毎比較
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn45_6_Click(object sender, EventArgs e)
        {
            var x = new SPoint45_6(1, 2);
            var y = x;

            MessageBox.Show((Convert.ToString(y))); // x のメンバー毎コピー = (1, 2)

            // メンバー毎比較(全メンバー一致なら一致)
            MessageBox.Show(Convert.ToString(x.Equals(new SPoint45_6(1, 2)))); // true
            MessageBox.Show(Convert.ToString(x.Equals(new SPoint45_6(1, 3)))); // false
        }

        public struct SPoint45_6
        {
            public int X { get; set; }
            public int Y { get; set; }

            public SPoint45_6(int x, int y) :this() { X = x; Y = y; }
            public override string ToString() 
            {
                return "({" + X + "},{" + Y + "})";
            }
        }

        #endregion

        #region++++++++++ 値型と参照型(46-1～46_4) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// おさらい: C# の型の分類
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn46_1_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("　　　　組み込み型　　ユーザー定義型　　他の型から合成\n値型");
            sb.Append("　　　単純型　　　　　構造体　　　　　Null許容型\n　　　　　");
            sb.Append("列挙型\n参照型　　文字列型　　　　クラス　　　　　配列\n");
            sb.Append("　　　　　オブジェクト型　インターフェース\n　　　　　デリゲート");
            MessageBox.Show(sb.ToString());
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 値型と参照型の違い
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn46_2_Click(object sender, EventArgs e)
        {
            //構造体（struct キーワードを使って定義した型）は値型に 
            
            MessageBox.Show("値型の場合");
            SPoint46_2 sa = new SPoint46_2(12, 5);
            SPoint46_2 sb = sa;
            SPoint46_2 sc = sa;
            MessageBox.Show(String.Format("sa: {0}\nsb: {1}\nsc: {2}\n", sa, sb, sc));
            sb.x = 0;
            Console.Write("sa: {0}\nb: {1}\nc: {2}\n", sa, sb, sc);
            //クラス（class キーワードを使って定義した型）は参照型になります。
            MessageBox.Show("参照型の場合");
            CPoint46_2 ca = new CPoint46_2(12, 5);
            CPoint46_2 cb = ca;
            CPoint46_2 cc = ca;
            MessageBox.Show(String.Format("ca: {0}\ncb: {1}\ncc: {2}\n", ca, cb, cc));
            cb.x = 0;
            MessageBox.Show(String.Format("ca: {0}\ncb: {1}\ncc: {2}\n", ca, cb, cc));
        }
        // 値型(構造体は値型になる)
        struct SPoint46_2
        {
            public int x, y;

            public SPoint46_2(int x, int y) { this.x = x; this.y = y; }
            public override string ToString()
            {
                return "(" + this.x.ToString() + ", " + this.y.ToString() + ")";
            }
        }
        // 参照型(クラスは参照型になる)
        class CPoint46_2
        {
            public int x, y;

            public CPoint46_2(int x, int y) { this.x = x; this.y = y; }
            public override string ToString()
            {
                return "(" + this.x.ToString() + ", " + this.y.ToString() + ")";
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 値型と参照型の利点
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn46_3_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("　　　　値型　　　　　　　　　　　　　　　参照型\n");
            sb.Append("代入時　・値の複製が生じる　　　　　　　　・値を複製しない\n");
            sb.Append("利点　　・間接参照が生じないので、　　　　・複製が生じないので、\n");
            sb.Append("　　　　　メンバーアクセスが高速　　　　　・変数への代入・引数渡しが高速\n");
            sb.Append("欠点　　・型のサイズが大きいとき、               間接参照が生じて、メンバー\n");
            sb.Append("         　  複製のコストが大きい　　                アクセス時に少しコストがかかる\n");
            sb.Append("         　  継承・多態的ふるまいができない\n");
            sb.Append("\n※このような特徴があるため、通常は データのサイズが小さく、継承の必要のないものは構造体として定義し、 それ以外のものはクラスとして定義します");
            MessageBox.Show(sb.ToString());
        }

        #endregion

        #region++++++++++ 規定値(47-1～47_4) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 規定値
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn47_1_Click(object sender, EventArgs e)
        {
            //ローカル変数 → 明示的に初期化しないとコンパイル エラー
            //フィールド →規定値で初期化される
            //初期化子なしの配列 →要素が全部規定値で初期化される
            // 初期化せずにフィールドを読んでみる(規定値が入っている)
            var a = new CDefaultValues47_1();
            MessageBox.Show(a.i.ToString());
            MessageBox.Show(a.x.ToString());
            MessageBox.Show(Convert.ToString((int)a.c)); // '\0' (ヌル文字)は表示できないので数値化して表示
            MessageBox.Show(Convert.ToString(a.b)); // False
            MessageBox.Show(Convert.ToString(a.s)); //Null になる

        }

        class CDefaultValues47_1
        {
            public int i;
            public double x;
            public char c;
            public bool b;
            public string s;
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// default(T)
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn47_2_Click(object sender, EventArgs e)
        {
            // 参照型の場合
            string s = getDefaultValue<string>();
            MessageBox.Show(s); // 出力：（何もなし）

            // 値型の場合
            int i = getDefaultValue<int>();
            MessageBox.Show(i.ToString()); // 出力：0

            // 構造体の場合
            DateTime dt = getDefaultValue<DateTime>();
            MessageBox.Show(Convert.ToString(dt)); // 出力：0001/01/01 0:00:00
        }

        public T47_2 getDefaultValue<T47_2>()
        {

            T47_2 myVar = default(T47_2);
            return myVar;// 型に応じて、null とか 0 とかになる
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// default(T) と構造体のコンストラクター
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn47_3_Click(object sender, EventArgs e)
        {
            // 値型の場合
            int i = getDefaultValue47_3<int>();
            MessageBox.Show(i.ToString()); // 出力：0

            // 構造体の場合
            DateTime dt = getDefaultValue47_3<DateTime>();
            MessageBox.Show(Convert.ToString(dt)); // 出力：0001/01/01 0:00:00
        }

        public T47_3 getDefaultValue47_3<T47_3>() where T47_3 : new()
        {
            var x = new T47_3();    // この場合はコンストラクターが呼ばれて欲しい
            var y = default(T47_3); // こいつは規定値(0 埋め)
            return x;
        }

        #endregion

        #region++++++++++ 引数の参照渡し(48-1～48_3) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 値渡し
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn48_1_Click(object sender, EventArgs e)
        {
            int a = 100;
            MessageBox.Show(String.Format("{0} → ", a));
            Test48_1(a);
            Console.Write(String.Format("{0}\n", a));
        }

        static void Test48_1(int a)
        {
            a = 10; // メソッド内で値を書き換える。
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 参照渡し
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn48_2_Click(object sender, EventArgs e)
        {
            int a = 100;
            MessageBox.Show(String.Format("{0} → ", a));
            Test48_2(ref a);
            MessageBox.Show(String.Format("{0}\n", a));
        }

        static void Test48_2(ref int a)
        {
            a = 10; // メソッド内で値を書き換える。
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// 出力引数
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn48_3_Click(object sender, EventArgs e)
        {
            int a;
            Test48_3(out a); // out を使った場合、変数を初期化しなくてもいい
            MessageBox.Show(String.Format("{0}\n", a));
        }

        static void Test48_3(out int a)
        {
            a = 10; // out を使った場合、メソッド内で必ず値を代入しなければならない
        }

        #endregion

        #region++++++++++ ボックス型(49-1～49_3) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 値型も object
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn49_1_Click(object sender, EventArgs e)
        {
            Write(5);
            Write("aaa");
        }

        static void Write(object x)
        {
            MessageBox.Show(x.GetType().Name + " " + x.ToString());
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// ボックス化
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn49_2_Click(object sender, EventArgs e)
        {
            int x = 5;
            object y = x;   // int を object に。ボックス化が起きる。
            int z = (int)y; // object から元の型に。ボックス化解除。
            MessageBox.Show(z.ToString());
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// ボックス化を避ける
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn49_3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Convert.ToString(CompareTo((IComparable)5, 6)));
            MessageBox.Show(Convert.ToString(CompareTo((IComparable<int>)5, 6)));
        }

        static int CompareTo(IComparable x, int value)
        {
            // IComparable.CompareTo(object) が呼ばれる。
            // value がボックス化される
            return x.CompareTo(value);
        }

        static int CompareTo(IComparable<int> x, int value)
        {
            // IComparable<int>.CompareTo(int) が呼ばれる。
            // value は int のまま渡される
            return x.CompareTo(value);
        }

        #endregion

        #region++++++++++ Nullable型(50-1～50_3) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// Nullable 型
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn50_1_Click(object sender, EventArgs e)
        {
            //Nullable 型は、値型の型名の後ろに ? を付ける事で、
            //元の型の値または null の値を取れる型になるというものです。 int 型で例に取ると、以下のような書き方が出来ます。
            int? x = 123;
            int? y = null;
            MessageBox.Show(String.Format("x = {0} , y = {1}",x,y));
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// Nullable 型のメンバー
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn50_2_Click(object sender, EventArgs e)
        {
            int? x;
            x = 123;
            x = new int?(123); // x = 123; と等価。
            MessageBox.Show(Convert.ToString(x));
            int? x2 = 123;
            int? y2 = null;
            int z;

            z = (int)x2; // OK。
            try{
                //T? → T の変換は、HasValue が true のときのみ可能で、 
                //HasValue が false の時には InvalidOperationException がスローされます。
                z = (int)y2; // 例外が発生。
               }
            catch (InvalidOperationException)
               {
                    MessageBox.Show("例外発生");
               }

        }

        ///// ********************************************************************************
        ///// <summary>
        ///// ?? 演算
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn50_3_Click(object sender, EventArgs e)
        {
            // x, y は int? 型の変数
            int? x = 123;
            int? y = null;
            int? z = x ?? y; // x != null ? x : y
            int i = z ?? -1; // z != null ? z.Value : -1
            MessageBox.Show(Convert.ToString(i));
        }

        #endregion

        #region++++++++++ リソースの破棄(51-1～51_3) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// リソース破棄の例
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn51_1_Click(object sender, EventArgs e)
        {
            string[] array = new string[3];
            array[0] = "51_1.txt";

            Getbyte(array);
        }

        private void Getbyte(string[] args)
        {
            FileStream reader = new FileStream(args[0], FileMode.Open);

            try
            {
                // 先頭のNバイトを読み出して画面に表示
                const int N = 10;
                byte[] buf = new byte[N];
                reader.Read(buf, 0, N);
                for (int i = 0; i < N; ++i)
                {
                    MessageBox.Show(String.Format("{0,4}", (int)buf[i]));
                }
            }
            catch (Exception)
            {
                // 例外処理を行う
            }
            finally
            {
                // 例外が発生しようがしまいが finally ブロックは必ず実行される。
                // リソースの破棄は finally ブロックで行う。
                if (reader != null)
                    reader.Close();
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// using ステートメント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn51_2_Click(object sender, EventArgs e)
        {
            string[] array = new string[3];
            array[0] = "51_1.txt";

            Getbyte51_2(array);
        }

        private void Getbyte51_2(string[] args)
        {
            using (FileStream reader = new FileStream(args[0], FileMode.Open))
            {
                // 先頭のNバイトを読み出して画面に表示

                const int N = 10;
                byte[] buf = new byte[N];
                reader.Read(buf, 0, N);
                for (int i = 0; i < N; ++i)
                {
                    MessageBox.Show(String.Format("{0,4}", (int)buf[i]));
                }
            }
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// 式だけの using ステートメント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn51_3_Click(object sender, EventArgs e)
        {
           object o51_3 = new object();
           o51_3 = "51_3テスト";
           GenericMethod<object>(o51_3);
           //MessageBox.Show(Convert.ToString());

        }

        static void GenericMethod<T>(T obj)
        {
            using (obj as IDisposable)
            {
               obj = default(T);
               MessageBox.Show(Convert.ToString(obj));
            }
        }

        #endregion

        #endregion

    }

    }
