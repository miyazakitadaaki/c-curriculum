﻿namespace curriculum1
{
    partial class FrmData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn54_1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn53_1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn52_4 = new System.Windows.Forms.Button();
            this.btn52_3 = new System.Windows.Forms.Button();
            this.btn52_2 = new System.Windows.Forms.Button();
            this.btn52_1 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn55_3 = new System.Windows.Forms.Button();
            this.btn55_2 = new System.Windows.Forms.Button();
            this.btn55_1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-1, 3);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1134, 66);
            this.panel1.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(100, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(387, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "データ処理(52-1_55-5)";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(10, 76);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1107, 591);
            this.tabControl1.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(1099, 562);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "52-55";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn54_1);
            this.groupBox3.Location = new System.Drawing.Point(19, 286);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(508, 86);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "LINQ";
            // 
            // btn54_1
            // 
            this.btn54_1.Location = new System.Drawing.Point(13, 22);
            this.btn54_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn54_1.Name = "btn54_1";
            this.btn54_1.Size = new System.Drawing.Size(131, 46);
            this.btn54_1.TabIndex = 3;
            this.btn54_1.Text = "LINQ の例(54_1)";
            this.btn54_1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn53_1);
            this.groupBox2.Location = new System.Drawing.Point(19, 177);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(508, 86);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "データ処理";
            // 
            // btn53_1
            // 
            this.btn53_1.Location = new System.Drawing.Point(13, 22);
            this.btn53_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn53_1.Name = "btn53_1";
            this.btn53_1.Size = new System.Drawing.Size(176, 46);
            this.btn53_1.TabIndex = 3;
            this.btn53_1.Text = "C# でデータ処理を書く上でのポイント(53-1)";
            this.btn53_1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn52_4);
            this.groupBox1.Controls.Add(this.btn52_3);
            this.groupBox1.Controls.Add(this.btn52_2);
            this.groupBox1.Controls.Add(this.btn52_1);
            this.groupBox1.Location = new System.Drawing.Point(19, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(508, 142);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "foreach";
            // 
            // btn52_4
            // 
            this.btn52_4.Location = new System.Drawing.Point(13, 79);
            this.btn52_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn52_4.Name = "btn52_4";
            this.btn52_4.Size = new System.Drawing.Size(131, 46);
            this.btn52_4.TabIndex = 5;
            this.btn52_4.Text = "コレクションクラスの自作(52-4)";
            this.btn52_4.UseVisualStyleBackColor = true;
            // 
            // btn52_3
            // 
            this.btn52_3.Location = new System.Drawing.Point(367, 22);
            this.btn52_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn52_3.Name = "btn52_3";
            this.btn52_3.Size = new System.Drawing.Size(131, 46);
            this.btn52_3.TabIndex = 3;
            this.btn52_3.Text = "foreach文とは(52-3)";
            this.btn52_3.UseVisualStyleBackColor = true;
            // 
            // btn52_2
            // 
            this.btn52_2.Location = new System.Drawing.Point(192, 22);
            this.btn52_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn52_2.Name = "btn52_2";
            this.btn52_2.Size = new System.Drawing.Size(131, 46);
            this.btn52_2.TabIndex = 3;
            this.btn52_2.Text = "IEnumerable インターフェース(52-2)";
            this.btn52_2.UseVisualStyleBackColor = true;
            // 
            // btn52_1
            // 
            this.btn52_1.Location = new System.Drawing.Point(13, 22);
            this.btn52_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn52_1.Name = "btn52_1";
            this.btn52_1.Size = new System.Drawing.Size(131, 46);
            this.btn52_1.TabIndex = 3;
            this.btn52_1.Text = "コレクション(52-1)";
            this.btn52_1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn55_3);
            this.groupBox4.Controls.Add(this.btn55_2);
            this.groupBox4.Controls.Add(this.btn55_1);
            this.groupBox4.Location = new System.Drawing.Point(19, 395);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(508, 90);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "イテレーター";
            // 
            // btn55_3
            // 
            this.btn55_3.Location = new System.Drawing.Point(367, 22);
            this.btn55_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn55_3.Name = "btn55_3";
            this.btn55_3.Size = new System.Drawing.Size(131, 46);
            this.btn55_3.TabIndex = 3;
            this.btn55_3.Text = "GetEnumerator(55-3)";
            this.btn55_3.UseVisualStyleBackColor = true;
            // 
            // btn55_2
            // 
            this.btn55_2.Location = new System.Drawing.Point(192, 22);
            this.btn55_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn55_2.Name = "btn55_2";
            this.btn55_2.Size = new System.Drawing.Size(131, 46);
            this.btn55_2.TabIndex = 3;
            this.btn55_2.Text = "イテレーターの制限(55-2)";
            this.btn55_2.UseVisualStyleBackColor = true;
            // 
            // btn55_1
            // 
            this.btn55_1.Location = new System.Drawing.Point(13, 22);
            this.btn55_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn55_1.Name = "btn55_1";
            this.btn55_1.Size = new System.Drawing.Size(131, 46);
            this.btn55_1.TabIndex = 3;
            this.btn55_1.Text = "イテレーター ブロック(55-1)";
            this.btn55_1.UseVisualStyleBackColor = true;
            // 
            // FrmData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 671);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmData";
            this.Text = "FrmData";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn52_4;
        private System.Windows.Forms.Button btn52_3;
        private System.Windows.Forms.Button btn52_2;
        private System.Windows.Forms.Button btn52_1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn54_1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn53_1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btn55_3;
        private System.Windows.Forms.Button btn55_2;
        private System.Windows.Forms.Button btn55_1;
    }
}