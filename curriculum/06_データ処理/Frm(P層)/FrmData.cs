﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace curriculum1
{
    /// ********************************************************************************
    /// <summary>
    /// データ処理フォーム
    /// </summary>
    /// <remarks>none</remarks>
    /// 
    /// <history>
    /// --------------------------------------------------------------------------------
    /// No     Date         User         Remorks
    /// --------------------------------------------------------------------------------
    /// 0001   2015.12.05   宮崎祥彰   *new*
    /// 
    /// </history>
    /// ********************************************************************************
    public partial class FrmData : Base_Form
    {
        #region ++++++++++ member variable ++++++++++
        public enum TopicNo
        {
        }
        #endregion

        #region++++++++++ constructor/destructor ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰     *new*
        /// 
        /// </history>
        /// ********************************************************************************
        public FrmData()
        {
            // -------------------------------------------------------
            // 初期化処理
            InitializeComponent();
            this.EntryEvents();
        }

        #endregion

        #region ++++++++++ event ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// イベント Form　Load時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void FormMain_Load(object sender, EventArgs e)
        {

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = !this.MaximizeBox;

        }

        /// ********************************************************************************
        /// <summary>
        /// イベントハンドラ登録
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void EntryEvents()
        {

            this.Load += new EventHandler(this.FormMain_Load);
            this.btn52_1.Click += new EventHandler(this.btn52_1_Click);
            this.btn52_2.Click += new EventHandler(this.btn52_2_Click);
            this.btn52_3.Click += new EventHandler(this.btn52_3_Click);
            this.btn52_4.Click += new EventHandler(this.btn52_4_Click);
            this.btn53_1.Click += new EventHandler(this.btn53_1_Click);
            this.btn54_1.Click += new EventHandler(this.btn54_1_Click);
            this.btn55_1.Click += new EventHandler(this.btn55_1_Click);
            this.btn55_2.Click += new EventHandler(this.btn55_2_Click);
            this.btn55_3.Click += new EventHandler(this.btn55_3_Click);

        }

        #region++++++++++foreach(52-1～55_5) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// コレクション
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn52_1_Click(object sender, EventArgs e)
        {
          //本格的な実装は52_3で紹介  
          MessageBox.Show("コレクション(「コンテナ」ともいいます)とは配列やリスト、辞書などの複数の要素をひとつにまとめるクラスのことです。 複数の要素をまとめておく方法にはさまざまな方法があり、 その方法によって呼び名が変わります。");
        }

        

        ///// ********************************************************************************
        ///// <summary>
        ///// IEnumerable インターフェース
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn52_2_Click(object sender, EventArgs e)
        {
            int[] array = new int[] { 1, 3, 5, 7 };

            IEnumerator ie = array.GetEnumerator();
            while (ie.MoveNext())
            {
                int val = (int)ie.Current;
                MessageBox.Show(string.Format("{0}\n", val));
            }
        }

        

        ///// ********************************************************************************
        ///// <summary>
        ///// foreach文とは
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn52_3_Click(object sender, EventArgs e)
        {
            int[] array = new int[10] { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512 };

            foreach (int n in array)
            {
                MessageBox.Show(n + " ");
            }
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// コレクションクラスの自作
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn52_4_Click(object sender, EventArgs e)
        {
            LinearList list = new LinearList();

            for (int i = 0; i < 10; ++i)
            {
                list.Add(i * (i + 1) / 2);
            }

            foreach (int s in list)
            {
                MessageBox.Show(s + " ");
            }
        }

        /// <summary>
        /// 片方向連結リストクラス
        /// </summary>
        class LinearList : IEnumerable
        {
            /// <summary>
            /// 連結リストのセル
            /// </summary>
            private class Cell
            {
                public object value;
                public Cell next;

                public Cell(object value, Cell next)
                {
                    this.value = value;
                    this.next = next;
                }
            }

            /// <summary>
            /// LinearList の列挙子
            /// </summary>
            private class LinearListEnumerator : IEnumerator
            {
                private LinearList list;
                private Cell current;

                public LinearListEnumerator(LinearList list)
                {
                    this.list = list;
                    this.current = null;
                }

                /// <summary>
                /// コレクション内の現在の要素を取得
                /// </summary>
                public object Current
                {
                    get { return this.current.value; }
                }

                /// <summary>
                /// 列挙子をコレクションの次の要素に進める
                /// </summary>
                public bool MoveNext()
                {
                    if (this.current == null)
                        this.current = this.list.head;
                    else
                        this.current = this.current.next;

                    if (this.current == null)
                        return false;
                    return true;
                }

                /// <summary>
                /// 列挙子を初期位置に戻す
                /// </summary>
                public void Reset()
                {
                    this.current = null;
                }
            }

            private Cell head;

            public LinearList()
            {
                head = null;
            }

            /// <summary>
            /// リストに新しい要素を追加
            /// </summary>
            public void Add(object value)
            {
                head = new Cell(value, head);
            }

            /// <summary>
            /// 列挙子を取得
            /// </summary>
            public IEnumerator GetEnumerator()
            {
                return new LinearListEnumerator(this);
            }
        }

        #endregion

        #region++++++++++データ処理(53-1) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// C# でデータ処理を書く上でのポイント
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn53_1_Click(object sender, EventArgs e)
        {
            List<int> list = new List<int>();
            list.Add(7);
            list.Add(5);
            list.Add(3);
            list.Add(1);

            Func<int, int> func = AddFunc;

            MessageBox.Show(Convert.ToString(Select(list,func)));
        }

        public int AddFunc(int a)
        {
            return a * a;
        }

        static IEnumerable<int> Select(IEnumerable<int> source, Func<int, int> filter)
        {
            foreach (var x in source)
            {
                yield return filter(x);
            }
        }

        

        #endregion

        #region++++++++++LINQ(54-1) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// データベース言語,LINQ の例,LINQ の全体像,クエリ式
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn54_1_Click(object sender, EventArgs e)
        {
            var 学生名簿 =
            new[] {
              new {学生番号 = 14, 姓 = "風浦", 名 = "可符香"},
              new {学生番号 = 20, 姓 = "小森", 名 = "霧"    },
              new {学生番号 = 22, 姓 = "常月", 名 = "まとい"},
              new {学生番号 = 19, 姓 = "小節", 名 = "あびる"},
              new {学生番号 = 18, 姓 = "木村", 名 = "カエレ"},
              new {学生番号 = 16, 姓 = "音無", 名 = "芽留"  },
              new {学生番号 = 17, 姓 = "木津", 名 = "千里"  },
              new {学生番号 =  8, 姓 = "関内", 名 = "マリア"},
              new {学生番号 = 28, 姓 = "日塔", 名 = "奈美"  },
            };

            var 出席番号前半名 =
              from p in 学生名簿
              where p.学生番号 <= 15
              orderby p.学生番号
              select p.名;

            foreach (var 名 in 出席番号前半名)
            {
                MessageBox.Show(String.Format("{0}\n", 名));
            }
        }

        #endregion

        #region++++++++++イテレーター(55-1～55_5) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// イテレーター ブロック
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn55_1_Click(object sender, EventArgs e)
        {
            foreach (int i in FromTo(10, 20))
            {
                MessageBox.Show(string.Format("{0}\n", i));
            }
        }

        // ↓これがイテレーター ブロック。IEnubrable を実装するクラスを自動生成してくれる。
        static public IEnumerable<int> FromTo(int from, int to)
        {
            while (from <= to)
                yield return from++;
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// イテレーターの制限
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn55_2_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("unsafeにはできない\n");
            sb.Append("関数メンバーにunsafe修飾子は付けれない。\n");
            sb.Append("イテレーター ブロック内にunsafeステートメントは書けない。\n");
            sb.Append("引数をref, outにはできない。\n");
            sb.Append("また、以下の場所にはyield return、yield break共に書けません。\n\n");
            sb.Append("finally 句内\n");
            sb.Append("匿名関数の中\n");
            sb.Append("匿名なイテレーター ブロック自体作れません。\n");
            sb.Append("そして、以下の場所にはyield returnを書けません。\n");
            sb.Append("try 句内\n");
            sb.Append("catch 句内");

            MessageBox.Show(Convert.ToString(sb));
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// GetEnumerator
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn55_3_Click(object sender, EventArgs e)
        {
            LinearList<int> list = new LinearList<int>();

            for(int i=0; i<10; ++i)
            {
                list.Add(i * (i + 1) / 2);
            }

            foreach(int s in list)
            {
                MessageBox.Show(s + " ");
            }
            
        }

        /// <summary>
        /// 片方向連結リストクラス
        /// </summary>
        class LinearList<T>
        {
            /// <summary>
            /// 連結リストのセル
            /// </summary>
            private class Cell
            {
                public T value;
                public Cell next;

                public Cell(T value, Cell next)
                {
                    this.value = value;
                    this.next = next;
                }
            }

            private Cell head;

            public LinearList()
            {
                this.head = null;
            }

            /// <summary>
            /// リストに新しい要素を追加
            /// </summary>
            public void Add(T value)
            {
                this.head = new Cell(value, head);
            }

            /// <summary>
            /// 列挙子を取得
            /// </summary>
            public IEnumerator<T> GetEnumerator()
            {
                for (Cell c = this.head; c != null; c = c.next)
                {
                    yield return c.value;
                }
            }
        }

        #endregion

    }
        #endregion
}
