﻿namespace curriculum1
{
    partial class FrmAsync
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnAsync3 = new System.Windows.Forms.Button();
            this.btnAsync2 = new System.Windows.Forms.Button();
            this.btnAsync1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(541, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "■同時に押して実行してください 。　→　実行後Dドライブ直下にファイルが出来ていればOK";
            // 
            // btnAsync3
            // 
            this.btnAsync3.Location = new System.Drawing.Point(466, 55);
            this.btnAsync3.Name = "btnAsync3";
            this.btnAsync3.Size = new System.Drawing.Size(180, 50);
            this.btnAsync3.TabIndex = 0;
            this.btnAsync3.Text = "btnAsync3";
            this.btnAsync3.UseVisualStyleBackColor = true;
            // 
            // btnAsync2
            // 
            this.btnAsync2.Location = new System.Drawing.Point(247, 55);
            this.btnAsync2.Name = "btnAsync2";
            this.btnAsync2.Size = new System.Drawing.Size(180, 50);
            this.btnAsync2.TabIndex = 0;
            this.btnAsync2.Text = "btnAsync2";
            this.btnAsync2.UseVisualStyleBackColor = true;
            // 
            // btnAsync1
            // 
            this.btnAsync1.Location = new System.Drawing.Point(25, 55);
            this.btnAsync1.Name = "btnAsync1";
            this.btnAsync1.Size = new System.Drawing.Size(180, 50);
            this.btnAsync1.TabIndex = 0;
            this.btnAsync1.Text = "btnAsync1";
            this.btnAsync1.UseVisualStyleBackColor = true;
            // 
            // FrmAsync
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 133);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAsync3);
            this.Controls.Add(this.btnAsync2);
            this.Controls.Add(this.btnAsync1);
            this.Name = "FrmAsync";
            this.Text = "FrmAsync";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAsync1;
        private System.Windows.Forms.Button btnAsync2;
        private System.Windows.Forms.Button btnAsync3;
        private System.Windows.Forms.Label label1;
    }
}