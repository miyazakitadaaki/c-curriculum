﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace curriculum1
{
    /// ********************************************************************************
    /// <summary>
    /// 非同期メソッド用フォーム
    /// </summary>
    /// <remarks>none</remarks>
    /// 
    /// <history>
    /// --------------------------------------------------------------------------------
    /// No     Date         User         Remorks
    /// --------------------------------------------------------------------------------
    /// 0001   2015.12.05   宮崎祥彰   *new*
    /// 
    /// </history>
    /// ********************************************************************************
    public partial class FrmAsync : Base_Form
    {
        
        #region ++++++++++ member variable ++++++++++
        #endregion

        #region++++++++++ constructor/destructor ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰     *new*
        /// 
        /// </history>
        /// ********************************************************************************
        public FrmAsync()
        {
            // -------------------------------------------------------
            // 初期化処理
            InitializeComponent();
            this.EntryEvents();
        }

        #endregion

        #region ++++++++++ event ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// イベント Form　Load時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void FormMain_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = !this.MaximizeBox;

        }

        /// ********************************************************************************
        /// <summary>
        /// イベントハンドラ登録
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void EntryEvents()
        {

            this.Load += new EventHandler(this.FormMain_Load);
            this.btnAsync1.Click += new EventHandler(this.btnAsync_Click);
            this.btnAsync2.Click += new EventHandler(this.btnAsync_Click);
            this.btnAsync3.Click += new EventHandler(this.btnAsync_Click);
        }

        #endregion

        #region ++++++++++ method ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// イベントハンドラ登録
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private async void btnAsync_Click(object sender, EventArgs e_)
        {
            string sbtn = string.Empty;
            sbtn = Convert.ToString(sender);
            if(sbtn == "System.Windows.Forms.Button, Text: btnAsync1"){sbtn = "btnAsync1";}
            if(sbtn == "System.Windows.Forms.Button, Text: btnAsync2"){sbtn = "btnAsync2";}
            if(sbtn == "System.Windows.Forms.Button, Text: btnAsync3"){sbtn = "btnAsync3";}
            GetAsyncMethod(sbtn);
            
        }

        /// ********************************************************************************
        /// <summary>
        /// 非同期実行用メソッド
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private Task<bool> GetAsyncMethod(string sender){
            
            return Task.Run(() => {
            //初期化
            bool result = false;
            //txtファイル読み込み
            StreamReader sr = new StreamReader(sender + ".txt", Encoding.GetEncoding("Shift_JIS"));
            string resulttext = sr.ReadToEnd();
            sr.Close();
            //txtファイル出力
            Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
            if (sender == "btnAsync1") 
            {
                StreamWriter writer =
                new StreamWriter(@"D:\result1.txt", true, sjisEnc);
                for(int i =0 ; i < 100000 ; i++){
                writer.WriteLine(resulttext);
                }
                writer.Close();
             }
            if (sender == "btnAsync2") 
            {
                StreamWriter writer =
                new StreamWriter(@"D:\result2.txt", true, sjisEnc);
                for(int i =0 ; i < 100000 ; i++){
                writer.WriteLine(resulttext);
                }
                writer.Close();
            }
            if (sender == "btnAsync3") 
            {
                StreamWriter writer =
                new StreamWriter(@"D:\result3.txt", true, sjisEnc);
                for(int i =0 ; i < 100000 ; i++){
                writer.WriteLine(resulttext);
                }
                writer.Close();
            }
            

            return result;
            });
        }

        #endregion

    }
}
