﻿namespace curriculum1
{
    partial class FrmAsynchronous
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn56_1 = new System.Windows.Forms.Button();
            this.btn56_2 = new System.Windows.Forms.Button();
            this.btn56_3 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn56_5 = new System.Windows.Forms.Button();
            this.btn56_4 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn57_1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn58_1 = new System.Windows.Forms.Button();
            this.btn58_2 = new System.Windows.Forms.Button();
            this.btn58_3 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(100, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(413, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "非同期処理(56-1_58-3)";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1409, 66);
            this.panel1.TabIndex = 2;
            // 
            // btn56_1
            // 
            this.btn56_1.Location = new System.Drawing.Point(13, 22);
            this.btn56_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn56_1.Name = "btn56_1";
            this.btn56_1.Size = new System.Drawing.Size(131, 46);
            this.btn56_1.TabIndex = 3;
            this.btn56_1.Text = "マルチスレッドとは(56-1)";
            this.btn56_1.UseVisualStyleBackColor = true;
            // 
            // btn56_2
            // 
            this.btn56_2.Location = new System.Drawing.Point(175, 22);
            this.btn56_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn56_2.Name = "btn56_2";
            this.btn56_2.Size = new System.Drawing.Size(169, 46);
            this.btn56_2.TabIndex = 3;
            this.btn56_2.Text = "C# におけるマルチスレッドプログラミング(56-2)";
            this.btn56_2.UseVisualStyleBackColor = true;
            // 
            // btn56_3
            // 
            this.btn56_3.Location = new System.Drawing.Point(367, 22);
            this.btn56_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn56_3.Name = "btn56_3";
            this.btn56_3.Size = new System.Drawing.Size(131, 46);
            this.btn56_3.TabIndex = 3;
            this.btn56_3.Text = "排他制御(56-3)";
            this.btn56_3.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn56_5);
            this.groupBox1.Controls.Add(this.btn56_4);
            this.groupBox1.Controls.Add(this.btn56_3);
            this.groupBox1.Controls.Add(this.btn56_2);
            this.groupBox1.Controls.Add(this.btn56_1);
            this.groupBox1.Location = new System.Drawing.Point(12, 86);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(508, 142);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "マルチスレッド";
            // 
            // btn56_5
            // 
            this.btn56_5.Location = new System.Drawing.Point(192, 79);
            this.btn56_5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn56_5.Name = "btn56_5";
            this.btn56_5.Size = new System.Drawing.Size(131, 46);
            this.btn56_5.TabIndex = 4;
            this.btn56_5.Text = "lock 文(56-5)";
            this.btn56_5.UseVisualStyleBackColor = true;
            // 
            // btn56_4
            // 
            this.btn56_4.Location = new System.Drawing.Point(13, 79);
            this.btn56_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn56_4.Name = "btn56_4";
            this.btn56_4.Size = new System.Drawing.Size(131, 46);
            this.btn56_4.TabIndex = 5;
            this.btn56_4.Text = "C# における排他制御(56-4)";
            this.btn56_4.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn57_1);
            this.groupBox2.Location = new System.Drawing.Point(12, 247);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(508, 79);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "非同期メソッド";
            // 
            // btn57_1
            // 
            this.btn57_1.Location = new System.Drawing.Point(13, 22);
            this.btn57_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn57_1.Name = "btn57_1";
            this.btn57_1.Size = new System.Drawing.Size(131, 46);
            this.btn57_1.TabIndex = 3;
            this.btn57_1.Text = "非同期メソッド(57-1)";
            this.btn57_1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn58_3);
            this.groupBox3.Controls.Add(this.btn58_2);
            this.groupBox3.Controls.Add(this.btn58_1);
            this.groupBox3.Location = new System.Drawing.Point(5, 351);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(709, 79);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "非同期処理の種類";
            // 
            // btn58_1
            // 
            this.btn58_1.Location = new System.Drawing.Point(13, 22);
            this.btn58_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn58_1.Name = "btn58_1";
            this.btn58_1.Size = new System.Drawing.Size(163, 46);
            this.btn58_1.TabIndex = 3;
            this.btn58_1.Text = "バックグラウンド処理（非同期メソッド）(58-1)";
            this.btn58_1.UseVisualStyleBackColor = true;
            // 
            // btn58_2
            // 
            this.btn58_2.Location = new System.Drawing.Point(195, 22);
            this.btn58_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn58_2.Name = "btn58_2";
            this.btn58_2.Size = new System.Drawing.Size(217, 46);
            this.btn58_2.TabIndex = 3;
            this.btn58_2.Text = "データ並列（Parallel クラスと並列 LINQ）(58-2)";
            this.btn58_2.UseVisualStyleBackColor = true;
            // 
            // btn58_3
            // 
            this.btn58_3.Location = new System.Drawing.Point(437, 22);
            this.btn58_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn58_3.Name = "btn58_3";
            this.btn58_3.Size = new System.Drawing.Size(252, 46);
            this.btn58_3.TabIndex = 3;
            this.btn58_3.Text = "タスク並列/非同期データフロー（TPL Dataflowライブラリ）(58-3)";
            this.btn58_3.UseVisualStyleBackColor = true;
            // 
            // FrmAsynchronous
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 671);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmAsynchronous";
            this.Text = "FrmAsynchronous";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn56_1;
        private System.Windows.Forms.Button btn56_2;
        private System.Windows.Forms.Button btn56_3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn56_5;
        private System.Windows.Forms.Button btn56_4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn57_1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn58_3;
        private System.Windows.Forms.Button btn58_2;
        private System.Windows.Forms.Button btn58_1;
    }
}