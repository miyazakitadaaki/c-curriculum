﻿using System;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Threading;
using System.Text;

namespace curriculum1
{
    /// ********************************************************************************
    /// <summary>
    /// 非同期処理フォーム
    /// </summary>
    /// <remarks>none</remarks>
    /// 
    /// <history>
    /// --------------------------------------------------------------------------------
    /// No     Date         User         Remorks
    /// --------------------------------------------------------------------------------
    /// 0001   2015.12.05   宮崎祥彰   *new*
    /// 
    /// </history>
    /// ********************************************************************************
    public partial class FrmAsynchronous : Base_Form
    {
        #region ++++++++++ member variable ++++++++++
        public enum TopicNo
        {
        }
        #endregion

        #region++++++++++ constructor/destructor ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰     *new*
        /// 
        /// </history>
        /// ********************************************************************************
        public FrmAsynchronous()
        {
            // -------------------------------------------------------
            // 初期化処理
            InitializeComponent();
            this.EntryEvents();
        }

        #endregion

        #region ++++++++++ event ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// イベント Form　Load時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void FormMain_Load(object sender, EventArgs e)
        {

            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = !this.MaximizeBox;

        }

        /// ********************************************************************************
        /// <summary>
        /// イベントハンドラ登録
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void EntryEvents()
        {

            this.Load += new EventHandler(this.FormMain_Load);
            this.btn56_1.Click += new EventHandler(this.btn56_1_Click);
            this.btn56_2.Click += new EventHandler(this.btn56_2_Click);
            this.btn56_3.Click += new EventHandler(this.btn56_3_Click);
            this.btn56_4.Click += new EventHandler(this.btn56_4_Click);
            this.btn56_5.Click += new EventHandler(this.btn56_5_Click);
            this.btn57_1.Click += new EventHandler(this.btn57_1_Click);
            this.btn58_1.Click += new EventHandler(this.btn58_1_Click);
            this.btn58_2.Click += new EventHandler(this.btn58_2_Click);
            this.btn58_3.Click += new EventHandler(this.btn58_3_Click);

        }

        #region++++++++++マルチスレッド(56-1～56_5) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// マルチスレッドとは
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn56_1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("まず、スレッドに関して簡単に説明しておきます。簡単に言うと、スレッド（thread: 糸、筋道）とは一連の処理の流れのことを言います。図1 に示すように、 処理の流れが一本道な物をシングルスレッド、 複数の処理を平行して行う物をマルチスレッドと呼びます。"));
            //削除予定
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// C# におけるマルチスレッドプログラミング
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn56_2_Click(object sender, EventArgs e)
        {
            const int N = 3;

            Parallel.For(0, N, id => // こう書くだけで、並行して処理が行われる
            {
                Random rnd = new Random();

                for (int i = 0; i < 4; ++i)
                {
                    Thread.Sleep(rnd.Next(50, 100)); // ランダムな間隔で処理を一時中断

                    MessageBox.Show(string.Format("{0} (ID: {1})\n", i, id));
                }
            });
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// 排他制御
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn56_3_Click(object sender, EventArgs e)
        {
            const int ThreadNum = 20;
            const int LoopNum = 20;
            int num = 0; // 複数のスレッドから同時にアクセスされる。

            Parallel.For(0, ThreadNum, i =>
            {
                for (int j = 0; j < LoopNum; j++)
                {
                    // num をインクリメント。
                    // 実行結果が顕著に出るように、途中で Sleep をはさむ。
                    int tmp = num;
                    Thread.Sleep(1);
                    num = tmp + 1;
                }
            });

            MessageBox.Show(string.Format("{0} ({1})\n", num, ThreadNum * LoopNum));
            // num と THREAD_NUM * ROOP_NUM は一致するはずなんだけど・・・
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// C# における排他制御
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn56_4_Click(object sender, EventArgs e)
        {
            const int ThreadNum = 20;
            const int LoopNum = 20;
            int num = 0; // 複数のスレッドから同時にアクセスされる。
            var syncObject = new object();

            Parallel.For(0, ThreadNum, i =>
            {
                for (int j = 0; j < LoopNum; j++)
                {
                    Monitor.Enter(syncObject); // ロック取得
                    try
                    {
                        //↓クリティカルセクション
                        int tmp = num;
                        Thread.Sleep(1);
                        num = tmp + 1;
                        //↑クリティカルセクション
                    }
                    finally
                    {
                        Monitor.Exit(syncObject); // ロック解放
                    }
                }
            });

            MessageBox.Show(string.Format("{0} ({1})\n", num, ThreadNum * LoopNum));
            // num と THREAD_NUM * ROOP_NUM は一致するはずなんだけど・・・
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// lock 文
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn56_5_Click(object sender, EventArgs e)
        {
            const int ThreadNum = 20;
            const int LoopNum = 20;
            int num = 0; // 複数のスレッドから同時にアクセスされる。
            var syncObject = new object();

            Parallel.For(0, ThreadNum, i =>
            {
                for (int j = 0; j < LoopNum; j++)
                {
                    lock (syncObject)
                    {
                        int tmp = num;
                        Thread.Sleep(1);
                        num = tmp + 1;
                    }
                }
            });

            MessageBox.Show(string.Format("{0} ({1})\n", num, ThreadNum * LoopNum));
            // num と THREAD_NUM * ROOP_NUM は一致するはずなんだけど・・・
        }

        #endregion

        #region++++++++++非同期メソッド(57-1) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 非同期メソッド
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn57_1_Click(object sender, EventArgs e)
        {
            //目的：非ブロッキング処理: I/O 待ちとかで UI スレッドをフリーズさせないようにする
            //並列処理: マルチコアを活かした並列処理でパフォーマンス向上
            FrmAsync fAsync = new FrmAsync();
            fAsync.Show();

        }
        #endregion

        #region++++++++++非同期処理の種類(58-1～58_3) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// バックグラウンド処理（非同期メソッド）
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn58_1_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("public async Task メソッド名(型　x)\n{\n    前処理();\n    ");
            sb.Append("var result = GetValue(x);\n    後処理(result);\n}");
            sb.Append("\async Task<int> GetValueAsync(int x)\n{ ......}");
            MessageBox.Show(Convert.ToString(sb));
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// データ並列（Parallel クラスと並列 LINQ）
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn58_2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("for (var i = 0; i < source.Length; i++)\n{\n    result[i] = selector(source[i]);\n}\n\n// 並列実行\nParallel.For(0, source.Length, i =>\n{\n    result[i] = selector(source[i]);\n});");
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// タスク並列/非同期データフロー（TPL Dataflowライブラリ）
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn58_3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("並列処理を行うもう1つの方法としては、 \n異なる処理（タスク）を独立して動かして、\nその間で非同期にデータのやり取りする方法があります。 \n異なるタスクを並列に動かすという意味ではタスク並列（task parallelism）、 \n非同期なデータの受け渡しという意味では非同期データフロー（asynchronous dataflow）と呼ばれます。");
        }

        #endregion

    }
        #endregion

}