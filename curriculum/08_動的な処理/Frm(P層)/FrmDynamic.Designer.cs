﻿namespace curriculum1
{
    partial class FrmDynamic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn62_10 = new System.Windows.Forms.Button();
            this.btn62_9 = new System.Windows.Forms.Button();
            this.btn62_8 = new System.Windows.Forms.Button();
            this.btn62_7 = new System.Windows.Forms.Button();
            this.btn62_6 = new System.Windows.Forms.Button();
            this.btn62_5 = new System.Windows.Forms.Button();
            this.btn62_4 = new System.Windows.Forms.Button();
            this.btn62_3 = new System.Windows.Forms.Button();
            this.btn62_2 = new System.Windows.Forms.Button();
            this.btn62_1 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn61_9 = new System.Windows.Forms.Button();
            this.btn61_8 = new System.Windows.Forms.Button();
            this.btn61_7 = new System.Windows.Forms.Button();
            this.btn61_6 = new System.Windows.Forms.Button();
            this.btn61_5 = new System.Windows.Forms.Button();
            this.btn61_4 = new System.Windows.Forms.Button();
            this.btn61_3 = new System.Windows.Forms.Button();
            this.btn61_2 = new System.Windows.Forms.Button();
            this.btn61_1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn60_4 = new System.Windows.Forms.Button();
            this.btn60_3 = new System.Windows.Forms.Button();
            this.btn60_2 = new System.Windows.Forms.Button();
            this.btn60_1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn59_1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(9, 73);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1107, 591);
            this.tabControl1.TabIndex = 15;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabPage1.Size = new System.Drawing.Size(1099, 562);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "59-62";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn62_10);
            this.groupBox4.Controls.Add(this.btn62_9);
            this.groupBox4.Controls.Add(this.btn62_8);
            this.groupBox4.Controls.Add(this.btn62_7);
            this.groupBox4.Controls.Add(this.btn62_6);
            this.groupBox4.Controls.Add(this.btn62_5);
            this.groupBox4.Controls.Add(this.btn62_4);
            this.groupBox4.Controls.Add(this.btn62_3);
            this.groupBox4.Controls.Add(this.btn62_2);
            this.groupBox4.Controls.Add(this.btn62_1);
            this.groupBox4.Location = new System.Drawing.Point(557, 15);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(508, 258);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "式木(ExpressionTrees)";
            // 
            // btn62_10
            // 
            this.btn62_10.Location = new System.Drawing.Point(13, 198);
            this.btn62_10.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn62_10.Name = "btn62_10";
            this.btn62_10.Size = new System.Drawing.Size(131, 46);
            this.btn62_10.TabIndex = 3;
            this.btn62_10.Text = "メソッド・デリゲート呼び出し(62-10)";
            this.btn62_10.UseVisualStyleBackColor = true;
            // 
            // btn62_9
            // 
            this.btn62_9.Location = new System.Drawing.Point(349, 139);
            this.btn62_9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn62_9.Name = "btn62_9";
            this.btn62_9.Size = new System.Drawing.Size(131, 46);
            this.btn62_9.TabIndex = 3;
            this.btn62_9.Text = "インスタンス生成(61-9)";
            this.btn62_9.UseVisualStyleBackColor = true;
            // 
            // btn62_8
            // 
            this.btn62_8.Font = new System.Drawing.Font("MS UI Gothic", 7F);
            this.btn62_8.Location = new System.Drawing.Point(182, 139);
            this.btn62_8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn62_8.Name = "btn62_8";
            this.btn62_8.Size = new System.Drawing.Size(131, 46);
            this.btn62_8.TabIndex = 3;
            this.btn62_8.Text = "メンバー参照(61-8)";
            this.btn62_8.UseVisualStyleBackColor = true;
            // 
            // btn62_7
            // 
            this.btn62_7.Location = new System.Drawing.Point(13, 139);
            this.btn62_7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn62_7.Name = "btn62_7";
            this.btn62_7.Size = new System.Drawing.Size(131, 46);
            this.btn62_7.TabIndex = 3;
            this.btn62_7.Text = "型変換・判定(61-7)";
            this.btn62_7.UseVisualStyleBackColor = true;
            // 
            // btn62_6
            // 
            this.btn62_6.Location = new System.Drawing.Point(349, 80);
            this.btn62_6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn62_6.Name = "btn62_6";
            this.btn62_6.Size = new System.Drawing.Size(131, 46);
            this.btn62_6.TabIndex = 3;
            this.btn62_6.Text = "その他の2項・3項演算(62-6)";
            this.btn62_6.UseVisualStyleBackColor = true;
            // 
            // btn62_5
            // 
            this.btn62_5.Location = new System.Drawing.Point(182, 80);
            this.btn62_5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn62_5.Name = "btn62_5";
            this.btn62_5.Size = new System.Drawing.Size(131, 46);
            this.btn62_5.TabIndex = 3;
            this.btn62_5.Text = "論理演算(62-5)";
            this.btn62_5.UseVisualStyleBackColor = true;
            // 
            // btn62_4
            // 
            this.btn62_4.Location = new System.Drawing.Point(13, 80);
            this.btn62_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn62_4.Name = "btn62_4";
            this.btn62_4.Size = new System.Drawing.Size(131, 46);
            this.btn62_4.TabIndex = 3;
            this.btn62_4.Text = "比較演算(62-4)";
            this.btn62_4.UseVisualStyleBackColor = true;
            // 
            // btn62_3
            // 
            this.btn62_3.Location = new System.Drawing.Point(349, 22);
            this.btn62_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn62_3.Name = "btn62_3";
            this.btn62_3.Size = new System.Drawing.Size(131, 46);
            this.btn62_3.TabIndex = 3;
            this.btn62_3.Text = "算術演算(62-3)";
            this.btn62_3.UseVisualStyleBackColor = true;
            // 
            // btn62_2
            // 
            this.btn62_2.Location = new System.Drawing.Point(182, 22);
            this.btn62_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn62_2.Name = "btn62_2";
            this.btn62_2.Size = new System.Drawing.Size(131, 46);
            this.btn62_2.TabIndex = 3;
            this.btn62_2.Text = "ラムダ式(62-2)";
            this.btn62_2.UseVisualStyleBackColor = true;
            // 
            // btn62_1
            // 
            this.btn62_1.Location = new System.Drawing.Point(13, 22);
            this.btn62_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn62_1.Name = "btn62_1";
            this.btn62_1.Size = new System.Drawing.Size(131, 46);
            this.btn62_1.TabIndex = 3;
            this.btn62_1.Text = "概要(62-1)";
            this.btn62_1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn61_9);
            this.groupBox3.Controls.Add(this.btn61_8);
            this.groupBox3.Controls.Add(this.btn61_7);
            this.groupBox3.Controls.Add(this.btn61_6);
            this.groupBox3.Controls.Add(this.btn61_5);
            this.groupBox3.Controls.Add(this.btn61_4);
            this.groupBox3.Controls.Add(this.btn61_3);
            this.groupBox3.Controls.Add(this.btn61_2);
            this.groupBox3.Controls.Add(this.btn61_1);
            this.groupBox3.Location = new System.Drawing.Point(19, 267);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(508, 205);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "dynamic";
            // 
            // btn61_9
            // 
            this.btn61_9.Location = new System.Drawing.Point(349, 139);
            this.btn61_9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn61_9.Name = "btn61_9";
            this.btn61_9.Size = new System.Drawing.Size(131, 46);
            this.btn61_9.TabIndex = 3;
            this.btn61_9.Text = "多重ディスパッチ(61-9)";
            this.btn61_9.UseVisualStyleBackColor = true;
            // 
            // btn61_8
            // 
            this.btn61_8.Font = new System.Drawing.Font("MS UI Gothic", 7F);
            this.btn61_8.Location = new System.Drawing.Point(182, 139);
            this.btn61_8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn61_8.Name = "btn61_8";
            this.btn61_8.Size = new System.Drawing.Size(131, 46);
            this.btn61_8.TabIndex = 3;
            this.btn61_8.Text = "ジェネリクス利用時の静的メソッド呼び出し(61-8)";
            this.btn61_8.UseVisualStyleBackColor = true;
            // 
            // btn61_7
            // 
            this.btn61_7.Location = new System.Drawing.Point(13, 139);
            this.btn61_7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn61_7.Name = "btn61_7";
            this.btn61_7.Size = new System.Drawing.Size(131, 46);
            this.btn61_7.TabIndex = 3;
            this.btn61_7.Text = "データ連携(61-7)";
            this.btn61_7.UseVisualStyleBackColor = true;
            // 
            // btn61_6
            // 
            this.btn61_6.Location = new System.Drawing.Point(349, 80);
            this.btn61_6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn61_6.Name = "btn61_6";
            this.btn61_6.Size = new System.Drawing.Size(131, 46);
            this.btn61_6.TabIndex = 3;
            this.btn61_6.Text = "ダックタイピング(61-6)";
            this.btn61_6.UseVisualStyleBackColor = true;
            // 
            // btn61_5
            // 
            this.btn61_5.Location = new System.Drawing.Point(182, 80);
            this.btn61_5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn61_5.Name = "btn61_5";
            this.btn61_5.Size = new System.Drawing.Size(131, 46);
            this.btn61_5.TabIndex = 3;
            this.btn61_5.Text = "DLR 連携(61-5)";
            this.btn61_5.UseVisualStyleBackColor = true;
            // 
            // btn61_4
            // 
            this.btn61_4.Location = new System.Drawing.Point(13, 80);
            this.btn61_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn61_4.Name = "btn61_4";
            this.btn61_4.Size = new System.Drawing.Size(131, 46);
            this.btn61_4.TabIndex = 3;
            this.btn61_4.Text = "遅延バインド(61-4)";
            this.btn61_4.UseVisualStyleBackColor = true;
            // 
            // btn61_3
            // 
            this.btn61_3.Location = new System.Drawing.Point(349, 22);
            this.btn61_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn61_3.Name = "btn61_3";
            this.btn61_3.Size = new System.Drawing.Size(131, 46);
            this.btn61_3.TabIndex = 3;
            this.btn61_3.Text = "dynamic で何ができるか(61-3)";
            this.btn61_3.UseVisualStyleBackColor = true;
            // 
            // btn61_2
            // 
            this.btn61_2.Location = new System.Drawing.Point(182, 22);
            this.btn61_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn61_2.Name = "btn61_2";
            this.btn61_2.Size = new System.Drawing.Size(131, 46);
            this.btn61_2.TabIndex = 3;
            this.btn61_2.Text = "dynamic の仕組み(61-2)";
            this.btn61_2.UseVisualStyleBackColor = true;
            // 
            // btn61_1
            // 
            this.btn61_1.Location = new System.Drawing.Point(13, 22);
            this.btn61_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn61_1.Name = "btn61_1";
            this.btn61_1.Size = new System.Drawing.Size(131, 46);
            this.btn61_1.TabIndex = 3;
            this.btn61_1.Text = "動的型付け変数(61-1)";
            this.btn61_1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn60_4);
            this.groupBox2.Controls.Add(this.btn60_3);
            this.groupBox2.Controls.Add(this.btn60_2);
            this.groupBox2.Controls.Add(this.btn60_1);
            this.groupBox2.Location = new System.Drawing.Point(19, 107);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(508, 141);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "属性";
            // 
            // btn60_4
            // 
            this.btn60_4.Location = new System.Drawing.Point(13, 80);
            this.btn60_4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn60_4.Name = "btn60_4";
            this.btn60_4.Size = new System.Drawing.Size(131, 46);
            this.btn60_4.TabIndex = 3;
            this.btn60_4.Text = "属性情報の取得(60-4)";
            this.btn60_4.UseVisualStyleBackColor = true;
            // 
            // btn60_3
            // 
            this.btn60_3.Location = new System.Drawing.Point(349, 22);
            this.btn60_3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn60_3.Name = "btn60_3";
            this.btn60_3.Size = new System.Drawing.Size(131, 46);
            this.btn60_3.TabIndex = 3;
            this.btn60_3.Text = "属性の自作(60-3)";
            this.btn60_3.UseVisualStyleBackColor = true;
            // 
            // btn60_2
            // 
            this.btn60_2.Location = new System.Drawing.Point(182, 22);
            this.btn60_2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn60_2.Name = "btn60_2";
            this.btn60_2.Size = new System.Drawing.Size(131, 46);
            this.btn60_2.TabIndex = 3;
            this.btn60_2.Text = "属性の対象(60-2)";
            this.btn60_2.UseVisualStyleBackColor = true;
            // 
            // btn60_1
            // 
            this.btn60_1.Location = new System.Drawing.Point(13, 22);
            this.btn60_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn60_1.Name = "btn60_1";
            this.btn60_1.Size = new System.Drawing.Size(131, 46);
            this.btn60_1.TabIndex = 3;
            this.btn60_1.Text = "定義済み属性(60-1)";
            this.btn60_1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn59_1);
            this.groupBox1.Location = new System.Drawing.Point(19, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(508, 77);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "実行時方情報";
            // 
            // btn59_1
            // 
            this.btn59_1.Location = new System.Drawing.Point(13, 22);
            this.btn59_1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn59_1.Name = "btn59_1";
            this.btn59_1.Size = new System.Drawing.Size(131, 46);
            this.btn59_1.TabIndex = 3;
            this.btn59_1.Text = "実行時型情報の取得(59-1)";
            this.btn59_1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(-2, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1134, 66);
            this.panel1.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(100, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(427, 40);
            this.label1.TabIndex = 0;
            this.label1.Text = "動的な処理(59-1_62-11)";
            // 
            // FrmDynamic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 671);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Name = "FrmDynamic";
            this.Text = "FrmDynamic";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn59_1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn60_1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btn62_9;
        private System.Windows.Forms.Button btn62_8;
        private System.Windows.Forms.Button btn62_7;
        private System.Windows.Forms.Button btn62_6;
        private System.Windows.Forms.Button btn62_5;
        private System.Windows.Forms.Button btn62_4;
        private System.Windows.Forms.Button btn62_3;
        private System.Windows.Forms.Button btn62_2;
        private System.Windows.Forms.Button btn62_1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn61_9;
        private System.Windows.Forms.Button btn61_8;
        private System.Windows.Forms.Button btn61_7;
        private System.Windows.Forms.Button btn61_6;
        private System.Windows.Forms.Button btn61_5;
        private System.Windows.Forms.Button btn61_4;
        private System.Windows.Forms.Button btn61_3;
        private System.Windows.Forms.Button btn61_2;
        private System.Windows.Forms.Button btn61_1;
        private System.Windows.Forms.Button btn60_4;
        private System.Windows.Forms.Button btn60_3;
        private System.Windows.Forms.Button btn60_2;
        private System.Windows.Forms.Button btn62_10;
    }
}