﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace curriculum1
{
    /// ********************************************************************************
    /// <summary>
    /// 非同期メソッド用フォーム
    /// </summary>
    /// <remarks>none</remarks>
    /// 
    /// <history>
    /// --------------------------------------------------------------------------------
    /// No     Date         User         Remorks
    /// --------------------------------------------------------------------------------
    /// 0001   2015.12.05   宮崎祥彰   *new*
    /// 
    /// </history>
    /// ********************************************************************************
    public partial class FrmDynamic : Base_Form
    {
        
        #region ++++++++++ member variable ++++++++++
        #endregion

        #region++++++++++ constructor/destructor ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰     *new*
        /// 
        /// </history>
        /// ********************************************************************************
        public FrmDynamic()
        {
            // -------------------------------------------------------
            // 初期化処理
            InitializeComponent();
            this.EntryEvents();
        }

        #endregion

        #region ++++++++++ event ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// イベント Form　Load時
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void FormMain_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = !this.MaximizeBox;

        }

        /// ********************************************************************************
        /// <summary>
        /// イベントハンドラ登録
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void EntryEvents()
        {

            this.Load += new EventHandler(this.FormMain_Load);
            this.btn59_1.Click += new EventHandler(this.btn59_1_Click);
            this.btn60_1.Click += new EventHandler(this.btn60_1_Click);
            this.btn60_2.Click += new EventHandler(this.btn60_2_Click);
            this.btn60_3.Click += new EventHandler(this.btn60_3_Click);
            this.btn60_4.Click += new EventHandler(this.btn60_4_Click);
            this.btn61_1.Click += new EventHandler(this.btn61_1_Click);
            this.btn61_2.Click += new EventHandler(this.btn61_2_Click);
            this.btn61_3.Click += new EventHandler(this.btn61_3_Click);
            this.btn61_4.Click += new EventHandler(this.btn61_4_Click);
            this.btn61_5.Click += new EventHandler(this.btn61_5_Click);
            this.btn61_6.Click += new EventHandler(this.btn61_6_Click);
            this.btn61_7.Click += new EventHandler(this.btn61_7_Click);
            this.btn61_8.Click += new EventHandler(this.btn61_8_Click);
            this.btn61_9.Click += new EventHandler(this.btn61_9_Click);
            this.btn62_1.Click += new EventHandler(this.btn62_1_Click);
            this.btn62_2.Click += new EventHandler(this.btn62_2_Click);
            this.btn62_3.Click += new EventHandler(this.btn62_3_Click);
            this.btn62_4.Click += new EventHandler(this.btn62_4_Click);
            this.btn62_5.Click += new EventHandler(this.btn62_5_Click);
            this.btn62_6.Click += new EventHandler(this.btn62_6_Click);
            this.btn62_7.Click += new EventHandler(this.btn62_7_Click);
            this.btn62_8.Click += new EventHandler(this.btn62_8_Click);
            this.btn62_9.Click += new EventHandler(this.btn62_9_Click);
            this.btn62_10.Click += new EventHandler(this.btn62_10_Click);

        }

        #endregion

        #region ++++++++++ 実行時型情報 ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// 実行時型情報の取得
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private async void btn59_1_Click(object sender, EventArgs e_)
        {
            //C# などのような、実行時型情報をサポートする言語では、
            // 実行には直接不要ではあっても、 「Rect 構造体は Width とか Height という
            //名前のメンバーを持っている」 という情報を持っていて、 
            //実行時にこういった情報を引き出せるようになっています。
            Type t = Type.GetType("Rect59_1");

            object o = Activator.CreateInstance(t);

            t.GetField("Width").SetValue(o, 3);
            t.GetField("Height").SetValue(o, 4);

            int w = (int)t.GetField("Width").GetValue(o);
            int h = (int)t.GetField("Height").GetValue(o);
            int area = w * h;

            Rect59_1 x = (Rect59_1)o;
            MessageBox.Show(String.Format("{0} × {1} ＝ {2}\n", x.Width, x.Height, area));
        }

        struct Rect59_1
        {
            public int Width;
            public int Height;
        }

        #endregion

        #region ++++++++++ 属性 ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// 定義済み属性取得
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn60_1_Click(object sender, EventArgs e_)
        {
            MessageBox.Show("・コンパイラが利用\nSystem.AttributeUsageAttribute\nSystem.ObsoleteAttribute\nSystem.Diagnostics.ConditionalAttribute");
            MessageBox.Show("・開発ツールCategoryAttribute\nDefaultValueAttribute\nDescriptionAttribute\nBrowsableAttribute");
            MessageBox.Show("・実行エンジン\nDllImportAttribute\nComImportAttribute");
            MessageBox.Show("ライブラリ");
            MessageBox.Show("■ASP.NET\nSystem.Web.Services.WebMethodAttribute\n");
            MessageBox.Show("■WCF（Windows Communication Foundation\nSystem.ServiceModel.OperationContractAttribute\nSystem.ServiceModel.ServiceContractAttribute\nSystem.Runtime.Serialization.DataContractAttribute\n");
            MessageBox.Show("■データ検証\nRequiredAttribute\nRangeAttribute\nStringLengthAttribute\n");
            MessageBox.Show("■テスト\nTestClassAttribute\nTestMethodAttribute");

        }

        /// ********************************************************************************
        /// <summary>
        /// 属性の対象
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn60_2_Click(object sender, EventArgs e_)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("このような曖昧さを解決するため、 明示的に属性の対象を指定する構文があります。\n");
            sb.Append("[属性の対象 : 属性名(属性のオプション)]\n");
            sb.Append("[assembly: AssemblyTitle('Test Attribute')] // プログラムそのものが対象\n");
            sb.Append("[Serializable] // クラスが対象\n");
            sb.Append("public class SampleClass\n");
            sb.Append("{\n");
            sb.Append("} \n");
            sb.Append("[Obsolete('時期版で削除します。使わないでください。')] // メソッドが対象");
            
            MessageBox.Show(Convert.ToString(sb));

        }

        /// ********************************************************************************
        /// <summary>
        /// 属性の自作
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn60_3_Click(object sender, EventArgs e_)
        {
            AuthorAttribute60_3 caa = new AuthorAttribute60_3("c60_3name");
            caa.affiliation = "systemgroup";
            caa.GetMethod();
        }

        [AttributeUsage(
          AttributeTargets.Class | AttributeTargets.Struct,
          AllowMultiple = true,
          Inherited = false)]
        public class AuthorAttribute60_3 : Attribute
        {
          private string name;       // 作者名
          public string affiliation; // 作者所属
          public AuthorAttribute60_3(string name)
          {
              this.name = name;
          }
          public void GetMethod(){
              MessageBox.Show(String.Format("作者名：{0}、作者所属：{1}",name,affiliation));
          }
        }


        /// ********************************************************************************
        /// <summary>
        /// 属性情報の取得
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private async void btn60_4_Click(object sender, EventArgs e_)
        {
            GetAllAuthors(typeof(AuthorTest));
        }
        /// <summary>
        /// クラス自体とクラス中の public メソッドの作者情報を取得する。
        /// </summary>
        /// <param name="t">クラスの Type</param>
        static void GetAllAuthors(Type t)
        {
            MessageBox.Show(String.Format("type name: {0}\n", t.Name));
            GetAuthors(t);

            foreach (MethodInfo info in t.GetMethods())
            {
                MessageBox.Show(String.Format("  method name: {0}\n", info.Name));
                GetAuthors(info);
            }
        }

        /// <summary>
        /// クラスやメソッドの作者情報を取得する。
        /// </summary>
        /// <param name="info">クラスやメソッドの MemberInfo</param>
        static void GetAuthors(MemberInfo info)
        {
            Attribute[] authors = Attribute.GetCustomAttributes(
                info, typeof(AuthorAttribute));
            foreach (Attribute att in authors)
            {
                AuthorAttribute author = att as AuthorAttribute;
                if (author != null)
                {
                    MessageBox.Show(String.Format("    author name: {0}\n", author.Name));
                }
            }
        }

        /// <summary>
        /// 作者情報を残すための属性。
        /// </summary>
        [AttributeUsage(
            AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Method,
            AllowMultiple = true,
            Inherited = false)]
        public class AuthorAttribute : Attribute
        {
            private string name;
            public AuthorAttribute(string name) { this.name = name; }
            public string Name { get { return this.name; } }
        }

        /// <summary>
        /// テスト用のクラス。
        /// メソッドごとに違う人が開発するなんてほとんどありえないけど、
        /// その辺は目をつぶってください。
        /// </summary>
        [Author("Stephanie McMahon")]
        [Author("Hunter Herst Helmsly")]
        class AuthorTest
        {
            [Author("Kurt Angle")]
            public static void A() { }
            [Author("Rocky Mavia")]
            public static void B() { }
            [Author("Chris Jericho")]
            public static void C() { }
            [Author("Glen Jacobs")]
            public static void D() { }
        }

        #endregion
        #region++++++++++dynamic(61_1～61_9) ++++++++++
        ///// ********************************************************************************
        ///// <summary>
        ///// 動的型付け変数
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn61_1_Click(object sender, EventArgs e)
        {
            C61_1 c = new C61_1();
            MessageBox.Show(GetX(c));
        }
        private class C61_1
        {
            //メンバー変数の定義
            public string X = "28_1のstring a をインスタンス";

        }
        static dynamic GetX(dynamic obj)
        {
            return obj.X;
        }

        ///// ********************************************************************************
        ///// <summary>
        ///// dynamic の仕組み
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn61_2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("C# の dynamic は、「型が動的」というよりは、「静的な型に対する動的コード生成」と言った方が正確です。 動的に生成したコードはキャッシュされていて、2度目の呼び出しからはかなり効率よく実行されます。 このような手法はインラインメソッドキャッシュ（inline method cache）と呼ばれています。");
            //削除予定
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// dynamic で何ができるか
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn61_3_Click(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("キーワード\n 「遅延バインド」\n 「DLR 連携」\n 「ダックタイピング」\n 「データ連携」\n 「ジェネリクス利用時の静的メソッド呼び出し」\n 「多重ディスパッチ」"));
            //削除予定
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// 遅延バインド
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        public class Calculator
        {
            public int Add(int x, int y) { return x + y; }
            public int Sub(int x, int y) { return x - y; }
            public int Mul(int x, int y) { return x * y; }
            public int Div(int x, int y) { return x / y; }
        }
        //上記のプログラムを記述したlib.dllをbinフォルダ内に配置。
        private void btn61_4_Click(object sender, EventArgs e)
        {
            var lib = Assembly.Load("lib");
            var type = lib.GetType("Calculator");
            dynamic calc = Activator.CreateInstance(type);
            MessageBox.Show(string.Format("{0}", calc.Add(1, 2)));
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// DLR 連携
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn61_5_Click(object sender, EventArgs e)
        {
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// ダックタイピング
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn61_6_Click(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("まず、スレッドに関して簡単に説明しておきます。簡単に言うと、スレッド（thread: 糸、筋道）とは一連の処理の流れのことを言います。図1 に示すように、 処理の流れが一本道な物をシングルスレッド、 複数の処理を平行して行う物をマルチスレッドと呼びます。"));
            //削除予定
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// データ連携
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn61_7_Click(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("まず、スレッドに関して簡単に説明しておきます。簡単に言うと、スレッド（thread: 糸、筋道）とは一連の処理の流れのことを言います。図1 に示すように、 処理の流れが一本道な物をシングルスレッド、 複数の処理を平行して行う物をマルチスレッドと呼びます。"));
            //削除予定
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// ジェネリクス利用時の静的メソッド呼び出し
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn61_8_Click(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("まず、スレッドに関して簡単に説明しておきます。簡単に言うと、スレッド（thread: 糸、筋道）とは一連の処理の流れのことを言います。図1 に示すように、 処理の流れが一本道な物をシングルスレッド、 複数の処理を平行して行う物をマルチスレッドと呼びます。"));
            //削除予定
        }
        ///// ********************************************************************************
        ///// <summary>
        ///// 多重ディスパッチ
        ///// </summary>
        ///// <remarks>none</remarks>
        ///// 
        ///// <history>
        ///// --------------------------------------------------------------------------------
        ///// No     Date         User         Remorks
        ///// --------------------------------------------------------------------------------
        ///// 0001   2016.01.03  宮崎祥彰   *new*
        ///// 
        ///// </history>
        ///// ********************************************************************************
        private void btn61_9_Click(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("まず、スレッドに関して簡単に説明しておきます。簡単に言うと、スレッド（thread: 糸、筋道）とは一連の処理の流れのことを言います。図1 に示すように、 処理の流れが一本道な物をシングルスレッド、 複数の処理を平行して行う物をマルチスレッドと呼びます。"));
            //削除予定
        }

        #endregion

        #region ++++++++++ 式木(ExpressionTrees) ++++++++++
        /// ********************************************************************************
        /// <summary>
        /// 概要
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn62_1_Click(object sender, EventArgs e_)
        {
            var e =
                Expression.Lambda<Func<int, int>>(
                Expression.Add(
                    Expression.Parameter(typeof(int), "x"),
                    Expression.Constant(5)),
                Expression.Parameter(typeof(int), "x"));
                MessageBox.Show(Convert.ToString(e));
        }

        static partial class Make
        {
            public static Expression<Func<TR>> Expression<TR>(Expression<Func<TR>> e)
            {
                return e;
            }

            public static Expression<Func<T1, TR>> Expression<T1, TR>(Expression<Func<T1, TR>> e)
            {
                return e;
            }

            public static Expression<Func<T1, T2, TR>> Expression<T1, T2, TR>(Expression<Func<T1, T2, TR>> e)
            {
                return e;
            }

            public static Expression<Func<T1, T2, T3, TR>> Expression<T1, T2, T3, TR>(Expression<Func<T1, T2, T3, TR>> e)
            {
                return e;
            }

            public static Expression<Func<T1, T2, T3, T4, TR>> Expression<T1, T2, T3, T4, TR>(Expression<Func<T1, T2, T3, T4, TR>> e)
            {
                return e;
            }
        }
        /// <summary>
        /// 式木の構造が一致してれば、少なくとも ToString の結果は一致するので、
        /// それで2つの式木の一致性を判定。
        /// </summary>
        static void SimpleCheck(Expression e1, Expression e2)
        {
            if (e1.ToString() != e2.ToString())
            {
                Console.Write("not match: {0}, {1}\n", e1, e2);
            }
        }
        // Parameter を作って変数に代入
        static ParameterExpression intX = Expression.Parameter(typeof(int), "x");
        static ParameterExpression intY = Expression.Parameter(typeof(int), "y");
        static ParameterExpression boolX = Expression.Parameter(typeof(bool), "x");
        static ParameterExpression boolY = Expression.Parameter(typeof(bool), "y");

        /// ********************************************************************************
        /// <summary>
        /// ラムダ式
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn62_2_Click(object sender, EventArgs e_)
        {
            SimpleCheck(
            Make.Expression(() =>
            (Expression<Func<int>>)(() => 0)
            ).Body,
            Expression.Convert(
            Expression.Quote(
                (Expression<Func<int>>)(() => 0)),
            typeof(Expression<Func<int>>))
            );

            MessageBox.Show("式木を62_2_Clickメソッド内に記述");
        }
        

        /// ********************************************************************************
        /// <summary>
        /// 算術演算
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn62_3_Click(object sender, EventArgs e_)
        {
            SimpleCheck(
              Make.Expression((int x) => -x).Body,
              Expression.Negate(intX)
            );
            SimpleCheck(
              Make.Expression((int x) => checked(-x)).Body,
              Expression.NegateChecked(intX)
            );

            MessageBox.Show("式木を62_3_Clickメソッド内に記述");
        }

        /// ********************************************************************************
        /// <summary>
        /// 比較演算
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn62_4_Click(object sender, EventArgs e_)
        {
            MessageBox.Show("削除予定");
        }

        /// ********************************************************************************
        /// <summary>
        /// 論理演算
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn62_5_Click(object sender, EventArgs e_)
        {
            MessageBox.Show("削除予定");
        }

        /// ********************************************************************************
        /// <summary>
        /// その他の2項・3項演算
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn62_6_Click(object sender, EventArgs e_)
        {
            MessageBox.Show("削除予定");
        }

        /// ********************************************************************************
        /// <summary>
        /// 型変換・判定
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn62_7_Click(object sender, EventArgs e_)
        {
            MessageBox.Show("削除予定");
        }

        /// ********************************************************************************
        /// <summary>
        /// メンバー参照
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn62_8_Click(object sender, EventArgs e_)
        {
            MessageBox.Show("削除予定");
        }

        /// ********************************************************************************
        /// <summary>
        /// インスタンス生成
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn62_9_Click(object sender, EventArgs e_)
        {
            MessageBox.Show("削除予定");
        }

        /// ********************************************************************************
        /// <summary>
        /// メソッド・デリゲート呼び出し
        /// </summary>
        /// <remarks>none</remarks>
        /// 
        /// <history>
        /// --------------------------------------------------------------------------------
        /// No     Date         User         Remorks
        /// --------------------------------------------------------------------------------
        /// 0001   2015.12.05   宮崎祥彰   *new*
        /// 
        /// </history>
        /// ********************************************************************************
        private void btn62_10_Click(object sender, EventArgs e_)
        {
            MessageBox.Show("削除予定");
        }

        #endregion
    }



}

