﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace curriculum1
{
    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MenuDisplay());
        }
    }

    static class StringExtensions
    {
        /// <summary>
        /// 文字列の大文字と小文字を入れ替える。
        /// </summary>
        /// <param name="s">変換元</param>
        /// <returns>変換結果</returns>
        public static string ToggleCase(this string s)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            foreach (char c in s)
            {
                if (char.IsUpper(c))
                    sb.Append(char.ToLower(c));
                else if (char.IsLower(c))
                    sb.Append(char.ToUpper(c));
                else
                    sb.Append(c);
            }
            return sb.ToString();
        }
    }

}
